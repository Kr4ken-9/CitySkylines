﻿using System;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class PlayerBuildingAI : CommonBuildingAI
{
	public override Color GetColor(ushort buildingID, ref Building data, InfoManager.InfoMode infoMode)
	{
		switch (infoMode)
		{
		case InfoManager.InfoMode.Electricity:
			if (this.m_electricityConsumption == 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		case InfoManager.InfoMode.Water:
			if (this.m_waterConsumption == 0 && this.m_sewageAccumulation == 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		case InfoManager.InfoMode.CrimeRate:
			if (data.m_citizenCount == 0)
			{
				return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
			}
			return base.GetColor(buildingID, ref data, infoMode);
		case InfoManager.InfoMode.Health:
		case InfoManager.InfoMode.Happiness:
			break;
		default:
			switch (infoMode)
			{
			case InfoManager.InfoMode.Entertainment:
				goto IL_46;
			case InfoManager.InfoMode.TerrainHeight:
				IL_31:
				if (infoMode != InfoManager.InfoMode.Garbage)
				{
					if (infoMode != InfoManager.InfoMode.Radio)
					{
						return base.GetColor(buildingID, ref data, infoMode);
					}
					if (this.RequireRoadAccess())
					{
						int num;
						Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.RadioCoverage, data.m_position, out num);
						return CommonBuildingAI.GetRadioCoverageColor((float)num);
					}
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				else
				{
					if (this.m_garbageAccumulation == 0)
					{
						return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
					}
					return base.GetColor(buildingID, ref data, infoMode);
				}
				break;
			case InfoManager.InfoMode.Heating:
				if (this.m_electricityConsumption == 0)
				{
					return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
				}
				return base.GetColor(buildingID, ref data, infoMode);
			}
			goto IL_31;
		}
		IL_46:
		return Singleton<InfoManager>.get_instance().m_properties.m_neutralColor;
	}

	protected override bool ShowConsumption(ushort buildingID, ref Building data)
	{
		return (data.m_flags & Building.Flags.Active) != Building.Flags.None;
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, NaturalResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, ImmaterialResourceManager.Resource resource)
	{
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetResourceRate(ushort buildingID, ref Building data, EconomyManager.Resource resource)
	{
		if (resource == EconomyManager.Resource.Maintenance)
		{
			int num = (int)data.m_productionRate;
			if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
			{
				num = 0;
			}
			int budget = this.GetBudget(buildingID, ref data);
			int num2 = this.GetMaintenanceCost() / 100;
			num2 = num * budget / 100 * num2;
			return -num2;
		}
		return base.GetResourceRate(buildingID, ref data, resource);
	}

	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = this.GetBudget(buildingID, ref data);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return -(num * this.m_electricityConsumption / 100);
	}

	public override int GetWaterRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = this.GetBudget(buildingID, ref data);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return -(num * this.m_waterConsumption / 100);
	}

	public override int GetGarbageRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = this.GetBudget(buildingID, ref data);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		return num * this.m_garbageAccumulation / 100;
	}

	public override void InitializePrefab()
	{
		base.InitializePrefab();
		if (this.m_createPassMilestone != null)
		{
			this.m_createPassMilestone.SetPrefab(this.m_info);
		}
		if (this.m_createPassMilestone2 != null)
		{
			this.m_createPassMilestone2.SetPrefab(this.m_info);
		}
		if (this.m_createPassMilestone3 != null)
		{
			this.m_createPassMilestone3.SetPrefab(this.m_info);
		}
	}

	public override string GetLocalizedStatus(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
		{
			if (data.GetLastFrameData().m_fireDamage == 255)
			{
				return Locale.Get("BUILDING_STATUS_BURNED");
			}
			return Locale.Get("BUILDING_STATUS_COLLAPSED");
		}
		else
		{
			if ((data.m_flags & Building.Flags.Completed) == Building.Flags.None)
			{
				return Locale.Get("BUILDING_STATUS_UNDER_CONSTRUCTION");
			}
			if (data.m_productionRate == 0)
			{
				return Locale.Get("BUILDING_STATUS_OFF");
			}
			if ((data.m_flags & (Building.Flags.Evacuating | Building.Flags.Active)) != Building.Flags.Active)
			{
				return this.GetLocalizedStatusInactive(buildingID, ref data);
			}
			return this.GetLocalizedStatusActive(buildingID, ref data);
		}
	}

	protected virtual string GetLocalizedStatusInactive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_EVACUATED");
		}
		return Locale.Get("BUILDING_STATUS_NOT_OPERATING");
	}

	protected virtual string GetLocalizedStatusActive(ushort buildingID, ref Building data)
	{
		if ((data.m_flags & Building.Flags.RateReduced) != Building.Flags.None)
		{
			return Locale.Get("BUILDING_STATUS_REDUCED");
		}
		return Locale.Get("BUILDING_STATUS_DEFAULT");
	}

	public override string GetDebugString(ushort buildingID, ref Building data)
	{
		if (data.m_eventIndex != 0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			EventInfo info = instance.m_events.m_buffer[(int)data.m_eventIndex].Info;
			return info.m_eventAI.GetDebugString(data.m_eventIndex, ref instance.m_events.m_buffer[(int)data.m_eventIndex]);
		}
		return base.GetDebugString(buildingID, ref data);
	}

	public override void CreateBuilding(ushort buildingID, ref Building data)
	{
		base.CreateBuilding(buildingID, ref data);
		data.m_productionRate = 100;
		Singleton<BuildingManager>.get_instance().AddServiceBuilding(buildingID, this.m_info.m_class.m_service);
		if (this.m_createPassMilestone != null)
		{
			this.m_createPassMilestone.Unlock();
		}
		if (this.m_createPassMilestone2 != null)
		{
			this.m_createPassMilestone2.Unlock();
		}
		if (this.m_createPassMilestone3 != null)
		{
			this.m_createPassMilestone3.Unlock();
		}
		if (this.RequireRoadAccess())
		{
			Singleton<BuildingManager>.get_instance().RoadCheckNeeded(buildingID);
		}
	}

	public override void ReleaseBuilding(ushort buildingID, ref Building data)
	{
		if (this.m_createPassMilestone != null)
		{
			this.m_createPassMilestone.Relock();
		}
		if (this.m_createPassMilestone2 != null)
		{
			this.m_createPassMilestone2.Relock();
		}
		if (this.m_createPassMilestone3 != null)
		{
			this.m_createPassMilestone3.Relock();
		}
		Singleton<BuildingManager>.get_instance().RemoveServiceBuilding(buildingID, this.m_info.m_class.m_service);
		if (data.m_eventIndex != 0 && (this.m_supportEvents & EventManager.EventType.SecondaryLocation) != (EventManager.EventType)0)
		{
			Singleton<EventManager>.get_instance().RemoveEventLocation(data.m_eventIndex, buildingID);
			data.m_eventIndex = 0;
		}
		base.ReleaseBuilding(buildingID, ref data);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		Singleton<BuildingManager>.get_instance().AddServiceBuilding(buildingID, this.m_info.m_class.m_service);
		if ((this.m_supportEvents & EventManager.EventType.SecondaryLocation) != (EventManager.EventType)0)
		{
			data.m_eventIndex = Singleton<EventManager>.get_instance().FindEvent(this.m_supportEvents, this.m_supportGroups);
			if (data.m_eventIndex != 0)
			{
				Singleton<EventManager>.get_instance().AddEventLocation(data.m_eventIndex, buildingID);
			}
		}
	}

	public override BuildingInfo GetUpgradeInfo(ushort buildingID, ref Building data)
	{
		return this.m_upgradeTarget;
	}

	public override void BuildingUpgraded(ushort buildingID, ref Building data)
	{
		base.BuildingUpgraded(buildingID, ref data);
		this.ManualActivation(buildingID, ref data);
		Singleton<MessageManager>.get_instance().TryCreateMessage(this.m_onCompleteMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID());
	}

	public override void BuildingDeactivated(ushort buildingID, ref Building data)
	{
		if (data.m_eventIndex != 0 && (this.m_supportEvents & EventManager.EventType.SecondaryLocation) == (EventManager.EventType)0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			EventInfo info = instance.m_events.m_buffer[(int)data.m_eventIndex].Info;
			if (info != null)
			{
				info.m_eventAI.BuildingDeactivated(data.m_eventIndex, ref instance.m_events.m_buffer[(int)data.m_eventIndex]);
			}
		}
		base.BuildingDeactivated(buildingID, ref data);
	}

	protected override void BuildingCompleted(ushort buildingID, ref Building buildingData)
	{
		base.BuildingCompleted(buildingID, ref buildingData);
		Singleton<MessageManager>.get_instance().TryCreateMessage(this.m_onCompleteMessage, Singleton<MessageManager>.get_instance().GetRandomResidentID());
		if (this.m_supportEvents != (EventManager.EventType)0 && buildingData.m_eventIndex == 0)
		{
			if ((this.m_supportEvents & EventManager.EventType.SecondaryLocation) != (EventManager.EventType)0)
			{
				buildingData.m_eventIndex = Singleton<EventManager>.get_instance().FindEvent(this.m_supportEvents, this.m_supportGroups);
				if (buildingData.m_eventIndex != 0)
				{
					Singleton<EventManager>.get_instance().AddEventLocation(buildingData.m_eventIndex, buildingID);
				}
			}
			else
			{
				EventInfo eventInfo = Singleton<EventManager>.get_instance().FindEventInfo(this.m_supportEvents, this.m_supportGroups, ref Singleton<SimulationManager>.get_instance().m_randomizer);
				if (eventInfo != null)
				{
					ushort num;
					Singleton<EventManager>.get_instance().CreateEvent(out num, buildingID, eventInfo);
				}
			}
		}
	}

	public override void EndRelocating(ushort buildingID, ref Building data)
	{
		base.EndRelocating(buildingID, ref data);
		if (this.RequireRoadAccess())
		{
			Singleton<BuildingManager>.get_instance().RoadCheckNeeded(buildingID);
		}
		Notification.Problem problems = data.m_problems;
		if (data.m_productionRate != 0)
		{
			data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.TurnedOff);
		}
		else
		{
			data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.TurnedOff);
		}
		if (data.m_problems != problems)
		{
			Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, data.m_problems);
		}
		if (data.m_eventIndex != 0 && (this.m_supportEvents & EventManager.EventType.SecondaryLocation) == (EventManager.EventType)0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			EventInfo info = instance.m_events.m_buffer[(int)data.m_eventIndex].Info;
			if (info != null)
			{
				info.m_eventAI.BuildingRelocated(data.m_eventIndex, ref instance.m_events.m_buffer[(int)data.m_eventIndex]);
			}
		}
	}

	public override void SimulationStep(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		base.SimulationStep(buildingID, ref buildingData, ref frameData);
		if ((buildingData.m_flags & Building.Flags.Completed) != Building.Flags.None)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			byte district = instance.GetDistrict(buildingData.m_position);
			District[] expr_43_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_43_cp_0_cp_1 = district;
			expr_43_cp_0_cp_0[(int)expr_43_cp_0_cp_1].m_playerData.m_tempBuildingCount = expr_43_cp_0_cp_0[(int)expr_43_cp_0_cp_1].m_playerData.m_tempBuildingCount + 1;
			District[] expr_67_cp_0_cp_0 = instance.m_districts.m_buffer;
			byte expr_67_cp_0_cp_1 = district;
			expr_67_cp_0_cp_0[(int)expr_67_cp_0_cp_1].m_playerData.m_tempBuildingArea = expr_67_cp_0_cp_0[(int)expr_67_cp_0_cp_1].m_playerData.m_tempBuildingArea + (uint)(buildingData.Width * buildingData.Length);
			if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage == 255)
			{
				District[] expr_B7_cp_0_cp_0 = instance.m_districts.m_buffer;
				byte expr_B7_cp_0_cp_1 = district;
				expr_B7_cp_0_cp_0[(int)expr_B7_cp_0_cp_1].m_playerData.m_tempBurnedCount = expr_B7_cp_0_cp_0[(int)expr_B7_cp_0_cp_1].m_playerData.m_tempBurnedCount + 1;
			}
			else if ((buildingData.m_flags & Building.Flags.Collapsed) != Building.Flags.None && frameData.m_fireDamage != 255)
			{
				District[] expr_101_cp_0_cp_0 = instance.m_districts.m_buffer;
				byte expr_101_cp_0_cp_1 = district;
				expr_101_cp_0_cp_0[(int)expr_101_cp_0_cp_1].m_playerData.m_tempCollapsedCount = expr_101_cp_0_cp_0[(int)expr_101_cp_0_cp_1].m_playerData.m_tempCollapsedCount + 1;
			}
		}
		if ((ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 8 & 15u) == (ulong)((long)(buildingID & 15)))
		{
			int constructionCost = this.GetConstructionCost();
			if (constructionCost != 0)
			{
				StatisticBase statisticBase = Singleton<StatisticsManager>.get_instance().Acquire<StatisticInt64>(StatisticType.CityValue);
				if (statisticBase != null)
				{
					statisticBase.Add(constructionCost);
				}
			}
		}
		if ((buildingData.m_flags & Building.Flags.Active) == Building.Flags.None)
		{
			buildingData.m_flags &= ~Building.Flags.EventActive;
			base.EmptyBuilding(buildingID, ref buildingData, CitizenUnit.Flags.Created, false);
		}
		else if (buildingData.m_eventIndex != 0)
		{
			if ((Singleton<EventManager>.get_instance().m_events.m_buffer[(int)buildingData.m_eventIndex].m_flags & EventData.Flags.Active) != EventData.Flags.None)
			{
				buildingData.m_flags |= Building.Flags.EventActive;
			}
			else
			{
				buildingData.m_flags &= ~Building.Flags.EventActive;
			}
		}
	}

	public int GetBudget(ushort buildingID, ref Building buildingData)
	{
		ushort eventIndex = buildingData.m_eventIndex;
		if (eventIndex != 0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			EventInfo info = instance.m_events.m_buffer[(int)eventIndex].Info;
			return info.m_eventAI.GetBudget(eventIndex, ref instance.m_events.m_buffer[(int)eventIndex]);
		}
		return Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
	}

	public int GetAverageBudget(ushort buildingID, ref Building buildingData)
	{
		ushort eventIndex = buildingData.m_eventIndex;
		if (eventIndex != 0)
		{
			EventManager instance = Singleton<EventManager>.get_instance();
			EventInfo info = instance.m_events.m_buffer[(int)eventIndex].Info;
			return info.m_eventAI.GetBudget(eventIndex, ref instance.m_events.m_buffer[(int)eventIndex]);
		}
		int num = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class, false) * SimulationManager.RELATIVE_DAY_LENGTH;
		int num2 = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class, true) * SimulationManager.RELATIVE_NIGHT_LENGTH;
		return (num + num2) / (SimulationManager.RELATIVE_DAY_LENGTH + SimulationManager.RELATIVE_NIGHT_LENGTH);
	}

	protected override void SimulationStepActive(ushort buildingID, ref Building buildingData, ref Building.Frame frameData)
	{
		int productionRate = (int)buildingData.m_productionRate;
		int num = productionRate;
		if ((buildingData.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = this.GetBudget(buildingID, ref buildingData);
		int averageBudget = this.GetAverageBudget(buildingID, ref buildingData);
		int num2 = this.GetMaintenanceCost() / 100;
		num2 = num * budget / 100 * num2 / 100;
		productionRate = PlayerBuildingAI.GetProductionRate(productionRate, averageBudget);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		DistrictManager instance = Singleton<DistrictManager>.get_instance();
		byte district = instance.GetDistrict(buildingData.m_position);
		DistrictPolicies.Services servicePolicies = instance.m_districts.m_buffer[(int)district].m_servicePolicies;
		if (buildingData.m_productionRate != 0)
		{
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.TurnedOff);
		}
		else
		{
			buildingData.m_problems = Notification.AddProblems(buildingData.m_problems, Notification.Problem.TurnedOff);
		}
		if (buildingData.m_fireIntensity != 0)
		{
			num = 0;
		}
		if ((buildingData.m_flags & Building.Flags.Original) == Building.Flags.None && num2 != 0)
		{
			int num3 = Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Maintenance, num2, this.m_info.m_class);
			if (num3 < num2)
			{
				num = num3 * 100 / num2;
			}
		}
		int num4 = num * this.m_electricityConsumption / 100;
		int waterConsumption = num * this.m_waterConsumption / 100;
		int sewageAccumulation = num * this.m_sewageAccumulation / 100;
		int num5 = num * this.m_garbageAccumulation / 100;
		int heatingConsumption = 0;
		if (num4 != 0 && instance.IsPolicyLoaded(DistrictPolicies.Policies.ExtraInsulation))
		{
			if ((servicePolicies & DistrictPolicies.Services.ExtraInsulation) != DistrictPolicies.Services.None)
			{
				heatingConsumption = Mathf.Max(1, num4 * 3 + 8 >> 4);
			}
			else
			{
				heatingConsumption = Mathf.Max(1, num4 + 2 >> 2);
			}
		}
		if (num5 != 0 && (servicePolicies & DistrictPolicies.Services.Recycling) != DistrictPolicies.Services.None)
		{
			num5 = Mathf.Max(1, num5 * 85 / 100);
		}
		Citizen.BehaviourData behaviourData = default(Citizen.BehaviourData);
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		int num11 = 0;
		this.HandleWorkAndVisitPlaces(buildingID, ref buildingData, ref behaviourData, ref num6, ref num7, ref num8, ref num9, ref num10, ref num11);
		bool flag = (buildingData.m_flags & Building.Flags.Active) != Building.Flags.None;
		if (num != 0)
		{
			int num12 = base.HandleCommonConsumption(buildingID, ref buildingData, ref frameData, ref num4, ref heatingConsumption, ref waterConsumption, ref sewageAccumulation, ref num5, servicePolicies);
			num = (num * num12 + 99) / 100;
			if (num != 0)
			{
				if (num12 < 100)
				{
					buildingData.m_flags |= Building.Flags.RateReduced;
				}
				else
				{
					buildingData.m_flags &= ~Building.Flags.RateReduced;
				}
				this.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, num, ref behaviourData, num6, num7, num8, num9, num10, num11);
			}
			else
			{
				buildingData.m_flags &= ~Building.Flags.RateReduced;
				this.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, num, ref behaviourData, num6, num7, num8, num9, num10, num11);
			}
			if (num6 != 0)
			{
				float radius = (float)(buildingData.Width + buildingData.Length) * 2.5f;
				Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.Density, num6, buildingData.m_position, radius);
			}
		}
		else
		{
			num4 = 0;
			heatingConsumption = 0;
			waterConsumption = 0;
			sewageAccumulation = 0;
			num5 = 0;
			buildingData.m_problems = Notification.RemoveProblems(buildingData.m_problems, Notification.Problem.Electricity | Notification.Problem.Water | Notification.Problem.DirtyWater | Notification.Problem.Pollution | Notification.Problem.Sewage | Notification.Problem.Death | Notification.Problem.Noise | Notification.Problem.Flood | Notification.Problem.Heating);
			buildingData.m_flags &= ~Building.Flags.RateReduced;
			base.CollapseIfFlooded(buildingID, ref buildingData, ref frameData);
			this.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, num, ref behaviourData, num6, num7, num8, num9, num10, num11);
		}
		if (buildingData.m_eventIndex != 0 && (this.m_supportEvents & EventManager.EventType.SecondaryLocation) == (EventManager.EventType)0 && (buildingData.m_flags & Building.Flags.Active) == Building.Flags.None && flag)
		{
			EventManager instance2 = Singleton<EventManager>.get_instance();
			EventInfo info = instance2.m_events.m_buffer[(int)buildingData.m_eventIndex].Info;
			if (info != null)
			{
				info.m_eventAI.BuildingDeactivated(buildingData.m_eventIndex, ref instance2.m_events.m_buffer[(int)buildingData.m_eventIndex]);
			}
		}
		int num13 = num6 + num9;
		int num14 = behaviourData.m_crimeAccumulation / 10;
		if (num13 > 255)
		{
			num14 = (num14 * 255 + (num13 >> 1)) / num13;
			num13 = 255;
		}
		buildingData.m_citizenCount = (byte)num13;
		if ((servicePolicies & DistrictPolicies.Services.RecreationalUse) != DistrictPolicies.Services.None)
		{
			num14 = num14 * 3 + 3 >> 2;
		}
		base.HandleCrime(buildingID, ref buildingData, num14, (int)buildingData.m_citizenCount);
		int num15 = (int)buildingData.m_crimeBuffer;
		if (buildingData.m_citizenCount != 0)
		{
			num15 = (num15 + (buildingData.m_citizenCount >> 1)) / (int)buildingData.m_citizenCount;
		}
		else
		{
			num15 = 0;
		}
		if (num6 != 0 || num9 != 0)
		{
			int num16 = behaviourData.m_educated0Count * 50 + behaviourData.m_educated1Count * 25 + behaviourData.m_educated2Count * 15;
			num16 = num16 / (num6 + num9) + 50;
			num16 *= this.m_fireHazard;
			buildingData.m_fireHazard = (byte)num16;
		}
		else
		{
			buildingData.m_fireHazard = 0;
		}
		instance.m_districts.m_buffer[(int)district].AddPlayerData(ref behaviourData, num15, num8, num6, Mathf.Max(0, num8 - num7), num11, num9, Mathf.Max(0, num11 - num10), num4, heatingConsumption, waterConsumption, sewageAccumulation, num5, num2, Mathf.Min(100, (int)(buildingData.m_garbageBuffer / 50)), (int)(buildingData.m_waterPollution * 100 / 255));
		base.SimulationStepActive(buildingID, ref buildingData, ref frameData);
		base.HandleFire(buildingID, ref buildingData, ref frameData, servicePolicies);
		if (buildingData.m_fireIntensity != 0)
		{
			GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
			if (properties != null)
			{
				Singleton<BuildingManager>.get_instance().m_buildingOnFire.Activate(properties.m_buildingOnFire, buildingID);
			}
		}
	}

	public override bool GetFireParameters(ushort buildingID, ref Building buildingData, out int fireHazard, out int fireSize, out int fireTolerance)
	{
		fireHazard = (int)(((buildingData.m_flags & Building.Flags.Active) != Building.Flags.None) ? buildingData.m_fireHazard : 0);
		fireSize = 255;
		fireTolerance = this.m_fireTolerance;
		return this.m_fireHazard != 0;
	}

	protected virtual void HandleWorkAndVisitPlaces(ushort buildingID, ref Building buildingData, ref Citizen.BehaviourData behaviour, ref int aliveWorkerCount, ref int totalWorkerCount, ref int workPlaceCount, ref int aliveVisitorCount, ref int totalVisitorCount, ref int visitPlaceCount)
	{
	}

	protected virtual void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		if (finalProductionRate != 0)
		{
			buildingData.m_flags |= Building.Flags.Active;
			if (this.m_supportEvents != (EventManager.EventType)0 || buildingData.m_eventIndex != 0)
			{
				this.CheckEvents(buildingID, ref buildingData);
			}
		}
		else
		{
			buildingData.m_flags &= ~Building.Flags.Active;
		}
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return !this.m_info.m_anyMeshRequireWaterMap && base.CanUseGroupMesh(buildingID, ref buildingData);
	}

	private void CheckEvents(ushort buildingID, ref Building buildingData)
	{
		EventManager instance = Singleton<EventManager>.get_instance();
		if ((this.m_supportEvents & EventManager.EventType.SecondaryLocation) != (EventManager.EventType)0)
		{
			if (buildingData.m_eventIndex != 0)
			{
				ushort building = instance.m_events.m_buffer[(int)buildingData.m_eventIndex].m_building;
				if (building == 0 || Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)building].m_eventIndex != buildingData.m_eventIndex)
				{
					instance.RemoveEventLocation(buildingData.m_eventIndex, buildingID);
					buildingData.m_eventIndex = 0;
				}
			}
			if (buildingData.m_eventIndex == 0)
			{
				buildingData.m_eventIndex = Singleton<EventManager>.get_instance().FindEvent(this.m_supportEvents, this.m_supportGroups);
				if (buildingData.m_eventIndex != 0)
				{
					instance.AddEventLocation(buildingData.m_eventIndex, buildingID);
				}
			}
		}
		else
		{
			ushort num = 0;
			ushort num2 = 0;
			ushort num3 = buildingData.m_eventIndex;
			int num4 = 0;
			EventInfo prevEvent = null;
			uint prevExpireFrame = 0u;
			if (num3 != 0)
			{
				prevEvent = instance.m_events.m_buffer[(int)num3].Info;
				prevExpireFrame = instance.m_events.m_buffer[(int)num3].m_expireFrame;
			}
			int num5 = 0;
			while (num3 != 0)
			{
				if ((instance.m_events.m_buffer[(int)num3].m_flags & (EventData.Flags.Completed | EventData.Flags.Cancelled)) == EventData.Flags.None)
				{
					num = num3;
				}
				else if (num2 == 0 || instance.m_events.m_buffer[(int)num2].m_startFrame > instance.m_events.m_buffer[(int)num3].m_startFrame)
				{
					num2 = num3;
				}
				num4++;
				num3 = instance.m_events.m_buffer[(int)num3].m_nextBuildingEvent;
				if (++num5 > 256)
				{
					CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
					break;
				}
			}
			if (num == 0)
			{
				EventInfo info;
				uint num6;
				if (this.FindNextEventInfo(buildingID, prevEvent, prevExpireFrame, out info, out num6) && instance.CreateEvent(out num3, buildingID, info))
				{
					if (num4 >= 7 && num2 != 0)
					{
						instance.ReleaseEvent(num2);
					}
					if (num2 == 0)
					{
						Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, ref buildingData, true);
					}
				}
			}
			else if (num4 > 7 && num2 != 0)
			{
				instance.ReleaseEvent(num2);
			}
		}
	}

	public override bool FindNextEventInfo(ushort buildingID, EventInfo prevEvent, uint prevExpireFrame, out EventInfo nextEvent, out uint nextStartFrame)
	{
		uint num = (uint)buildingID ^ prevExpireFrame >> 16;
		if (prevEvent != null)
		{
			num ^= (uint)(prevEvent.m_prefabDataIndex + 1);
		}
		Randomizer randomizer;
		randomizer..ctor(num);
		nextEvent = Singleton<EventManager>.get_instance().FindEventInfo(this.m_supportEvents, this.m_supportGroups, ref randomizer, prevEvent);
		if (nextEvent != null)
		{
			nextStartFrame = nextEvent.m_eventAI.CalculateStartFrame(prevExpireFrame, false);
			return true;
		}
		nextStartFrame = 0u;
		return false;
	}

	public override bool ClearOccupiedZoning()
	{
		return true;
	}

	public override void SetProductionRate(ushort buildingID, ref Building data, byte rate)
	{
		if (data.m_productionRate != rate)
		{
			if ((data.m_flags & Building.Flags.Collapsed) != Building.Flags.None)
			{
				data.m_productionRate = rate;
				return;
			}
			Building.Flags flags = data.m_flags;
			if (data.m_productionRate == 0)
			{
				this.ManualActivation(buildingID, ref data);
			}
			else if (rate == 0)
			{
				this.ManualDeactivation(buildingID, ref data);
				this.BuildingDeactivated(buildingID, ref data);
			}
			data.m_productionRate = rate;
			Notification.Problem problems = data.m_problems;
			if (data.m_productionRate != 0)
			{
				data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.TurnedOff);
			}
			else
			{
				data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.TurnedOff);
			}
			if (data.m_problems != problems)
			{
				Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, data.m_problems);
			}
			base.SetProductionRate(buildingID, ref data, rate);
			Building.Flags flags2 = data.m_flags;
			Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, false);
			if (flags2 != flags)
			{
				Singleton<BuildingManager>.get_instance().UpdateFlags(buildingID, flags2 ^ flags);
			}
			Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		}
	}

	protected override void SetEvacuating(ushort buildingID, ref Building data, bool evacuating)
	{
		base.SetEvacuating(buildingID, ref data, evacuating);
		Singleton<BuildingManager>.get_instance().UpdateBuildingRenderer(buildingID, false);
		Singleton<CoverageManager>.get_instance().CoverageUpdated(this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors toolErrors = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		if (this.IsWonder() && relocateID == 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			FastList<ushort> serviceBuildings = instance.GetServiceBuildings(this.m_info.m_class.m_service);
			for (int i = 0; i < serviceBuildings.m_size; i++)
			{
				ushort num = serviceBuildings.m_buffer[i];
				if (num != 0)
				{
					BuildingInfo info = instance.m_buildings.m_buffer[(int)num].Info;
					if (info == this.m_info)
					{
						toolErrors |= ToolBase.ToolErrors.AlreadyExists;
					}
				}
			}
		}
		return toolErrors;
	}

	public override void CheckRoadAccess(ushort buildingID, ref Building data)
	{
		bool flag = true;
		if ((data.m_flags & Building.Flags.Collapsed) == Building.Flags.None && this.RequireRoadAccess())
		{
			Vector3 position;
			if (this.m_info.m_zoningMode == BuildingInfo.ZoningMode.CornerLeft)
			{
				position = data.CalculateSidewalkPosition((float)data.Width * 4f, 4f);
			}
			else if (this.m_info.m_zoningMode == BuildingInfo.ZoningMode.CornerRight)
			{
				position = data.CalculateSidewalkPosition((float)data.Width * -4f, 4f);
			}
			else
			{
				position = data.CalculateSidewalkPosition(0f, 4f);
			}
			if (PlayerBuildingAI.FindRoadAccess(position))
			{
				flag = false;
			}
		}
		else
		{
			flag = false;
		}
		Notification.Problem problems = data.m_problems;
		if (flag)
		{
			data.m_problems = Notification.AddProblems(data.m_problems, Notification.Problem.RoadNotConnected);
		}
		else
		{
			data.m_problems = Notification.RemoveProblems(data.m_problems, Notification.Problem.RoadNotConnected);
		}
		if (data.m_problems != problems)
		{
			Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, data.m_problems);
		}
	}

	private static bool FindRoadAccess(Vector3 position)
	{
		Bounds bounds;
		bounds..ctor(position, new Vector3(40f, 40f, 40f));
		int num = Mathf.Max((int)((bounds.get_min().x - 64f) / 64f + 135f), 0);
		int num2 = Mathf.Max((int)((bounds.get_min().z - 64f) / 64f + 135f), 0);
		int num3 = Mathf.Min((int)((bounds.get_max().x + 64f) / 64f + 135f), 269);
		int num4 = Mathf.Min((int)((bounds.get_max().z + 64f) / 64f + 135f), 269);
		NetManager instance = Singleton<NetManager>.get_instance();
		for (int i = num2; i <= num4; i++)
		{
			for (int j = num; j <= num3; j++)
			{
				ushort num5 = instance.m_segmentGrid[i * 270 + j];
				int num6 = 0;
				while (num5 != 0)
				{
					NetInfo info = instance.m_segments.m_buffer[(int)num5].Info;
					if (info.m_class.m_service == ItemClass.Service.Road && !info.m_netAI.IsUnderground() && info.m_netAI is RoadBaseAI && info.m_hasPedestrianLanes && (info.m_hasForwardVehicleLanes || info.m_hasBackwardVehicleLanes))
					{
						ushort startNode = instance.m_segments.m_buffer[(int)num5].m_startNode;
						ushort endNode = instance.m_segments.m_buffer[(int)num5].m_endNode;
						Vector3 position2 = instance.m_nodes.m_buffer[(int)startNode].m_position;
						Vector3 position3 = instance.m_nodes.m_buffer[(int)endNode].m_position;
						float num7 = Mathf.Max(Mathf.Max(bounds.get_min().x - 64f - position2.x, bounds.get_min().z - 64f - position2.z), Mathf.Max(position2.x - bounds.get_max().x - 64f, position2.z - bounds.get_max().z - 64f));
						float num8 = Mathf.Max(Mathf.Max(bounds.get_min().x - 64f - position3.x, bounds.get_min().z - 64f - position3.z), Mathf.Max(position3.x - bounds.get_max().x - 64f, position3.z - bounds.get_max().z - 64f));
						Vector3 vector;
						int num9;
						float num10;
						Vector3 vector2;
						int num11;
						float num12;
						if ((num7 < 0f || num8 < 0f) && instance.m_segments.m_buffer[(int)num5].m_bounds.Intersects(bounds) && instance.m_segments.m_buffer[(int)num5].GetClosestLanePosition(position, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, VehicleInfo.VehicleType.Car, VehicleInfo.VehicleType.None, false, out vector, out num9, out num10, out vector2, out num11, out num12))
						{
							float num13 = Vector3.SqrMagnitude(position - vector);
							if (num13 < 400f)
							{
								return true;
							}
						}
					}
					num5 = instance.m_segments.m_buffer[(int)num5].m_nextGridSegment;
					if (++num6 >= 36864)
					{
						CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
						break;
					}
				}
			}
		}
		return false;
	}

	public static int GetProductionRate(int productionRate, int budget)
	{
		if (budget < 100)
		{
			budget = (budget * budget + 99) / 100;
		}
		else if (budget > 150)
		{
			budget = 125;
		}
		else if (budget > 100)
		{
			budget -= (100 - budget) * (100 - budget) / 100;
		}
		return (productionRate * budget + 99) / 100;
	}

	public override int GetConstructionCost()
	{
		int result = this.m_constructionCost * 100;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetConstructionCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override int GetMaintenanceCost()
	{
		int result = this.m_maintenanceCost * 100;
		Singleton<EconomyManager>.get_instance().m_EconomyWrapper.OnGetMaintenanceCost(ref result, this.m_info.m_class.m_service, this.m_info.m_class.m_subService, this.m_info.m_class.m_level);
		return result;
	}

	public override int GetWaterConsumption()
	{
		return this.m_waterConsumption;
	}

	public override int GetElectricityConsumption()
	{
		return this.m_electricityConsumption;
	}

	public override string GetLocalizedTooltip()
	{
		int num;
		int num2;
		this.GetPollutionAccumulation(out num, out num2);
		return TooltipHelper.Append(base.GetLocalizedTooltip(), TooltipHelper.Format(new string[]
		{
			LocaleFormatter.Pollution,
			LocaleFormatter.FormatGeneric("AIINFO_POLLUTION", new object[]
			{
				num
			}),
			LocaleFormatter.NoisePollution,
			LocaleFormatter.FormatGeneric("AIINFO_NOISEPOLLUTION", new object[]
			{
				num2
			})
		}));
	}

	public override bool CanBeRelocated(ushort buildingID, ref Building data)
	{
		return true;
	}

	public override MilestoneInfo GetUnlockMilestone()
	{
		CombinedMilestone combinedMilestone = this.m_info.m_UnlockMilestone as CombinedMilestone;
		if (combinedMilestone != null && combinedMilestone.m_requirePassed != null && combinedMilestone.m_requirePassed.Length == 1 && combinedMilestone.m_requirePassedLimit == 1 && combinedMilestone.m_delayWeeks == 0)
		{
			MilestoneInfo milestoneInfo = combinedMilestone.m_requirePassed[0];
			if (milestoneInfo != null && milestoneInfo.m_openUnlockPanel)
			{
				return milestoneInfo;
			}
		}
		return this.m_info.m_UnlockMilestone;
	}

	public virtual bool RequireRoadAccess()
	{
		return this.m_garbageAccumulation != 0;
	}

	public override bool SupportEvents(EventManager.EventType types, EventManager.EventGroup groups)
	{
		return (this.m_supportEvents & EventManager.EventType.SecondaryLocation) == (EventManager.EventType)0 && (this.m_supportEvents & types) != (EventManager.EventType)0 && (this.m_supportGroups & groups) != (EventManager.EventGroup)0;
	}

	public MessageInfo m_onCompleteMessage;

	public ManualMilestone m_createPassMilestone;

	public ManualMilestone m_createPassMilestone2;

	public ManualMilestone m_createPassMilestone3;

	[CustomizableProperty("Construction Cost", "Gameplay common")]
	public int m_constructionCost = 1000;

	[CustomizableProperty("Maintenance Cost", "Gameplay common")]
	public int m_maintenanceCost = 100;

	[CustomizableProperty("Electricity Consumption", "Electricity")]
	public int m_electricityConsumption = 10;

	[CustomizableProperty("Water Consumption", "Water")]
	public int m_waterConsumption = 10;

	[CustomizableProperty("Sewage Accumulation", "Water")]
	public int m_sewageAccumulation = 10;

	[CustomizableProperty("Garbage Accumulation", "Gameplay common")]
	public int m_garbageAccumulation = 10;

	[CustomizableProperty("Fire Hazard", "Gameplay common")]
	public int m_fireHazard = 1;

	[CustomizableProperty("Fire Tolerance", "Gameplay common")]
	public int m_fireTolerance = 20;

	[BitMask]
	public EventManager.EventType m_supportEvents;

	[BitMask]
	public EventManager.EventGroup m_supportGroups;

	public BuildingInfo m_upgradeTarget;
}
