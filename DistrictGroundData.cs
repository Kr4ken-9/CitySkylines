﻿using System;
using ColossalFramework.IO;

public struct DistrictGroundData
{
	public void Add(ref DistrictGroundData data)
	{
		this.m_tempLandvalue += data.m_tempLandvalue;
		this.m_tempPollution += data.m_tempPollution;
		this.m_tempCoverage += data.m_tempCoverage;
	}

	public void Update()
	{
		if (this.m_tempCoverage != 0u)
		{
			this.m_finalLandvalue = (ushort)((this.m_tempLandvalue + (this.m_tempCoverage >> 1)) / this.m_tempCoverage);
			this.m_finalPollution = (byte)((this.m_tempPollution + (this.m_tempCoverage >> 1)) / this.m_tempCoverage);
		}
		else
		{
			this.m_finalLandvalue = 0;
		}
	}

	public void Reset()
	{
		this.m_tempLandvalue = 0u;
		this.m_tempPollution = 0u;
		this.m_tempCoverage = 0u;
	}

	public void Serialize(DataSerializer s)
	{
		s.WriteUInt32(this.m_tempLandvalue);
		s.WriteUInt32(this.m_tempCoverage);
		s.WriteUInt16((uint)this.m_finalLandvalue);
		s.WriteUInt32(this.m_tempPollution);
		s.WriteUInt8((uint)this.m_finalPollution);
	}

	public void Deserialize(DataSerializer s)
	{
		this.m_tempLandvalue = s.ReadUInt32();
		this.m_tempCoverage = s.ReadUInt32();
		this.m_finalLandvalue = (ushort)s.ReadUInt16();
		if (s.get_version() >= 108u)
		{
			this.m_tempPollution = s.ReadUInt32();
			this.m_finalPollution = (byte)s.ReadUInt8();
		}
		else
		{
			this.m_tempPollution = 0u;
			this.m_finalPollution = 0;
		}
	}

	public uint m_tempLandvalue;

	public uint m_tempPollution;

	public uint m_tempCoverage;

	public ushort m_finalLandvalue;

	public byte m_finalPollution;
}
