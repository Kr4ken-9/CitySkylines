﻿using System;
using System.IO;
using ColossalFramework.Packaging;

public static class WorkshopHelper
{
	public static void VerifyAndFinalizeFiles(string[] files, string publishedID)
	{
		for (int i = 0; i < files.Length; i++)
		{
			string path = files[i];
			if (string.Compare(Path.GetExtension(path), PackageManager.packageExtension, StringComparison.OrdinalIgnoreCase) == 0)
			{
				WorkshopHelper.VerifyAndFinalizePackage(path, publishedID);
			}
		}
	}

	public static void VerifyAndFinalizePackage(string path, string publishedID)
	{
		Package package = new Package(publishedID, path, true);
		package.Save(true);
	}

	public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
	{
		DirectoryInfo directoryInfo = new DirectoryInfo(sourceDirName);
		DirectoryInfo[] directories = directoryInfo.GetDirectories();
		if (!directoryInfo.Exists)
		{
			throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
		}
		if (!Directory.Exists(destDirName))
		{
			Directory.CreateDirectory(destDirName);
		}
		FileInfo[] files = directoryInfo.GetFiles();
		FileInfo[] array = files;
		for (int i = 0; i < array.Length; i++)
		{
			FileInfo fileInfo = array[i];
			string destFileName = Path.Combine(destDirName, fileInfo.Name);
			fileInfo.CopyTo(destFileName, false);
		}
		if (copySubDirs)
		{
			DirectoryInfo[] array2 = directories;
			for (int j = 0; j < array2.Length; j++)
			{
				DirectoryInfo directoryInfo2 = array2[j];
				string destDirName2 = Path.Combine(destDirName, directoryInfo2.Name);
				WorkshopHelper.DirectoryCopy(directoryInfo2.FullName, destDirName2, copySubDirs);
			}
		}
	}
}
