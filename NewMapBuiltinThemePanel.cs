﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Packaging;
using ColossalFramework.PlatformServices;
using ColossalFramework.UI;
using UnityEngine;

public class NewMapBuiltinThemePanel : LoadSavePanelBase<SystemMapMetaData>
{
	public int selectedIndex
	{
		get
		{
			return this.m_SelectedIndex;
		}
		set
		{
			if (this.m_SelectedIndex != value)
			{
				UIComponent c = (this.m_SelectedIndex < 0 || this.m_SelectedIndex >= this.m_Container.get_childCount()) ? null : this.m_Container.get_components()[this.m_SelectedIndex];
				if (c != null)
				{
					ValueAnimator.Animate("NewMap" + this.m_SelectedIndex, delegate(float val)
					{
						if (c != null)
						{
							c.set_size(Vector2.Lerp(this.m_MinSize, this.m_MaxSize, val));
						}
					}, new AnimatedFloat(1f, 0f, this.m_InterpSpeed));
				}
				UIComponent c2 = (value < 0 || value >= this.m_Container.get_childCount()) ? null : this.m_Container.get_components()[value];
				if (c2 != null)
				{
					this.m_SelectedIndex = value;
					this.m_ThemeName.set_text(base.GetListingItem(this.m_SelectedIndex));
					this.m_Previous.set_isEnabled(this.m_SelectedIndex > 0);
					this.m_Next.set_isEnabled(this.m_SelectedIndex < this.m_Container.get_childCount() - 1);
					ValueAnimator.Animate("NewMap" + this.m_SelectedIndex, delegate(float val)
					{
						if (c2 != null)
						{
							c2.set_size(Vector2.Lerp(this.m_MinSize, this.m_MaxSize, val));
						}
					}, new AnimatedFloat(0f, 1f, this.m_InterpSpeed));
					ValueAnimator.Animate("Select", delegate(float val)
					{
						this.m_Container.set_scrollPosition(new Vector2(val, 0f));
					}, new AnimatedFloat(this.m_Container.get_scrollPosition().x, this.CalculatePosition(this.m_SelectedIndex) - this.m_Container.get_size().x * 0.5f, this.m_InterpSpeed));
				}
			}
		}
	}

	public void ForceSelectedIndex(int index)
	{
		if (this.m_SelectedIndex != index)
		{
			UIComponent uIComponent = (this.m_SelectedIndex < 0 || this.m_SelectedIndex >= this.m_Container.get_childCount()) ? null : this.m_Container.get_components()[this.m_SelectedIndex];
			if (uIComponent != null)
			{
				uIComponent.set_size(this.m_MinSize);
			}
		}
		UIComponent uIComponent2 = (index < 0 || index >= this.m_Container.get_childCount()) ? null : this.m_Container.get_components()[index];
		if (uIComponent2 != null)
		{
			this.m_SelectedIndex = index;
			this.m_ThemeName.set_text(base.GetListingItem(this.m_SelectedIndex));
			this.m_Previous.set_isEnabled(this.m_SelectedIndex > 0);
			this.m_Next.set_isEnabled(this.m_SelectedIndex < this.m_Container.get_childCount() - 1);
			uIComponent2.set_size(this.m_MaxSize);
			this.m_Container.set_scrollPosition(new Vector2(this.CalculatePosition(this.m_SelectedIndex) - this.m_Container.get_size().x * 0.5f, 0f));
		}
	}

	private float CalculatePosition(int index)
	{
		return (this.m_MinSize.x + (float)this.m_Container.get_autoLayoutPadding().get_horizontal()) * (float)index + (this.m_MaxSize.x + (float)this.m_Container.get_autoLayoutPadding().get_horizontal()) * 0.5f;
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_Create = base.Find<UIButton>("Create");
		this.m_ThemeName = base.Find<UILabel>("ThemeName");
		this.m_Container = base.Find<UIScrollablePanel>("Container");
		this.m_Container.add_eventMouseWheel(new MouseEventHandler(this.OnContainerMouseWheel));
		this.m_Previous = base.Find<UIButton>("Prev");
		this.m_Previous.add_eventClick(new MouseEventHandler(this.OnSelectPrevious));
		this.m_Previous.add_eventDoubleClick(new MouseEventHandler(this.OnSelectPrevious));
		this.m_Next = base.Find<UIButton>("Next");
		this.m_Next.add_eventClick(new MouseEventHandler(this.OnSelectNext));
		this.m_Next.add_eventDoubleClick(new MouseEventHandler(this.OnSelectNext));
		this.Refresh();
	}

	public void OnContainerMouseWheel(UIComponent c, UIMouseEventParameter p)
	{
		this.m_AccumulatedMouseWheelDelta += p.get_wheelDelta();
		if (this.m_AccumulatedMouseWheelDelta <= -1f)
		{
			this.SelectNext();
			this.m_AccumulatedMouseWheelDelta = 0f;
		}
		else if (this.m_AccumulatedMouseWheelDelta >= 1f)
		{
			this.SelectPrevious();
			this.m_AccumulatedMouseWheelDelta = 0f;
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.Refresh();
			base.get_component().Focus();
		}
	}

	public void OnSelectNext(UIComponent comp, UIMouseEventParameter p)
	{
		if (!p.get_used())
		{
			this.SelectNext();
			p.Use();
		}
	}

	public void OnSelectPrevious(UIComponent comp, UIMouseEventParameter p)
	{
		if (!p.get_used())
		{
			this.SelectPrevious();
			p.Use();
		}
	}

	private void SelectPrevious()
	{
		if (this.selectedIndex > 0)
		{
			this.selectedIndex--;
		}
	}

	private void SelectNext()
	{
		if (this.selectedIndex < this.m_Container.get_childCount() - 1)
		{
			this.selectedIndex++;
		}
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used())
		{
			if (p.get_keycode() == 275)
			{
				this.SelectNext();
				p.Use();
			}
			else if (p.get_keycode() == 276)
			{
				this.SelectPrevious();
				p.Use();
			}
			else if (p.get_keycode() == 13)
			{
				this.OnCreate();
				p.Use();
			}
		}
	}

	protected override void Refresh()
	{
		using (AutoProfile.Start("NewMapPanel.Refresh()"))
		{
			UITemplateManager.ClearInstances("SnapShotBuiltin");
			base.ClearListing();
			bool flag = SteamHelper.IsDLCOwned(SteamHelper.DLC.SnowFallDLC);
			foreach (Package.Asset current in PackageManager.FilterAssets(new Package.AssetType[]
			{
				UserAssetType.SystemMapMetaData
			}))
			{
				if (current != null && current.get_isEnabled())
				{
					SystemMapMetaData systemMapMetaData = current.Instantiate<SystemMapMetaData>();
					if (systemMapMetaData.environment != "Winter" || flag)
					{
						string text = systemMapMetaData.environment;
						if (Locale.Exists("THEME_NAME", text))
						{
							text = Locale.Get("THEME_NAME", text);
						}
						base.AddToListing(text, current, systemMapMetaData);
						GameObject asGameObject = UITemplateManager.GetAsGameObject("SnapShotBuiltin");
						UITextureSprite uITextureSprite = this.m_Container.AttachUIComponent(asGameObject) as UITextureSprite;
						uITextureSprite.add_eventClick(delegate(UIComponent c, UIMouseEventParameter p)
						{
							this.selectedIndex = p.get_source().get_zOrder();
						});
						uITextureSprite.set_size(this.m_MinSize);
						if (systemMapMetaData.imageRef != null)
						{
							uITextureSprite.set_texture(systemMapMetaData.imageRef.Instantiate<Texture>());
							uITextureSprite.get_texture().set_wrapMode(1);
						}
					}
				}
			}
			if (base.GetListingCount() == 0)
			{
				this.m_ThemeName.set_text(Locale.Get("NEWMAP_NOTHEME"));
				this.m_Create.set_isEnabled(false);
			}
			else
			{
				this.m_Create.set_isEnabled(true);
			}
			this.m_Previous.set_isEnabled(false);
			this.m_Next.set_isEnabled(false);
			this.ForceSelectedIndex(0);
		}
	}

	public void OnCreate()
	{
		base.CloseEverything();
		SimulationMetaData simulationMetaData = new SimulationMetaData();
		simulationMetaData.m_gameInstanceIdentifier = Guid.NewGuid().ToString();
		simulationMetaData.m_WorkshopPublishedFileId = PublishedFileId.invalid;
		simulationMetaData.m_newMapAppVersion = 172090642u;
		simulationMetaData.m_updateMode = SimulationManager.UpdateMode.NewMap;
		Singleton<LoadingManager>.get_instance().LoadLevel(base.GetListingData(this.selectedIndex), "MapEditor", "InMapEditor", simulationMetaData);
		UIView.get_library().Hide("NewMapPanel", 1);
	}

	private const string kSnapShot = "SnapShotBuiltin";

	public float m_InterpSpeed = 0.18f;

	public Vector2 m_MinSize = new Vector2(157f, 114f);

	public Vector2 m_MaxSize = new Vector2(368f, 267f);

	private UIButton m_Create;

	private UILabel m_ThemeName;

	private UIScrollablePanel m_Container;

	private UIButton m_Previous;

	private UIButton m_Next;

	private int m_SelectedIndex;

	private float m_AccumulatedMouseWheelDelta;
}
