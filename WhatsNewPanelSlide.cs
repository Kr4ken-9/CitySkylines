﻿using System;
using ColossalFramework.UI;

public class WhatsNewPanelSlide : UICustomControl
{
	public bool shouldShow
	{
		get
		{
			return SteamHelper.IsDLCOwned(this.m_DLC);
		}
	}

	public SteamHelper.DLC m_DLC;
}
