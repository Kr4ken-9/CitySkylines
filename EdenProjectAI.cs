﻿using System;
using ColossalFramework;

public class EdenProjectAI : ParkAI
{
	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		int num = (productionRate * this.m_landValueAccumulation + 9) / 10;
		if (num != 0)
		{
			Singleton<ImmaterialResourceManager>.get_instance().AddResource(ImmaterialResourceManager.Resource.LandValue, num);
		}
		if (finalProductionRate == 0)
		{
			return;
		}
		if (Singleton<SimulationManager>.get_instance().m_randomizer.Int32(100u) < finalProductionRate)
		{
			Singleton<NaturalResourceManager>.get_instance().AddPollutionDisposeRate(1);
		}
		int num2 = (finalProductionRate * 16 + 99) / 100;
		if (num2 != 0)
		{
			Singleton<TerrainManager>.get_instance().WaterSimulation.AddPollutionDisposeRate(num2);
		}
	}

	public override bool IsWonder()
	{
		return true;
	}

	public override bool CanBeBuilt()
	{
		return this.m_createPassMilestone == null || this.m_createPassMilestone.GetPassCount() == 0;
	}

	public int m_landValueAccumulation = 50;
}
