﻿using System;
using UnityEngine;

public class ImportAssetCitizen : ImportAssetLodded
{
	public ImportAssetCitizen(GameObject template, PreviewCamera camera) : base(template, camera)
	{
	}

	protected override void InitializeObject()
	{
		this.m_Object.SetActive(true);
		CitizenInfo component = this.m_Object.GetComponent<CitizenInfo>();
		component.InitializePrefab();
		component.CheckReferences();
		component.m_prefabInitialized = true;
		this.m_Object.set_layer(LayerMask.NameToLayer("Citizens"));
	}

	protected override void GenerateAssetData(GameObject instance)
	{
		base.GenerateAssetData(instance);
		if (this.m_IsImportedAsset)
		{
			Animator component = this.m_TemplateObject.GetComponent<Animator>();
			Animator animator = ImportAsset.CopyComponent<Animator>(component, this.m_Object);
			animator.set_runtimeAnimatorController(component.get_runtimeAnimatorController());
			animator.set_avatar(component.get_avatar());
		}
		else
		{
			AnimatorUtility.DeoptimizeTransformHierarchy(this.m_Object);
		}
		this.m_Object.GetComponent<Animator>().set_cullingMode(0);
	}

	protected override void CreateInfo()
	{
		CitizenInfo component = this.m_TemplateObject.GetComponent<CitizenInfo>();
		CitizenInfo citizenInfo = ImportAsset.CopyComponent<CitizenInfo>(component, this.m_Object);
		citizenInfo.m_lodObject = this.m_LODObject;
		citizenInfo.m_skinRenderer = this.m_Object.GetComponentInChildren<SkinnedMeshRenderer>();
		citizenInfo.m_availableIn = ItemClass.Availability.Game;
		ImportAsset.CopyComponent<CitizenAI>(component.m_citizenAI, this.m_Object);
	}

	public override void ApplyTransform(Vector3 scale, Vector3 rotation, bool bottomPivot)
	{
	}

	protected override Mesh GetSharedMesh(GameObject go)
	{
		SkinnedMeshRenderer componentInChildren = go.GetComponentInChildren<SkinnedMeshRenderer>();
		if (componentInChildren != null)
		{
			return componentInChildren.get_sharedMesh();
		}
		return null;
	}

	protected override void SetMesh(GameObject go, Mesh mesh, bool instantiate = false)
	{
		Mesh mesh2 = mesh;
		if (instantiate)
		{
			mesh2 = UnityEngine.Object.Instantiate<Mesh>(mesh);
			mesh2.set_name(mesh.get_name());
		}
		SkinnedMeshRenderer componentInChildren = go.GetComponentInChildren<SkinnedMeshRenderer>();
		if (componentInChildren != null)
		{
			componentInChildren.set_sharedMesh(mesh2);
		}
	}

	protected override Shader GetShader(GameObject go)
	{
		SkinnedMeshRenderer componentInChildren = go.GetComponentInChildren<SkinnedMeshRenderer>();
		if (componentInChildren != null && componentInChildren.get_sharedMaterial() != null && componentInChildren.get_sharedMaterial().get_shader() != null)
		{
			return componentInChildren.get_sharedMaterial().get_shader();
		}
		return null;
	}

	protected override Renderer GetRenderer(GameObject go)
	{
		return go.GetComponentInChildren<SkinnedMeshRenderer>();
	}

	protected override Renderer[] GetRenderers(GameObject go)
	{
		return go.GetComponentsInChildren<SkinnedMeshRenderer>(true);
	}
}
