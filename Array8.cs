﻿using System;
using ColossalFramework.Math;

public class Array8<T>
{
	public Array8(uint arraySize)
	{
		this.m_size = arraySize;
		this.m_buffer = new T[arraySize];
		this.m_unusedCount = arraySize - 1u;
		this.m_unusedItems = new byte[arraySize];
		for (uint num = 1u; num < arraySize; num += 1u)
		{
			this.m_unusedItems[(int)((UIntPtr)(num - 1u))] = (byte)num;
		}
		this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount++))] = 0;
	}

	public bool CreateItem(out byte item)
	{
		if (this.m_unusedCount == 0u)
		{
			item = 0;
			return false;
		}
		item = this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount -= 1u))];
		return true;
	}

	public bool CreateItem(out byte item, ref Randomizer r)
	{
		if (this.m_unusedCount == 0u)
		{
			item = 0;
			return false;
		}
		int num = r.Int32(this.m_unusedCount);
		item = this.m_unusedItems[num];
		this.m_unusedItems[num] = this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount -= 1u))];
		return true;
	}

	public byte NextFreeItem()
	{
		if (this.m_unusedCount == 0u)
		{
			return 0;
		}
		return this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount - 1u))];
	}

	public void ReleaseItem(byte item)
	{
		this.m_unusedItems[(int)((UIntPtr)(this.m_unusedCount++))] = item;
	}

	public void ClearUnused()
	{
		this.m_unusedCount = 0u;
	}

	public uint ItemCount()
	{
		return this.m_size - this.m_unusedCount;
	}

	public uint m_size;

	public T[] m_buffer;

	private uint m_unusedCount;

	private byte[] m_unusedItems;
}
