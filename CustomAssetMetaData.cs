﻿using System;
using ColossalFramework.Packaging;

[Serializable]
public class CustomAssetMetaData : MetaData
{
	public override DateTime getTimeStamp
	{
		get
		{
			return this.timeStamp;
		}
	}

	public override string[] getTags
	{
		get
		{
			return this.steamTags;
		}
	}

	public string name;

	public DateTime timeStamp;

	public Package.Asset imageRef;

	public Package.Asset steamPreviewRef;

	public string[] steamTags;

	public string guid;

	public CustomAssetMetaData.Type type = CustomAssetMetaData.Type.Unknown;

	public ModInfo[] mods;

	public int width = -1;

	public int length = -1;

	public ItemClass.Level level = ItemClass.Level.None;

	public ItemClass.Service service;

	public ItemClass.SubService subService;

	public VehicleInfo.VehicleType vehicleType;

	public SteamHelper.DLC_BitMask dlcMask;

	public int triangles;

	public int lodTriangles;

	public int textureHeight;

	public int textureWidth;

	public int lodTextureHeight;

	public int lodTextureWidth;

	public string templateName;

	public enum Type
	{
		Building,
		Prop,
		Tree,
		Vehicle,
		Trailer,
		Unknown,
		SubBuilding,
		PropVariation,
		Citizen,
		Road,
		RoadElevation,
		Pillar
	}
}
