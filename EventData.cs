﻿using System;
using ColossalFramework;
using UnityEngine;

public struct EventData
{
	public EventInfo Info
	{
		get
		{
			return PrefabCollection<EventInfo>.GetPrefab((uint)this.m_infoIndex);
		}
		set
		{
			this.m_infoIndex = (ushort)Mathf.Clamp(value.m_prefabDataIndex, 0, 65535);
		}
	}

	public DateTime StartTime
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().FrameToTime(this.m_startFrame);
		}
	}

	public ulong m_rewardMoney;

	public ulong m_totalReward;

	public ulong m_ticketMoney;

	public ulong m_totalTicket;

	public ulong m_customSeed;

	public EventData.Flags m_flags;

	public Color32 m_color;

	public uint m_startFrame;

	public uint m_expireFrame;

	public ushort m_infoIndex;

	public ushort m_building;

	public ushort m_nextBuildingEvent;

	public ushort m_ticketPrice;

	public ushort m_successCount;

	public ushort m_failureCount;

	public ushort m_securityBudget;

	public short m_popularityDelta;

	[Flags]
	public enum Flags
	{
		None = 0,
		Created = 1,
		Deleted = 2,
		Success = 4,
		Failure = 8,
		Preparing = 16,
		Active = 32,
		Expired = 64,
		Completed = 128,
		Disorganizing = 256,
		Cancelled = 512,
		CustomSeed = 1024
	}
}
