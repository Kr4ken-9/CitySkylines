﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class ResourceWrapper : IResource
{
	public ResourceWrapper(NaturalResourceManager resourceManager)
	{
		this.m_ResourceManager = resourceManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnResourceExtensionsReleased();
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	public int gridResolution
	{
		get
		{
			return 512;
		}
	}

	public float cellSize
	{
		get
		{
			return 33.75f;
		}
	}

	public int maxDirt
	{
		get
		{
			return 524288;
		}
	}

	public void WorldToGridPosition(Vector3 position, out int x, out int z)
	{
		x = this.ClampToGrid(this.W2G(position.x));
		z = this.ClampToGrid(this.W2G(position.z));
	}

	public Vector3 GridToWorldPosition(int x, int z)
	{
		return new Vector3(this.G2W(this.ClampToGrid(x)), 0f, this.G2W(this.ClampToGrid(z)));
	}

	public byte GetResource(int x, int z, NaturalResource type)
	{
		x = this.ClampToGrid(x);
		z = this.ClampToGrid(z);
		NaturalResourceManager.ResourceCell resourceCell = this.m_ResourceManager.m_naturalResources[this.CoordToIndex(x, z)];
		switch (type)
		{
		case 0:
			return resourceCell.m_ore;
		case 1:
			return resourceCell.m_sand;
		case 2:
			return resourceCell.m_oil;
		case 3:
			return resourceCell.m_fertility;
		case 4:
			return resourceCell.m_forest;
		case 10:
			return resourceCell.m_pollution;
		case 11:
			return resourceCell.m_burned;
		case 12:
			return resourceCell.m_destroyed;
		}
		return 0;
	}

	private int CoordToIndex(int x, int z)
	{
		return z * this.gridResolution + x;
	}

	private int W2G(float pos)
	{
		return (int)(pos / this.cellSize + (float)this.gridResolution * 0.5f);
	}

	private float G2W(int pos)
	{
		return ((float)pos - (float)this.gridResolution * 0.5f) * this.cellSize + 0.5f * this.cellSize;
	}

	private int ClampToGrid(int coordinate)
	{
		return Mathf.Clamp(coordinate, 0, this.gridResolution - 1);
	}

	public void SetResource(int x, int z, NaturalResource type, byte value, bool refresh)
	{
		x = this.ClampToGrid(x);
		z = this.ClampToGrid(z);
		int num = this.CoordToIndex(x, z);
		NaturalResourceManager.ResourceCell resourceCell = this.m_ResourceManager.m_naturalResources[num];
		byte b = 0;
		switch (type)
		{
		case 0:
			b = ((resourceCell.m_ore == value) ? 0 : 1);
			resourceCell.m_ore = value;
			break;
		case 1:
			b = ((resourceCell.m_sand == value) ? 0 : 1);
			resourceCell.m_sand = value;
			break;
		case 2:
			b = ((resourceCell.m_oil == value) ? 0 : 1);
			resourceCell.m_oil = value;
			break;
		case 3:
			b = ((resourceCell.m_fertility == value) ? 0 : 1);
			resourceCell.m_fertility = value;
			break;
		case 4:
			b = ((resourceCell.m_forest == value) ? 0 : 1);
			resourceCell.m_forest = value;
			break;
		case 10:
			b = ((resourceCell.m_pollution == value) ? 0 : 2);
			resourceCell.m_pollution = value;
			break;
		case 11:
			b = ((resourceCell.m_burned == value) ? 0 : 2);
			resourceCell.m_burned = value;
			break;
		case 12:
			b = ((resourceCell.m_destroyed == value) ? 0 : 2);
			resourceCell.m_destroyed = value;
			break;
		}
		if (b != 0)
		{
			if (refresh)
			{
				this.m_ResourceManager.m_naturalResources[num] = resourceCell;
				if (b == 1)
				{
					this.m_ResourceManager.AreaModified(x, z, x, z);
				}
				else if (b == 2)
				{
					this.m_ResourceManager.AreaModifiedB(x, z, x, z);
				}
			}
			else
			{
				resourceCell.m_modified |= b;
				this.m_ResourceManager.m_naturalResources[num] = resourceCell;
			}
		}
	}

	public int GetDirt()
	{
		return Singleton<TerrainManager>.get_instance().RawDirtBuffer;
	}

	public void SetDirt(int dirt)
	{
		Singleton<TerrainManager>.get_instance().DirtBuffer = dirt;
	}

	private void GetImplementations()
	{
		this.OnResourceExtensionsReleased();
		this.m_ResourceExtensions = Singleton<PluginManager>.get_instance().GetImplementations<IResourceExtension>();
		this.OnResourceExtensionsCreated();
	}

	private void OnResourceExtensionsCreated()
	{
		for (int i = 0; i < this.m_ResourceExtensions.Count; i++)
		{
			try
			{
				this.m_ResourceExtensions[i].OnCreated(this);
			}
			catch (Exception ex2)
			{
				Type type = this.m_ResourceExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	private void OnResourceExtensionsReleased()
	{
		for (int i = 0; i < this.m_ResourceExtensions.Count; i++)
		{
			try
			{
				this.m_ResourceExtensions[i].OnReleased();
			}
			catch (Exception ex2)
			{
				Type type = this.m_ResourceExtensions[i].GetType();
				PluginManager.PluginInfo pluginInfo = Singleton<PluginManager>.get_instance().FindPluginInfo(type.Assembly);
				if (pluginInfo != null)
				{
					ModException ex = new ModException("The Mod " + pluginInfo.ToString() + " has caused an error", ex2);
					UIView.ForwardException(ex);
					Debug.LogException(ex);
				}
				else
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
			}
		}
	}

	public int OnGetDirt(int amount)
	{
		for (int i = 0; i < this.m_ResourceExtensions.Count; i++)
		{
			try
			{
				amount = this.m_ResourceExtensions[i].OnGetDirt(amount);
			}
			catch (Exception ex)
			{
				Debug.LogException(ex);
			}
		}
		return amount;
	}

	public void OnAfterResourcesModified(int x, int z, NaturalResourceManager.Resource internalType, int amount)
	{
		if (Enum.IsDefined(typeof(NaturalResource), (int)internalType))
		{
			for (int i = 0; i < this.m_ResourceExtensions.Count; i++)
			{
				try
				{
					this.m_ResourceExtensions[i].OnAfterResourcesModified(x, z, internalType, amount);
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
				}
			}
		}
	}

	public void OnBeforeResourcesModified(int x, int z, NaturalResourceManager.Resource internalType, int desiredAmount)
	{
		if (Enum.IsDefined(typeof(NaturalResource), (int)internalType))
		{
			for (int i = 0; i < this.m_ResourceExtensions.Count; i++)
			{
				try
				{
					this.m_ResourceExtensions[i].OnBeforeResourcesModified(x, z, internalType, desiredAmount);
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
				}
			}
		}
	}

	private NaturalResourceManager m_ResourceManager;

	private List<IResourceExtension> m_ResourceExtensions = new List<IResourceExtension>();
}
