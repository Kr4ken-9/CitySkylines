﻿using System;
using ColossalFramework;
using ColossalFramework.UI;
using UnityEngine;

public class ChirpOptionsPanel : UICustomControl
{
	public ChirpPanel owner
	{
		set
		{
			this.m_Owner = value;
			this.RefreshImages();
		}
	}

	public void OnClosed()
	{
		UIView.get_library().Hide(base.GetType().Name, -1);
	}

	private void RefreshImages()
	{
		UIComponent uIComponent = base.Find("ChirperImagesContainer");
		int num = 0;
		for (int i = 0; i < this.m_Owner.m_ChirperSet.Length; i++)
		{
			if (this.m_Owner.m_ChirperSet[i].m_RequiredDLCMask == (SteamHelper.DLC_BitMask)0 || (SteamHelper.GetOwnedDLCMask() & this.m_Owner.m_ChirperSet[i].m_RequiredDLCMask) != (SteamHelper.DLC_BitMask)0)
			{
				UIButton uIButton;
				if (uIComponent.get_components().Count > num)
				{
					uIButton = (uIComponent.get_components()[num] as UIButton);
					uIButton.remove_eventClick(new MouseEventHandler(this.OnSelected));
				}
				else
				{
					uIButton = (uIComponent.AttachUIComponent(UITemplateManager.GetAsGameObject(ChirpOptionsPanel.kChirperImageTemplate)) as UIButton);
				}
				uIButton.set_atlas(this.m_Owner.m_ChirperAtlas);
				uIButton.set_normalFgSprite(this.m_Owner.m_ChirperSet[i].m_Normal);
				uIButton.set_size(this.m_Owner.m_ChirperAtlas.get_Item(uIButton.get_normalFgSprite()).get_pixelSize());
				uIButton.set_stringUserData(this.m_Owner.m_ChirperSet[i].m_SetName);
				uIButton.add_eventClick(new MouseEventHandler(this.OnSelected));
				UIComponent uIComponent2 = uIButton.Find("Selected");
				uIComponent2.set_isVisible(this.m_SelectedChirper == this.m_Owner.m_ChirperSet[i].m_SetName);
				num++;
			}
		}
	}

	private void OnSelected(UIComponent c, UIMouseEventParameter p)
	{
		if (p.get_source() != null)
		{
			this.m_SelectedChirper.set_value(p.get_source().get_stringUserData());
			this.m_Owner.SetChirperImage(this.m_SelectedChirper);
			this.RefreshImages();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			base.get_component().CenterToParent();
			base.get_component().Focus();
			if (Singleton<SimulationManager>.get_exists())
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = true;
				});
			}
			if (Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().MuteAll = true;
			}
			Cursor.set_visible(true);
			Cursor.set_lockState(0);
		}
		else
		{
			if (Singleton<SimulationManager>.get_exists())
			{
				Singleton<SimulationManager>.get_instance().AddAction(delegate
				{
					Singleton<SimulationManager>.get_instance().ForcedSimulationPaused = false;
				});
			}
			if (Singleton<AudioManager>.get_exists())
			{
				Singleton<AudioManager>.get_instance().MuteAll = false;
			}
		}
	}

	private void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}

	private static readonly string kChirperImageTemplate = "ChirperImageTemplate";

	private SavedString m_SelectedChirper = new SavedString(Settings.chirperImage, Settings.sharedSettings, DefaultSettings.chirperImage, true);

	private ChirpPanel m_Owner;
}
