﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Plugins;
using ColossalFramework.UI;
using ICities;
using UnityEngine;

public class OptionsMainPanel : UICustomControl
{
	private void Awake()
	{
		this.m_Categories = base.Find<UIListBox>("Categories");
		this.m_CategoriesContainer = base.Find<UITabContainer>("OptionsContainer");
		this.CreateCategories();
		this.SetCategory(OptionsMainPanel.Category.Graphics);
	}

	private void OnEnable()
	{
		LocaleManager.add_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.RefreshPlugins));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.RefreshPlugins));
	}

	private void OnDisable()
	{
		LocaleManager.remove_eventLocaleChanged(new LocaleManager.LocaleChangedHandler(this.OnLocaleChanged));
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.RefreshPlugins));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.RefreshPlugins));
	}

	public void SelectMod(string modName)
	{
		int num = Array.IndexOf<string>(this.m_Categories.get_items(), modName);
		if (num != -1)
		{
			this.SetCategory((OptionsMainPanel.Category)num);
		}
	}

	private void OnLocaleChanged()
	{
		this.CreateCategories();
	}

	private void RefreshPlugins()
	{
		this.CreateCategories();
	}

	private void OnCategoryChanged(UIComponent c, int index)
	{
		this.SetContainerCategory(index);
	}

	private void AddGroupCategory(string name)
	{
		this.AddCategory("<color #5EC3FF>" + name + "</color>", null);
	}

	private void AddSpace()
	{
		this.AddCategory(string.Empty, null);
	}

	private void AddCategory(string name, UIComponent container)
	{
		List<string> list;
		if (this.m_Categories.get_items() != null)
		{
			list = new List<string>(this.m_Categories.get_items());
		}
		else
		{
			list = new List<string>();
		}
		list.Add(name);
		if (container == null)
		{
			container = this.m_CategoriesContainer.AddUIComponent<UIPanel>();
			this.m_Dummies.Add(container);
		}
		container.set_zOrder(list.Count - 1);
		this.m_Categories.set_items(list.ToArray());
	}

	private void CreateCategories()
	{
		for (int i = 0; i < this.m_Dummies.Count; i++)
		{
			if (this.m_Dummies[i].get_parent() != null)
			{
				this.m_Dummies[i].get_parent().RemoveUIComponent(this.m_Dummies[i]);
			}
			Object.DestroyImmediate(this.m_Dummies[i]);
		}
		this.m_Dummies.Clear();
		this.m_Categories.remove_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnCategoryChanged));
		this.m_Categories.set_items(new string[0]);
		int selectedIndex = this.m_CategoriesContainer.get_selectedIndex();
		this.m_CategoriesContainer.set_selectedIndex(-1);
		this.AddGroupCategory(Locale.Get("OPTIONS_SYSTEMSETTINGS"));
		this.AddSpace();
		this.AddCategory(Locale.Get("OPTIONS_GRAPHICS"), base.Find("Graphics"));
		this.AddCategory(Locale.Get("OPTIONS_GAMEPLAY"), base.Find("Gameplay"));
		this.AddCategory(Locale.Get("OPTIONS_KEYMAPPING"), base.Find("Keymapping"));
		this.AddCategory(Locale.Get("OPTIONS_AUDIO"), base.Find("Audio"));
		this.AddCategory(Locale.Get("OPTIONS_MISC"), base.Find("Misc"));
		if (Singleton<PluginManager>.get_instance().get_enabledModCount() > 0 && this.ProbeUISettingsMods())
		{
			this.AddSpace();
			this.AddGroupCategory(Locale.Get("OPTIONS_MODSSETTINGS"));
			this.AddSpace();
			this.AddUserMods();
		}
		this.m_Categories.set_filteredItems(new int[]
		{
			0,
			1,
			7,
			8,
			9
		});
		this.m_Categories.add_eventSelectedIndexChanged(new PropertyChangedEventHandler<int>(this.OnCategoryChanged));
		this.m_Categories.set_selectedIndex(selectedIndex);
	}

	private bool ProbeUISettingsMods()
	{
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			if (current.get_isEnabled())
			{
				IUserMod[] instances = current.GetInstances<IUserMod>();
				if (instances.Length == 1)
				{
					MethodInfo method = instances[0].GetType().GetMethod("OnSettingsUI", BindingFlags.Instance | BindingFlags.Public);
					if (method != null)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	private void AddUserMods()
	{
		foreach (PluginManager.PluginInfo current in Singleton<PluginManager>.get_instance().GetPluginsInfo())
		{
			if (current.get_isEnabled())
			{
				string name = current.get_name();
				try
				{
					IUserMod[] instances = current.GetInstances<IUserMod>();
					if (instances.Length == 1)
					{
						name = instances[0].get_Name();
					}
					MethodInfo method = instances[0].GetType().GetMethod("OnSettingsUI", BindingFlags.Instance | BindingFlags.Public);
					if (method != null)
					{
						UIComponent uIComponent = this.m_CategoriesContainer.AttachUIComponent(UITemplateManager.GetAsGameObject("OptionsScrollPanelTemplate"));
						uIComponent.set_name(instances[0].get_Name());
						this.m_Dummies.Add(uIComponent);
						try
						{
							method.Invoke(instances[0], new object[]
							{
								new UIHelper(uIComponent.Find("ScrollContent"))
							});
							this.AddCategory(name, uIComponent);
						}
						catch (Exception ex)
						{
							Debug.LogException(ex);
							UIView.ForwardException(new Exception("A Mod caused an error", ex));
						}
					}
				}
				catch (UnityException ex2)
				{
					Debug.LogException(ex2);
					UIView.ForwardException(new ModException("A Mod caused an error", ex2));
				}
				catch (Exception ex3)
				{
					Debug.LogException(ex3);
					UIView.ForwardException(new ModException("A Mod caused an error", ex3));
				}
			}
		}
	}

	private void SetCategory(OptionsMainPanel.Category index)
	{
		this.m_Categories.set_selectedIndex((int)index);
	}

	private void SetContainerCategory(int index)
	{
		this.m_CategoriesContainer.set_selectedIndex(index);
	}

	public void OnKeyDown(UIComponent comp, UIKeyEventParameter p)
	{
		if (!p.get_used() && p.get_keycode() == 27)
		{
			this.OnClosed();
			p.Use();
		}
	}

	public void OnMouseDown(UIComponent comp, UIMouseEventParameter p)
	{
		if (!p.get_used() && p.get_source() == base.get_component())
		{
			base.get_component().Focus();
		}
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			this.m_Categories.Focus();
		}
		else
		{
			MainMenu.SetFocus();
		}
	}

	public void OnClosed()
	{
		UIView.get_library().Hide("OptionsPanel", -1);
		PauseMenu pauseMenu = UIView.get_library().Get<PauseMenu>("PauseMenu");
		if (pauseMenu != null)
		{
			pauseMenu.OnClosed();
		}
	}

	private UIListBox m_Categories;

	private UITabContainer m_CategoriesContainer;

	private List<UIComponent> m_Dummies = new List<UIComponent>();

	private enum Category
	{
		Graphics = 2,
		Gameplay,
		Audio,
		Misc
	}
}
