﻿using System;
using ColossalFramework;
using UnityEngine;

public class SupportCableAI : NetAI
{
	public override Color GetColor(ushort segmentID, ref NetSegment data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(segmentID, ref data, infoMode);
	}

	public override Color GetColor(ushort nodeID, ref NetNode data, InfoManager.InfoMode infoMode)
	{
		return base.GetColor(nodeID, ref data, infoMode);
	}

	public override void GetPlacementInfoMode(out InfoManager.InfoMode mode, out InfoManager.SubInfoMode subMode, float elevation)
	{
		mode = InfoManager.InfoMode.None;
		subMode = InfoManager.SubInfoMode.Default;
	}

	public override void GetNodeBuilding(ushort nodeID, ref NetNode data, out BuildingInfo building, out float heightOffset)
	{
		if (data.m_elevation < 10)
		{
			building = this.m_anchorInfo;
			heightOffset = 0f;
		}
		else
		{
			building = null;
			heightOffset = 0f;
		}
	}

	public override bool NodeModifyMask(ushort nodeID, ref NetNode data, ushort segment1, ushort segment2, int index, ref TerrainModify.Surface surface, ref TerrainModify.Heights heights, ref TerrainModify.Edges edges, ref float leftT, ref float rightT, ref float leftY, ref float rightY)
	{
		return false;
	}

	public override bool CollapseSegment(ushort segmentID, ref NetSegment data, InstanceManager.Group group, bool demolish)
	{
		return demolish && base.CollapseSegment(segmentID, ref data, group, demolish);
	}

	public override void ParentCollapsed(ushort segmentID, ref NetSegment data, InstanceManager.Group group)
	{
		if ((data.m_flags & (NetSegment.Flags.Created | NetSegment.Flags.Deleted | NetSegment.Flags.Collapsed)) != NetSegment.Flags.Created)
		{
			return;
		}
		data.m_flags |= NetSegment.Flags.Collapsed;
		Singleton<NetManager>.get_instance().UpdateSegmentRenderer(segmentID, true);
		Singleton<NetManager>.get_instance().UpdateSegmentColors(segmentID);
	}

	public override bool IsOverground()
	{
		return true;
	}

	public override float GetTerrainLowerOffset()
	{
		return Mathf.Max(0f, this.m_info.m_minHeight - 3f);
	}

	public BuildingInfo m_anchorInfo;
}
