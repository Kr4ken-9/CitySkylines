﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class SolarPowerPlantAI : PowerPlantAI
{
	public override int GetElectricityRate(ushort buildingID, ref Building data)
	{
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		float num2 = this.m_batteryFactor + (1f - this.m_batteryFactor) * Singleton<WeatherManager>.get_instance().SampleSunIntensity(data.m_position, false);
		num = Mathf.RoundToInt((float)num * num2);
		int num3;
		int num4;
		this.GetElectricityProduction(out num3, out num4);
		return num4 * num / 100;
	}

	public override string GetConstructionInfo(int productionRate)
	{
		int num;
		int num2;
		this.GetElectricityProduction(out num, out num2);
		return StringUtils.SafeFormat(Locale.Get("TOOL_ELECTRICITY_ESTIMATE"), num2 * productionRate * 16 / 100000);
	}

	public override void BuildingLoaded(ushort buildingID, ref Building data, uint version)
	{
		base.BuildingLoaded(buildingID, ref data, version);
		int num = (int)data.m_productionRate;
		if ((data.m_flags & Building.Flags.Evacuating) != Building.Flags.None)
		{
			num = 0;
		}
		int budget = Singleton<EconomyManager>.get_instance().GetBudget(this.m_info.m_class);
		num = PlayerBuildingAI.GetProductionRate(num, budget);
		float num2 = this.m_batteryFactor + (1f - this.m_batteryFactor) * Singleton<WeatherManager>.get_instance().SampleSunIntensity(data.m_position, false);
		num = Mathf.RoundToInt((float)num * num2);
		uint num3 = (uint)(((int)buildingID << 8) / 49152);
		uint num4 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - num3;
		Building.Frame lastFrameData = data.GetLastFrameData();
		data.SetFrameData(num4 - 512u, lastFrameData);
		lastFrameData.m_productionState = (byte)((int)lastFrameData.m_productionState + num);
		data.SetFrameData(num4 - 256u, lastFrameData);
		lastFrameData.m_productionState = (byte)((int)lastFrameData.m_productionState + num);
		data.SetFrameData(num4, lastFrameData);
	}

	public override bool CanUseGroupMesh(ushort buildingID, ref Building buildingData)
	{
		return !this.m_info.m_hasVertexAnimation && base.CanUseGroupMesh(buildingID, ref buildingData);
	}

	public override ToolBase.ToolErrors CheckBuildPosition(ushort relocateID, ref Vector3 position, ref float angle, float waterHeight, float elevation, ref Segment3 connectionSegment, out int productionRate, out int constructionCost)
	{
		ToolBase.ToolErrors result = base.CheckBuildPosition(relocateID, ref position, ref angle, waterHeight, elevation, ref connectionSegment, out productionRate, out constructionCost);
		productionRate = Mathf.Clamp(Mathf.RoundToInt(Singleton<WeatherManager>.get_instance().SampleSunIntensity(position, true) * 100f), 0, 100);
		return result;
	}

	protected override void ProduceGoods(ushort buildingID, ref Building buildingData, ref Building.Frame frameData, int productionRate, int finalProductionRate, ref Citizen.BehaviourData behaviour, int aliveWorkerCount, int totalWorkerCount, int workPlaceCount, int aliveVisitorCount, int totalVisitorCount, int visitPlaceCount)
	{
		float num = this.m_batteryFactor + (1f - this.m_batteryFactor) * Singleton<WeatherManager>.get_instance().SampleSunIntensity(buildingData.m_position, false);
		finalProductionRate = Mathf.RoundToInt((float)finalProductionRate * num);
		base.ProduceGoods(buildingID, ref buildingData, ref frameData, productionRate, finalProductionRate, ref behaviour, aliveWorkerCount, totalWorkerCount, workPlaceCount, aliveVisitorCount, totalVisitorCount, visitPlaceCount);
		frameData.m_productionState = (byte)((int)frameData.m_productionState + finalProductionRate);
	}

	protected override void AddProductionCapacity(ref District districtData, int electricityProduction)
	{
		districtData.m_productionData.m_tempElectricityCapacity = districtData.m_productionData.m_tempElectricityCapacity + (uint)electricityProduction;
		districtData.m_productionData.m_tempRenewableElectricity = districtData.m_productionData.m_tempRenewableElectricity + (uint)electricityProduction;
	}

	public override void GetElectricityProduction(out int min, out int max)
	{
		min = Mathf.RoundToInt((float)this.m_electricityProduction * this.m_batteryFactor);
		max = this.m_electricityProduction;
	}

	public override string GetLocalizedStats(ushort buildingID, ref Building data)
	{
		string str = base.GetLocalizedStats(buildingID, ref data) + Environment.NewLine;
		int num = Mathf.RoundToInt(Singleton<WeatherManager>.get_instance().SampleSunIntensity(data.m_position, false) * 100f);
		return str + LocaleFormatter.FormatGeneric("AIINFO_SUN_INTENSITY", new object[]
		{
			num
		});
	}

	public float m_batteryFactor = 0.7f;
}
