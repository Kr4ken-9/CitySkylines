﻿using System;
using System.Reflection;
using ColossalFramework.UI;

public class REBoolSet : REPropertySet
{
	private void OnEnable()
	{
		if (this.m_Input != null)
		{
			this.m_Input.add_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.CheckChanged));
		}
	}

	private void OnDisable()
	{
		if (this.m_Input != null)
		{
			this.m_Input.remove_eventCheckChanged(new PropertyChangedEventHandler<bool>(this.CheckChanged));
		}
	}

	private void CheckChanged(UIComponent component, bool value)
	{
		base.SetValue(value);
	}

	protected override void Initialize(object target, FieldInfo targetField, string labelText)
	{
		base.get_component().Disable();
		if (this.m_Label != null)
		{
			this.m_Label.set_text(labelText);
		}
		if (this.m_Input != null)
		{
			this.m_Input.set_isChecked((bool)targetField.GetValue(target));
		}
		base.get_component().Enable();
	}

	protected override bool Validate(object target, FieldInfo targetField)
	{
		return target != null && targetField != null && targetField.FieldType == typeof(bool);
	}

	public UILabel m_Label;

	public UICheckBox m_Input;
}
