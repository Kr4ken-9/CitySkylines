﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public class EditorChirpPanel : ToolsModifierControl
{
	public void Expand()
	{
		this.Expand(0f);
	}

	public void Expand(float timeout)
	{
		this.Show(timeout);
	}

	public void Collapse()
	{
		this.Hide();
	}

	private void Awake()
	{
		this.m_Header = base.Find<UILabel>("Header");
		this.m_ClipRegion = base.Find<UIPanel>("ClipRegion");
		this.m_DefaultSize = base.get_component().get_size();
		base.get_component().set_size(new Vector2(0f, 37f));
		this.m_ClipRegion.Hide();
		this.m_Header.Hide();
		this.m_WaterTile0 = base.Find<UICheckBox>("WaterTile0");
		this.m_ShipConnections = base.Find<UICheckBox>("ShipConnections");
		this.m_TrainConnections = base.Find<UICheckBox>("TrainConnections");
		this.m_PlaneConnections = base.Find<UICheckBox>("PlaneConnections");
		this.m_RoadInConnections = base.Find<UICheckBox>("RoadInConnections");
		this.m_RoadOutConnections = base.Find<UICheckBox>("RoadOutConnections");
		this.m_ResourceOil = base.Find<UICheckBox>("ResourceOil");
		this.m_ResourceOre = base.Find<UICheckBox>("ResourceOre");
		this.m_ResourceForest = base.Find<UICheckBox>("ResourceForest");
		this.m_ResourceFertility = base.Find<UICheckBox>("ResourceFertility");
	}

	private void OnEnable()
	{
	}

	private void OnDisable()
	{
	}

	public void ShowHeader()
	{
		if (!this.m_Header.get_isVisible())
		{
			this.m_Header.Show();
			if (!this.m_Showing)
			{
				this.m_ClipRegion.Show();
				ValueAnimator.Animate("EditorChirpPanelX", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.x = val;
					size.y = 37f;
					base.get_component().set_size(size);
				}, new AnimatedFloat(0f, this.m_DefaultSize.x, this.m_ShowHideTime, this.m_ShowEasingType));
			}
			ValueAnimator.Animate("EditorChirpPanelHeader", delegate(float val)
			{
				this.m_Header.set_opacity(val);
			}, new AnimatedFloat(0f, 1f, this.m_ShowHideTime, this.m_ShowEasingType));
		}
	}

	public void HideHeader()
	{
		if (this.m_Header.get_isVisible())
		{
			if (!this.m_Showing)
			{
				ValueAnimator.Animate("EditorChirpPanelX", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.x = val;
					size.y = 37f;
					base.get_component().set_size(size);
				}, new AnimatedFloat(this.m_DefaultSize.x, 0f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
				{
					this.m_ClipRegion.Hide();
				});
			}
			ValueAnimator.Animate("EditorChirpPanelHeader", delegate(float val)
			{
				this.m_Header.set_opacity(val);
			}, new AnimatedFloat(1f, 0f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
			{
				this.m_Header.Hide();
			});
		}
	}

	public void Show(float timeout)
	{
		if (!this.m_Showing)
		{
			this.m_Timeout = timeout;
		}
		if (!this.m_Showing)
		{
			this.m_Showing = true;
			this.m_ClipRegion.Show();
			if (this.m_Header.get_isVisible())
			{
				ValueAnimator.Animate("EditorChirpPanelY", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.y = val;
					base.get_component().set_size(size);
				}, new AnimatedFloat(37f, this.m_DefaultSize.y, this.m_ShowHideTime, this.m_ShowEasingType));
			}
			else
			{
				ValueAnimator.Animate("EditorChirpPanelX", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.x = val;
					base.get_component().set_size(size);
				}, new AnimatedFloat(0f, this.m_DefaultSize.x, this.m_ShowHideTime, this.m_ShowEasingType), delegate
				{
					ValueAnimator.Animate("EditorChirpPanelY", delegate(float val)
					{
						Vector2 size = base.get_component().get_size();
						size.y = val;
						base.get_component().set_size(size);
					}, new AnimatedFloat(37f, this.m_DefaultSize.y, this.m_ShowHideTime, this.m_ShowEasingType));
				});
			}
		}
	}

	public void Hide()
	{
		if (this.m_Showing)
		{
			if (this.m_Header.get_isVisible())
			{
				ValueAnimator.Animate("EditorChirpPanelY", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.y = val;
					base.get_component().set_size(size);
				}, new AnimatedFloat(this.m_DefaultSize.y, 37f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
				{
					this.m_Showing = false;
				});
			}
			else
			{
				ValueAnimator.Animate("EditorChirpPanelY", delegate(float val)
				{
					Vector2 size = base.get_component().get_size();
					size.y = val;
					base.get_component().set_size(size);
				}, new AnimatedFloat(this.m_DefaultSize.y, 37f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
				{
					ValueAnimator.Animate("EditorChirpPanelX", delegate(float val)
					{
						Vector2 size = base.get_component().get_size();
						size.x = val;
						base.get_component().set_size(size);
					}, new AnimatedFloat(this.m_DefaultSize.x, 0f, this.m_ShowHideTime, this.m_ShowEasingType), delegate
					{
						this.m_ClipRegion.Hide();
						this.m_Showing = false;
					});
				});
			}
		}
	}

	public void OnMouseDown(UIComponent component, UIMouseEventParameter p)
	{
		this.m_Timeout = 0f;
	}

	private void Update()
	{
		if (Singleton<ToolManager>.get_exists() && Singleton<ToolManager>.get_instance().m_properties.m_mode == ItemClass.Availability.MapEditor)
		{
			if (this.m_TimeSinceLoaded > this.m_ShowAfterLoadTime)
			{
				this.m_TimeSinceLoaded = -1f;
				this.Show(this.m_ShowTimeAfterLoadTime);
			}
			else if (this.m_TimeSinceLoaded >= 0f)
			{
				this.m_TimeSinceLoaded += Time.get_deltaTime();
			}
		}
		this.UpdateHeader();
		if (base.get_component().get_isVisible())
		{
			this.UpdateRequirements();
		}
		if (this.m_Showing && this.m_Timeout > 0f)
		{
			this.m_Timeout -= Time.get_deltaTime();
			if (this.m_Timeout <= 0f)
			{
				this.Hide();
			}
		}
	}

	public void Toggle()
	{
		if (this.m_Showing)
		{
			this.Hide();
		}
		else
		{
			this.Show(0f);
		}
	}

	private void UpdateRequirements()
	{
		if (this.m_Showing)
		{
			int x;
			int z;
			Singleton<GameAreaManager>.get_instance().GetStartTile(out x, out z);
			uint num;
			uint num2;
			uint num3;
			uint num4;
			uint num5;
			Singleton<NaturalResourceManager>.get_instance().GetTileResources(x, z, out num, out num2, out num3, out num4, out num5);
			this.m_WaterTile0.set_isChecked(num5 != 0u);
			int num6;
			int num7;
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, out num6, out num7);
			this.m_ShipConnections.set_isChecked(num6 != 0);
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, out num6, out num7);
			this.m_TrainConnections.set_isChecked(num6 != 0);
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, out num6, out num7);
			this.m_PlaneConnections.set_isChecked(num6 != 0);
			Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.Road, ItemClass.SubService.None, out num6, out num7);
			this.m_RoadInConnections.set_isChecked(num6 != 0);
			this.m_RoadOutConnections.set_isChecked(num7 != 0);
			Singleton<NaturalResourceManager>.get_instance().CalculateUnlockableResources(out num, out num2, out num3, out num4, out num5);
			this.m_ResourceOil.set_isChecked(num2 != 0u);
			this.m_ResourceOre.set_isChecked(num != 0u);
			this.m_ResourceFertility.set_isChecked(num4 != 0u);
			this.m_ResourceForest.set_isChecked(num3 != 0u);
		}
	}

	private void UpdateHeader()
	{
		if (ToolsModifierControl.GetCurrentTool<TreeTool>() != null)
		{
			if (!this.m_Header.get_isVisible())
			{
				this.ShowHeader();
			}
			if (Singleton<TreeManager>.get_exists())
			{
				this.m_Header.set_text(StringUtils.SafeFormat(Locale.Get("CHIRPHEADER_TREES"), new object[]
				{
					Singleton<TreeManager>.get_instance().m_treeCount,
					250000
				}));
			}
		}
		else if (ToolsModifierControl.GetCurrentTool<PropTool>() != null)
		{
			if (!this.m_Header.get_isVisible())
			{
				this.ShowHeader();
			}
			if (Singleton<PropManager>.get_exists())
			{
				this.m_Header.set_text(StringUtils.SafeFormat(Locale.Get("CHIRPHEADER_PROPS"), new object[]
				{
					Singleton<PropManager>.get_instance().m_propCount,
					50000
				}));
			}
		}
		else if (ToolsModifierControl.GetCurrentTool<NetTool>() != null)
		{
			if (Singleton<BuildingManager>.get_exists())
			{
				NetTool tool = ToolsModifierControl.GetTool<NetTool>();
				if (tool.m_prefab.m_class.m_service == ItemClass.Service.Road)
				{
					if (!this.m_Header.get_isVisible())
					{
						this.ShowHeader();
					}
					int num;
					int num2;
					Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.Road, ItemClass.SubService.None, out num, out num2);
					this.m_Header.set_text(StringUtils.SafeFormat(Locale.Get("CHIRPHEADER_ROAD_CONNECTIONS"), new object[]
					{
						num,
						4,
						num2,
						4
					}));
				}
				else if (tool.m_prefab.m_class.m_service == ItemClass.Service.PublicTransport)
				{
					if (!this.m_Header.get_isVisible())
					{
						this.ShowHeader();
					}
					if (tool.m_prefab.m_class.m_subService == ItemClass.SubService.PublicTransportShip)
					{
						int num3;
						int num4;
						Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportShip, out num3, out num4);
						this.m_Header.set_text(StringUtils.SafeFormat(Locale.Get("CHIRPHEADER_SHIP_CONNECTIONS"), new object[]
						{
							num3,
							4
						}));
					}
					else if (tool.m_prefab.m_class.m_subService == ItemClass.SubService.PublicTransportPlane)
					{
						int num5;
						int num6;
						Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportPlane, out num5, out num6);
						this.m_Header.set_text(StringUtils.SafeFormat(Locale.Get("CHIRPHEADER_PLANE_CONNECTIONS"), new object[]
						{
							num5,
							4
						}));
					}
					else if (tool.m_prefab.m_class.m_subService == ItemClass.SubService.PublicTransportTrain)
					{
						int num7;
						int num8;
						Singleton<BuildingManager>.get_instance().CalculateOutsideConnectionCount(ItemClass.Service.PublicTransport, ItemClass.SubService.PublicTransportTrain, out num7, out num8);
						this.m_Header.set_text(StringUtils.SafeFormat(Locale.Get("CHIRPHEADER_TRAIN_CONNECTIONS"), new object[]
						{
							num7,
							4
						}));
					}
				}
			}
		}
		else
		{
			this.HideHeader();
		}
	}

	public AudioClip m_NotificationSound;

	public float m_MessageTimeout = 6f;

	public float m_ShowHideTime = 0.3f;

	public EasingType m_ShowEasingType = 13;

	public EasingType m_HideEasingType = 13;

	private UIPanel m_ClipRegion;

	private Vector2 m_DefaultSize;

	private bool m_Showing;

	private UILabel m_Header;

	private UICheckBox m_WaterTile0;

	private UICheckBox m_ShipConnections;

	private UICheckBox m_TrainConnections;

	private UICheckBox m_PlaneConnections;

	private UICheckBox m_RoadInConnections;

	private UICheckBox m_RoadOutConnections;

	private UICheckBox m_ResourceOil;

	private UICheckBox m_ResourceOre;

	private UICheckBox m_ResourceForest;

	private UICheckBox m_ResourceFertility;

	private float m_Timeout;

	private float m_TimeSinceLoaded;

	public float m_ShowAfterLoadTime = 2f;

	public float m_ShowTimeAfterLoadTime = 4f;
}
