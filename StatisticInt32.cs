﻿using System;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;

public class StatisticInt32 : StatisticBase
{
	public StatisticInt32()
	{
		this.m_startFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		this.m_buffer = new int[256];
	}

	public override void Reset()
	{
		this.m_startFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		this.m_temporary1 = 0;
		this.m_temporary2 = 0;
		this.m_latest = 0;
		this.m_total = 0;
		this.m_count = 0u;
		this.m_shift = 0u;
		this.m_index = 0u;
		if (this.m_buffer == null)
		{
			this.m_buffer = new int[256];
		}
		for (uint num = 0u; num < 256u; num += 1u)
		{
			this.m_buffer[(int)((UIntPtr)num)] = 0;
		}
	}

	public override StatisticBase Acquire<T>(int index, int count)
	{
		throw new Exception("Method not supported");
	}

	public override void Add(int delta)
	{
		this.m_temporary1 += delta;
		this.m_temporary2 += delta;
		this.m_total += delta;
	}

	public override void Set(int value)
	{
		this.m_temporary1 = 0;
		this.m_temporary2 = value;
		this.m_latest = value;
	}

	public override void Add(long delta)
	{
		throw new Exception("Method not supported");
	}

	public override void Set(long value)
	{
		throw new Exception("Method not supported");
	}

	public override void Add(float delta)
	{
		throw new Exception("Method not supported");
	}

	public override void Set(float value)
	{
		throw new Exception("Method not supported");
	}

	public override void Tick()
	{
		if (this.m_temporary1 == 0)
		{
			this.m_buffer[(int)((UIntPtr)this.m_index)] = this.m_temporary2;
			this.m_count += 1u;
		}
		else
		{
			this.m_buffer[(int)((UIntPtr)this.m_index)] = this.m_temporary1 / (int)(this.m_count += 1u);
		}
		this.m_latest = this.m_temporary2;
		this.m_temporary2 = 0;
		if (this.m_count == 1u << (int)this.m_shift)
		{
			while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				this.m_endFrame = (Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 4294963200u) + 4096u;
				if ((this.m_index += 1u) == 256u)
				{
					this.m_shift += 1u;
					this.m_index = 128u;
					for (uint num = 0u; num < this.m_index; num += 1u)
					{
						this.m_buffer[(int)((UIntPtr)num)] = (this.m_buffer[(int)((UIntPtr)(num << 1))] + this.m_buffer[(int)((UIntPtr)((num << 1) + 1u))]) / 2;
					}
					for (uint num2 = this.m_index; num2 < 256u; num2 += 1u)
					{
						this.m_buffer[(int)((UIntPtr)num2)] = 0;
					}
				}
			}
			finally
			{
				Monitor.Exit(this);
			}
			this.m_temporary1 = 0;
			this.m_count = 0u;
		}
	}

	public override void Serialize(DataSerializer s)
	{
		s.WriteUInt32(this.m_startFrame);
		s.WriteUInt32(this.m_endFrame);
		s.WriteInt32(this.m_temporary1);
		s.WriteInt32(this.m_temporary2);
		s.WriteInt32(this.m_latest);
		s.WriteInt32(this.m_total);
		s.WriteUInt16(this.m_count);
		s.WriteUInt8(this.m_shift);
		s.WriteUInt8(this.m_index);
		EncodedArray.Int.Write(s, this.m_buffer);
	}

	public override void Deserialize(DataSerializer s)
	{
		if (this.m_buffer == null)
		{
			this.m_buffer = new int[256];
		}
		this.m_startFrame = s.ReadUInt32();
		if (s.get_version() >= 189u)
		{
			this.m_endFrame = s.ReadUInt32();
		}
		else
		{
			this.m_endFrame = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
		}
		this.m_temporary1 = s.ReadInt32();
		this.m_temporary2 = s.ReadInt32();
		this.m_latest = s.ReadInt32();
		this.m_total = s.ReadInt32();
		this.m_count = s.ReadUInt16();
		this.m_shift = s.ReadUInt8();
		this.m_index = s.ReadUInt8();
		EncodedArray.Int.Read(s, this.m_buffer);
	}

	public override void AfterDeserialize(DataSerializer s)
	{
	}

	public override StatisticBase Get(int index)
	{
		throw new Exception("Method not supported");
	}

	public override int GetLatestInt32()
	{
		return this.m_latest;
	}

	public override int GetTotalInt32()
	{
		return this.m_total;
	}

	public override long GetLatestInt64()
	{
		return (long)this.m_latest;
	}

	public override long GetTotalInt64()
	{
		return (long)this.m_total;
	}

	public override float GetLatestFloat()
	{
		return (float)this.m_latest;
	}

	public override float GetTotalFloat()
	{
		return (float)this.m_total;
	}

	public override void GetValues(double[] buffer, out int count, out DateTime startTime, out DateTime endTime)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			count = (int)this.m_index;
			for (int i = 0; i < count; i++)
			{
				buffer[i] = (double)this.m_buffer[i];
			}
			startTime = instance.FrameToTime(this.m_startFrame);
			endTime = instance.FrameToTime(this.m_endFrame);
		}
		finally
		{
			Monitor.Exit(this);
		}
	}

	public override void AddValues(double[] buffer, ref int count, ref DateTime startTime, ref DateTime endTime)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		while (!Monitor.TryEnter(this, SimulationManager.SYNCHRONIZE_TIMEOUT))
		{
		}
		try
		{
			count = (int)this.m_index;
			for (int i = 0; i < count; i++)
			{
				buffer[i] += (double)this.m_buffer[i];
			}
			startTime = instance.FrameToTime(this.m_startFrame);
			endTime = instance.FrameToTime(this.m_endFrame);
		}
		finally
		{
			Monitor.Exit(this);
		}
	}

	private const uint BUFFER_SIZE = 256u;

	private uint m_startFrame;

	private uint m_endFrame;

	private int m_temporary1;

	private int m_temporary2;

	private int m_latest;

	private int m_total;

	private int[] m_buffer;

	private uint m_count;

	private uint m_shift;

	private uint m_index;
}
