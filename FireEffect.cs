﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class FireEffect : EffectInfo
{
	public override void RenderEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, float timeOffset, float timeDelta, RenderManager.CameraInfo cameraInfo)
	{
		Vector3 zero;
		zero..ctor(100000f, 100000f, 100000f);
		Vector3 zero2;
		zero2..ctor(-100000f, -100000f, -100000f);
		int num = Mathf.RoundToInt(magnitude * 100f);
		if (this.m_particleEffect != null)
		{
			float particlesPerSquare = timeDelta * 0.01f;
			this.m_particleEffect.EmitParticles(id, area, velocity, particlesPerSquare, num, ref zero, ref zero2);
		}
		if (zero.x > zero2.x)
		{
			zero = Vector3.get_zero();
			zero2 = Vector3.get_zero();
		}
		if (this.m_lightEffect != null)
		{
			Randomizer randomizer;
			randomizer..ctor(Singleton<SimulationManager>.get_instance().m_referenceFrameIndex >> 2);
			Vector3 pos = (zero + zero2) * 0.5f;
			pos.y += 15f;
			float intensity = Mathf.Min(3f, (float)num * 0.125f) + (float)randomizer.Int32(10, 20) * 0.05f;
			float range = Vector3.Distance(zero, zero2) * 0.5f + 30f;
			Light component = this.m_lightEffect.GetComponent<Light>();
			Singleton<RenderManager>.get_instance().lightSystem.DrawLight(component.get_type(), pos, Vector3.get_forward(), Vector3.get_zero(), component.get_color(), intensity, range, component.get_spotAngle(), this.m_lightEffect.m_spotLeaking, false);
		}
	}

	public override void PlayEffect(InstanceID id, EffectInfo.SpawnArea area, Vector3 velocity, float acceleration, float magnitude, AudioManager.ListenerInfo listenerInfo, AudioGroup audioGroup)
	{
		if (this.m_soundEffect != null)
		{
			this.m_soundEffect.PlaySound(id, listenerInfo, audioGroup, area.m_bezier.a, velocity, this.m_soundEffect.m_range, magnitude, 1f);
		}
	}

	protected override void CreateEffect()
	{
		base.CreateEffect();
		if (this.m_particleEffect != null)
		{
			this.m_particleEffect.InitializeEffect();
		}
		if (this.m_lightEffect != null)
		{
			this.m_lightEffect.InitializeEffect();
		}
		if (this.m_soundEffect != null)
		{
			this.m_soundEffect.InitializeEffect();
		}
	}

	protected override void DestroyEffect()
	{
		if (this.m_particleEffect != null)
		{
			this.m_particleEffect.ReleaseEffect();
		}
		if (this.m_lightEffect != null)
		{
			this.m_lightEffect.ReleaseEffect();
		}
		if (this.m_soundEffect != null)
		{
			this.m_soundEffect.ReleaseEffect();
		}
		base.DestroyEffect();
	}

	public override bool RequireRender()
	{
		return true;
	}

	public override bool RequirePlay()
	{
		return true;
	}

	public ParticleEffect m_particleEffect;

	public LightEffect m_lightEffect;

	public SoundEffect m_soundEffect;
}
