﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using UnityEngine;

public class TerrainTool : ToolBase
{
	public bool IsUndoAvailable()
	{
		return this.m_undoList != null && this.m_undoList.Count > 0;
	}

	public void ResetUndoBuffer()
	{
		this.m_undoList.Clear();
		ushort[] backupHeights = Singleton<TerrainManager>.get_instance().BackupHeights;
		ushort[] rawHeights = Singleton<TerrainManager>.get_instance().RawHeights;
		for (int i = 0; i <= 1080; i++)
		{
			for (int j = 0; j <= 1080; j++)
			{
				int num = i * 1081 + j;
				backupHeights[num] = rawHeights[num];
			}
		}
	}

	public void Undo()
	{
		Singleton<SimulationManager>.get_instance().AddAction(this.UndoRequest());
	}

	protected override void Awake()
	{
		base.Awake();
		this.m_undoList = new List<TerrainTool.UndoStroke>();
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	protected override void OnToolGUI(Event e)
	{
		if (!this.m_toolController.IsInsideUI && e.get_type() == null)
		{
			if (e.get_button() == 0)
			{
				this.m_mouseLeftDown = true;
				this.m_endPosition = this.m_mousePosition;
			}
			else if (e.get_button() == 1)
			{
				if (this.m_mode == TerrainTool.Mode.Shift || this.m_mode == TerrainTool.Mode.Soften)
				{
					this.m_mouseRightDown = true;
				}
				else if (this.m_mode == TerrainTool.Mode.Level || this.m_mode == TerrainTool.Mode.Slope)
				{
					this.m_startPosition = this.m_mousePosition;
				}
			}
		}
		else if (e.get_type() == 1)
		{
			if (e.get_button() == 0)
			{
				this.m_mouseLeftDown = false;
				if (!this.m_mouseRightDown)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.StrokeEnded());
				}
			}
			else if (e.get_button() == 1)
			{
				this.m_mouseRightDown = false;
				if (!this.m_mouseLeftDown)
				{
					Singleton<SimulationManager>.get_instance().AddAction(this.StrokeEnded());
				}
			}
		}
		if (this.m_UndoKey.IsPressed(e) && !this.m_mouseLeftDown && !this.m_mouseRightDown && this.IsUndoAvailable())
		{
			this.Undo();
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		this.m_toolErrors = ToolBase.ToolErrors.Pending;
		this.m_toolController.SetBrush(this.m_brush, this.m_mousePosition, this.m_brushSize);
		this.m_strokeXmin = 1080;
		this.m_strokeXmax = 0;
		this.m_strokeZmin = 1080;
		this.m_strokeZmax = 0;
		ushort[] backupHeights = Singleton<TerrainManager>.get_instance().BackupHeights;
		ushort[] rawHeights = Singleton<TerrainManager>.get_instance().RawHeights;
		for (int i = 0; i <= 1080; i++)
		{
			for (int j = 0; j <= 1080; j++)
			{
				int num = i * 1081 + j;
				backupHeights[num] = rawHeights[num];
			}
		}
		Singleton<TerrainManager>.get_instance().RenderTopography = true;
		Singleton<TransportManager>.get_instance().TunnelsVisible = true;
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		this.ResetUndoBuffer();
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		base.ToolCursor = null;
		Singleton<TransportManager>.get_instance().TunnelsVisible = false;
		Singleton<TerrainManager>.get_instance().RenderTopography = false;
		this.m_toolController.SetBrush(null, Vector3.get_zero(), 1f);
		this.m_mouseRayValid = false;
		this.m_toolErrors = ToolBase.ToolErrors.Pending;
		Singleton<SimulationManager>.get_instance().AddAction(this.DisableTool());
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	protected override void OnToolUpdate()
	{
		switch (this.m_mode)
		{
		case TerrainTool.Mode.Shift:
			base.ToolCursor = this.m_shiftCursor;
			break;
		case TerrainTool.Mode.Level:
			base.ToolCursor = this.m_levelCursor;
			break;
		case TerrainTool.Mode.Soften:
			base.ToolCursor = this.m_softenCursor;
			break;
		case TerrainTool.Mode.Slope:
			base.ToolCursor = this.m_slopeCursor;
			break;
		}
		if (!this.m_toolController.IsInsideUI && Cursor.get_visible() && this.m_toolErrors != ToolBase.ToolErrors.Pending)
		{
			int currentCost = this.m_currentCost;
			if (this.m_strokeInProgress)
			{
				string text = null;
				if (currentCost > 0)
				{
					text = StringUtils.SafeFormat(Locale.Get("TOOL_LANDSCAPING_COST"), currentCost / 100);
				}
				else if (currentCost < 0)
				{
					text = StringUtils.SafeFormat(Locale.Get("TOOL_REFUND_AMOUNT"), -currentCost / 100);
				}
				base.ShowToolInfo(true, text, this.m_mousePosition);
			}
			else
			{
				base.ShowToolInfo(false, null, this.m_mousePosition);
			}
		}
		else
		{
			base.ShowToolInfo(false, null, this.m_mousePosition);
		}
	}

	protected override void OnToolLateUpdate()
	{
		Vector3 mousePosition = Input.get_mousePosition();
		this.m_mouseRay = Camera.get_main().ScreenPointToRay(mousePosition);
		this.m_mouseRayLength = Camera.get_main().get_farClipPlane();
		this.m_mouseRayValid = (!this.m_toolController.IsInsideUI && Cursor.get_visible());
		this.m_toolController.SetBrush(this.m_brush, this.m_mousePosition, this.m_brushSize);
		ToolBase.OverrideInfoMode = false;
	}

	[DebuggerHidden]
	private IEnumerator DisableTool()
	{
		TerrainTool.<DisableTool>c__Iterator0 <DisableTool>c__Iterator = new TerrainTool.<DisableTool>c__Iterator0();
		<DisableTool>c__Iterator.$this = this;
		return <DisableTool>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator UndoRequest()
	{
		TerrainTool.<UndoRequest>c__Iterator1 <UndoRequest>c__Iterator = new TerrainTool.<UndoRequest>c__Iterator1();
		<UndoRequest>c__Iterator.$this = this;
		return <UndoRequest>c__Iterator;
	}

	[DebuggerHidden]
	private IEnumerator StrokeEnded()
	{
		TerrainTool.<StrokeEnded>c__Iterator2 <StrokeEnded>c__Iterator = new TerrainTool.<StrokeEnded>c__Iterator2();
		<StrokeEnded>c__Iterator.$this = this;
		return <StrokeEnded>c__Iterator;
	}

	public override void SimulationStep()
	{
		ToolBase.RaycastInput input = new ToolBase.RaycastInput(this.m_mouseRay, this.m_mouseRayLength);
		ToolBase.RaycastOutput raycastOutput;
		if (this.m_mouseRayValid && ToolBase.RayCast(input, out raycastOutput))
		{
			this.m_mousePosition = raycastOutput.m_hitPos;
			if (this.m_mouseLeftDown != this.m_mouseRightDown)
			{
				if (!this.m_strokeInProgress)
				{
					this.m_strokeInProgress = true;
					this.m_currentCost = 0;
				}
				this.ApplyBrush();
			}
			else
			{
				this.m_toolErrors = ToolBase.ToolErrors.Pending;
			}
		}
		else
		{
			this.m_toolErrors = ToolBase.ToolErrors.RaycastFailed;
		}
		GuideController properties = Singleton<GuideManager>.get_instance().m_properties;
		if (properties != null)
		{
			Singleton<TerrainManager>.get_instance().m_terrainToolNotUsed.Activate(properties.m_terrainToolNotUsed);
		}
	}

	private int GetFreeUndoSpace()
	{
		int num = Singleton<TerrainManager>.get_instance().UndoBuffer.Length;
		if (this.m_undoList.Count > 0)
		{
			return (num + this.m_undoList[0].pointer - this.m_undoBufferFreePointer) % num - 1;
		}
		return num - 1;
	}

	private void EndStroke()
	{
		if ((this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None)
		{
			if (this.m_currentCost > 0)
			{
				Singleton<EconomyManager>.get_instance().FetchResource(EconomyManager.Resource.Landscaping, this.m_currentCost, ItemClass.Service.Beautification, ItemClass.SubService.None, ItemClass.Level.None);
			}
			else if (this.m_currentCost < 0)
			{
				Singleton<EconomyManager>.get_instance().AddResource(EconomyManager.Resource.RefundAmount, -this.m_currentCost, ItemClass.Service.Beautification, ItemClass.SubService.None, ItemClass.Level.None);
			}
			this.m_currentCost = 0;
		}
		else
		{
			int num = Singleton<TerrainManager>.get_instance().UndoBuffer.Length;
			int num2 = Math.Max(0, 1 + this.m_strokeXmax - this.m_strokeXmin) * Math.Max(0, 1 + this.m_strokeZmax - this.m_strokeZmin);
			if (num2 < 1)
			{
				return;
			}
			int num3 = 0;
			while (this.GetFreeUndoSpace() < num2 && num3 < 10000)
			{
				this.m_undoList.RemoveAt(0);
				num3++;
			}
			if (num3 >= 10000)
			{
				Debug.Log("TerrainTool:EndStroke: unexpectedly terminated freeing loop, might be a bug.");
				return;
			}
			TerrainTool.UndoStroke item = default(TerrainTool.UndoStroke);
			item.xmin = this.m_strokeXmin;
			item.xmax = this.m_strokeXmax;
			item.zmin = this.m_strokeZmin;
			item.zmax = this.m_strokeZmax;
			item.pointer = this.m_undoBufferFreePointer;
			this.m_undoList.Add(item);
			ushort[] undoBuffer = Singleton<TerrainManager>.get_instance().UndoBuffer;
			ushort[] backupHeights = Singleton<TerrainManager>.get_instance().BackupHeights;
			ushort[] rawHeights = Singleton<TerrainManager>.get_instance().RawHeights;
			for (int i = this.m_strokeZmin; i <= this.m_strokeZmax; i++)
			{
				for (int j = this.m_strokeXmin; j <= this.m_strokeXmax; j++)
				{
					int num4 = i * 1081 + j;
					undoBuffer[this.m_undoBufferFreePointer++] = backupHeights[num4];
					backupHeights[num4] = rawHeights[num4];
					this.m_undoBufferFreePointer %= num;
				}
			}
		}
		this.m_strokeXmin = 1080;
		this.m_strokeXmax = 0;
		this.m_strokeZmin = 1080;
		this.m_strokeZmax = 0;
	}

	public void ApplyUndo()
	{
		if (this.m_undoList.Count < 1)
		{
			return;
		}
		TerrainTool.UndoStroke undoStroke = this.m_undoList[this.m_undoList.Count - 1];
		this.m_undoList.RemoveAt(this.m_undoList.Count - 1);
		ushort[] undoBuffer = Singleton<TerrainManager>.get_instance().UndoBuffer;
		ushort[] backupHeights = Singleton<TerrainManager>.get_instance().BackupHeights;
		ushort[] rawHeights = Singleton<TerrainManager>.get_instance().RawHeights;
		int num = Singleton<TerrainManager>.get_instance().UndoBuffer.Length;
		int num2 = Singleton<TerrainManager>.get_instance().RawHeights.Length;
		int num3 = undoStroke.pointer;
		for (int i = undoStroke.zmin; i <= undoStroke.zmax; i++)
		{
			for (int j = undoStroke.xmin; j <= undoStroke.xmax; j++)
			{
				int num4 = i * 1081 + j;
				rawHeights[num4] = undoBuffer[num3];
				backupHeights[num4] = undoBuffer[num3];
				num3++;
				num3 %= num;
			}
		}
		this.m_undoBufferFreePointer = undoStroke.pointer;
		for (int k = 0; k < num2; k++)
		{
			backupHeights[k] = rawHeights[k];
		}
		int num5 = 128;
		undoStroke.xmin = Math.Max(0, undoStroke.xmin - 2);
		undoStroke.xmax = Math.Min(1080, undoStroke.xmax + 2);
		undoStroke.zmin = Math.Max(0, undoStroke.zmin - 2);
		undoStroke.zmax = Math.Min(1080, undoStroke.zmax + 2);
		for (int l = undoStroke.zmin; l <= undoStroke.zmax; l += num5 + 1)
		{
			for (int m = undoStroke.xmin; m <= undoStroke.xmax; m += num5 + 1)
			{
				TerrainModify.UpdateArea(m, l, m + num5, l + num5, true, false, false);
			}
		}
		this.m_strokeXmin = 1080;
		this.m_strokeXmax = 0;
		this.m_strokeZmin = 1080;
		this.m_strokeZmax = 0;
	}

	private void ApplyBrush()
	{
		TerrainManager instance = Singleton<TerrainManager>.get_instance();
		GameAreaManager instance2 = Singleton<GameAreaManager>.get_instance();
		SimulationManager instance3 = Singleton<SimulationManager>.get_instance();
		float[] brushData = this.m_toolController.BrushData;
		float num = this.m_brushSize * 0.5f;
		float num2 = 16f;
		int num3 = 1080;
		ushort[] rawHeights = instance.RawHeights;
		ushort[] finalHeights = instance.FinalHeights;
		ushort[] backupHeights = instance.BackupHeights;
		float strength = this.m_strength;
		int num4 = 3;
		float num5 = 0.015625f;
		float num6 = 64f;
		Vector3 mousePosition = this.m_mousePosition;
		Vector3 vector = this.m_endPosition - this.m_startPosition;
		vector.y = 0f;
		float num7 = vector.get_sqrMagnitude();
		if (num7 != 0f)
		{
			num7 = 1f / num7;
		}
		float num8 = 20f;
		bool flag = (this.m_toolController.m_mode & ItemClass.Availability.Game) != ItemClass.Availability.None;
		int num9 = 0;
		int num10 = 0;
		int num11 = this.m_currentCost;
		int num12 = instance.DirtBuffer;
		int num13 = 524288;
		int num14 = 0;
		if (flag)
		{
			if (instance2.PointOutOfArea(mousePosition))
			{
				this.m_toolErrors = ToolBase.ToolErrors.OutOfArea;
				return;
			}
			TerrainProperties properties = instance.m_properties;
			if (properties != null)
			{
				num14 = properties.m_dirtPrice;
			}
		}
		int num15 = Mathf.Max((int)((mousePosition.x - num) / num2 + (float)num3 * 0.5f), 0);
		int num16 = Mathf.Max((int)((mousePosition.z - num) / num2 + (float)num3 * 0.5f), 0);
		int num17 = Mathf.Min((int)((mousePosition.x + num) / num2 + (float)num3 * 0.5f) + 1, num3);
		int num18 = Mathf.Min((int)((mousePosition.z + num) / num2 + (float)num3 * 0.5f) + 1, num3);
		if (this.m_mode == TerrainTool.Mode.Shift)
		{
			if (this.m_mouseRightDown)
			{
				num8 = -num8;
			}
		}
		else if (this.m_mode == TerrainTool.Mode.Soften && this.m_mouseRightDown)
		{
			num4 = 10;
		}
		if (this.m_tempBuffer == null || this.m_tempBuffer.Length < (num18 - num16 + 1) * (num17 - num15 + 1))
		{
			this.m_tempBuffer = new ushort[(num18 - num16 + 1) * (num17 - num15 + 1)];
		}
		for (int i = num16; i <= num18; i++)
		{
			float num19 = ((float)i - (float)num3 * 0.5f) * num2;
			float num20 = (num19 - mousePosition.z + num) / this.m_brushSize * 64f - 0.5f;
			int num21 = Mathf.Clamp(Mathf.FloorToInt(num20), 0, 63);
			int num22 = Mathf.Clamp(Mathf.CeilToInt(num20), 0, 63);
			for (int j = num15; j <= num17; j++)
			{
				float num23 = ((float)j - (float)num3 * 0.5f) * num2;
				float num24 = (num23 - mousePosition.x + num) / this.m_brushSize * 64f - 0.5f;
				int num25 = Mathf.Clamp(Mathf.FloorToInt(num24), 0, 63);
				int num26 = Mathf.Clamp(Mathf.CeilToInt(num24), 0, 63);
				int num27 = (int)rawHeights[i * (num3 + 1) + j];
				float num28 = (float)num27 * num5;
				float num29 = 0f;
				if (flag && instance2.PointOutOfArea(new Vector3(num23, mousePosition.y, num19), num2 * 0.5f))
				{
					this.m_tempBuffer[(i - num16) * (num17 - num15 + 1) + j - num15] = (ushort)num27;
				}
				else
				{
					float num30 = brushData[num21 * 64 + num25];
					float num31 = brushData[num21 * 64 + num26];
					float num32 = brushData[num22 * 64 + num25];
					float num33 = brushData[num22 * 64 + num26];
					float num34 = num30 + (num31 - num30) * (num24 - (float)num25);
					float num35 = num32 + (num33 - num32) * (num24 - (float)num25);
					float num36 = num34 + (num35 - num34) * (num20 - (float)num21);
					num36 *= strength;
					if (num36 <= 0f)
					{
						this.m_tempBuffer[(i - num16) * (num17 - num15 + 1) + j - num15] = (ushort)num27;
					}
					else
					{
						if (this.m_mode == TerrainTool.Mode.Shift)
						{
							num29 = (float)finalHeights[i * (num3 + 1) + j] * num5 + num8;
						}
						else if (this.m_mode == TerrainTool.Mode.Level)
						{
							num29 = this.m_startPosition.y;
						}
						else if (this.m_mode == TerrainTool.Mode.Soften)
						{
							int num37 = Mathf.Max(j - num4, 0);
							int num38 = Mathf.Max(i - num4, 0);
							int num39 = Mathf.Min(j + num4, num3);
							int num40 = Mathf.Min(i + num4, num3);
							float num41 = 0f;
							for (int k = num38; k <= num40; k++)
							{
								for (int l = num37; l <= num39; l++)
								{
									float num42 = 1f - (float)((l - j) * (l - j) + (k - i) * (k - i)) / (float)(num4 * num4);
									if (num42 > 0f)
									{
										num29 += (float)finalHeights[k * (num3 + 1) + l] * (num5 * num42);
										num41 += num42;
									}
								}
							}
							if (num41 > 0.001f)
							{
								num29 /= num41;
							}
							else
							{
								num29 = (float)finalHeights[i * (num3 + 1) + j];
							}
						}
						else if (this.m_mode == TerrainTool.Mode.Slope)
						{
							float num43 = ((float)j - (float)num3 * 0.5f) * num2;
							float num44 = ((float)i - (float)num3 * 0.5f) * num2;
							float num45 = ((num43 - this.m_startPosition.x) * vector.x + (num44 - this.m_startPosition.z) * vector.z) * num7;
							num29 = Mathf.Lerp(this.m_startPosition.y, this.m_endPosition.y, num45);
						}
						float num46 = num29;
						num29 = Mathf.Lerp(num28, num29, num36);
						int num47 = Mathf.Clamp(Mathf.RoundToInt(num29 * num6), 0, 65535);
						if (num47 == num27)
						{
							int num48 = Mathf.Clamp(Mathf.RoundToInt(num46 * num6), 0, 65535);
							if (num48 > num27)
							{
								if ((num29 - num28) * num6 > (float)instance3.m_randomizer.Int32(0, 10000) * 0.0001f)
								{
									num47++;
								}
							}
							else if (num48 < num27 && (num28 - num29) * num6 > (float)instance3.m_randomizer.Int32(0, 10000) * 0.0001f)
							{
								num47--;
							}
						}
						this.m_tempBuffer[(i - num16) * (num17 - num15 + 1) + j - num15] = (ushort)num47;
						if (flag)
						{
							if (num47 > num27)
							{
								num9 += num47 - num27;
							}
							else if (num47 < num27)
							{
								num10 += num27 - num47;
							}
							int num49 = (int)backupHeights[i * (num3 + 1) + j];
							int num50 = Mathf.Abs(num47 - num49) - Mathf.Abs(num27 - num49);
							num11 += num50 * num14;
						}
					}
				}
			}
		}
		int num51 = num9;
		int num52 = num10;
		ToolBase.ToolErrors toolErrors = ToolBase.ToolErrors.None;
		if (flag)
		{
			if (num9 > num10)
			{
				num51 = Mathf.Min(num9, num12 + num10);
				if (num51 < num9)
				{
					toolErrors |= ToolBase.ToolErrors.NotEnoughDirt;
					GuideController properties2 = Singleton<GuideManager>.get_instance().m_properties;
					if (properties2 != null)
					{
						Singleton<TerrainManager>.get_instance().m_notEnoughDirt.Activate(properties2.m_notEnoughDirt);
					}
				}
				GenericGuide tooMuchDirt = Singleton<TerrainManager>.get_instance().m_tooMuchDirt;
				if (tooMuchDirt != null)
				{
					tooMuchDirt.Deactivate();
				}
			}
			else if (num10 > num9)
			{
				num52 = Mathf.Min(num10, num13 - num12 + num9);
				if (num52 < num10)
				{
					toolErrors |= ToolBase.ToolErrors.TooMuchDirt;
					GuideController properties3 = Singleton<GuideManager>.get_instance().m_properties;
					if (properties3 != null)
					{
						Singleton<TerrainManager>.get_instance().m_tooMuchDirt.Activate(properties3.m_tooMuchDirt);
					}
				}
				GenericGuide notEnoughDirt = Singleton<TerrainManager>.get_instance().m_notEnoughDirt;
				if (notEnoughDirt != null)
				{
					notEnoughDirt.Deactivate();
				}
			}
			if (num11 != Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Landscaping, num11))
			{
				this.m_toolErrors = (toolErrors | ToolBase.ToolErrors.NotEnoughMoney);
				return;
			}
			num11 = this.m_currentCost;
		}
		this.m_toolErrors = toolErrors;
		if (num51 != 0 || num52 != 0)
		{
			GenericGuide terrainToolNotUsed = Singleton<TerrainManager>.get_instance().m_terrainToolNotUsed;
			if (terrainToolNotUsed != null && !terrainToolNotUsed.m_disabled)
			{
				terrainToolNotUsed.Disable();
			}
		}
		for (int m = num16; m <= num18; m++)
		{
			for (int n = num15; n <= num17; n++)
			{
				int num53 = (int)rawHeights[m * (num3 + 1) + n];
				int num54 = (int)this.m_tempBuffer[(m - num16) * (num17 - num15 + 1) + n - num15];
				if (flag)
				{
					int num55 = num54 - num53;
					if (num55 > 0)
					{
						if (num9 > num51)
						{
							num55 = (num9 - 1 + num55 * num51) / num9;
						}
						num9 -= num54 - num53;
						num51 -= num55;
						num54 = num53 + num55;
						num12 -= num55;
					}
					else if (num55 < 0)
					{
						if (num10 > num52)
						{
							num55 = -((num10 - 1 - num55 * num52) / num10);
						}
						num10 -= num53 - num54;
						num52 += num55;
						num54 = num53 + num55;
						num12 -= num55;
					}
					int num56 = (int)backupHeights[m * (num3 + 1) + n];
					int num57 = Mathf.Abs(num54 - num56) - Mathf.Abs(num53 - num56);
					num11 += num57 * num14;
				}
				if (num54 != num53)
				{
					rawHeights[m * (num3 + 1) + n] = (ushort)num54;
					this.m_strokeXmin = Math.Min(this.m_strokeXmin, n);
					this.m_strokeXmax = Math.Max(this.m_strokeXmax, n);
					this.m_strokeZmin = Math.Min(this.m_strokeZmin, m);
					this.m_strokeZmax = Math.Max(this.m_strokeZmax, m);
				}
			}
		}
		if (flag)
		{
			instance.DirtBuffer = num12;
			this.m_currentCost = num11;
		}
		TerrainModify.UpdateArea(num15 - 2, num16 - 2, num17 + 2, num18 + 2, true, false, false);
	}

	public override ToolBase.ToolErrors GetErrors()
	{
		return this.m_toolErrors;
	}

	public TerrainTool.Mode m_mode;

	public float m_brushSize = 1f;

	public float m_strength = 0.5f;

	public Texture2D m_brush;

	public CursorInfo m_shiftCursor;

	public CursorInfo m_levelCursor;

	public CursorInfo m_softenCursor;

	public CursorInfo m_slopeCursor;

	private Vector3 m_mousePosition;

	internal Vector3 m_startPosition;

	private Vector3 m_endPosition;

	private Ray m_mouseRay;

	private float m_mouseRayLength;

	private bool m_mouseLeftDown;

	private bool m_mouseRightDown;

	private bool m_mouseRayValid;

	private ushort[] m_tempBuffer;

	private ToolBase.ToolErrors m_toolErrors;

	private int m_currentCost;

	private int m_strokeXmin;

	private int m_strokeXmax;

	private int m_strokeZmin;

	private int m_strokeZmax;

	private int m_undoBufferFreePointer;

	private List<TerrainTool.UndoStroke> m_undoList;

	private bool m_strokeInProgress;

	private SavedInputKey m_UndoKey = new SavedInputKey(Settings.mapEditorTerrainUndo, Settings.inputSettingsFile, DefaultSettings.mapEditorTerrainUndo, true);

	public enum Mode
	{
		[EnumPosition("Shift", 0)]
		Shift,
		[EnumPosition("Level", 1)]
		Level,
		[EnumPosition("Soften", 2)]
		Soften,
		[EnumPosition("Slope", 3)]
		Slope
	}

	public struct UndoStroke
	{
		public int xmin;

		public int xmax;

		public int zmin;

		public int zmax;

		public int pointer;
	}
}
