﻿using System;
using ColossalFramework.UI;
using UnityEngine;

public class ContinueButtonClicker : MonoBehaviour
{
	private void Update()
	{
		UIButton uIButton = UIView.Find<UIButton>("Continue");
		uIButton.SimulateClick();
		Object.Destroy(base.get_gameObject());
	}
}
