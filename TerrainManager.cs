﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using ColossalFramework;
using ColossalFramework.IO;
using ColossalFramework.Math;
using UnityEngine;

public class TerrainManager : SimulationManagerBase<TerrainManager, TerrainProperties>, ISimulationManager, IRenderableManager
{
	public ushort[] RawHeights
	{
		get
		{
			return this.m_rawHeights;
		}
	}

	public ushort[] RawHeights2
	{
		get
		{
			return this.m_rawHeights2;
		}
	}

	public ushort[] FinalHeights
	{
		get
		{
			return this.m_finalHeights;
		}
	}

	public ushort[] BackupHeights
	{
		get
		{
			return this.m_backupHeights;
		}
	}

	public ushort[] UndoBuffer
	{
		get
		{
			return this.m_undoBuffer;
		}
	}

	public ushort[] BlockHeights
	{
		get
		{
			return this.m_blockHeights;
		}
	}

	public ushort[] BlockHeightTargets
	{
		get
		{
			return this.m_blockHeights2;
		}
	}

	public TerrainManager.SurfaceCell[] RawSurface
	{
		get
		{
			return this.m_rawSurface;
		}
	}

	public WaterSimulation WaterSimulation
	{
		get
		{
			return this.m_waterSimulation;
		}
	}

	public bool RenderZones
	{
		get
		{
			return this.m_renderZones;
		}
		set
		{
			this.m_renderZones = value;
		}
	}

	public bool RenderTopography
	{
		get
		{
			return this.m_renderTopography;
		}
		set
		{
			this.m_renderTopography = value;
		}
	}

	public bool RenderTopographyInfo
	{
		get
		{
			return this.m_renderTopographyInfo;
		}
		set
		{
			this.m_renderTopographyInfo = value;
		}
	}

	public bool TransparentWater
	{
		get
		{
			return this.m_transparentWater;
		}
		set
		{
			this.m_transparentWater = value;
		}
	}

	public int DirtBuffer
	{
		get
		{
			return Singleton<NaturalResourceManager>.get_instance().m_ResourceWrapper.OnGetDirt(this.m_dirtBuffer);
		}
		set
		{
			this.m_dirtBuffer = value;
		}
	}

	public int RawDirtBuffer
	{
		get
		{
			return this.m_dirtBuffer;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		int maxAreaCount = Singleton<GameAreaManager>.get_instance().MaxAreaCount;
		this.m_detailHeights = new ushort[231361 * maxAreaCount];
		this.m_detailHeights2 = new ushort[231361 * maxAreaCount];
		this.m_detailSurface = new TerrainManager.SurfaceCell[230400 * maxAreaCount];
		this.m_detailZones = new TerrainManager.ZoneCell[230400 * maxAreaCount];
		this.m_rawHeights = new ushort[1168561];
		this.m_rawHeights2 = new ushort[1168561];
		this.m_backupHeights = new ushort[1168561];
		this.m_undoBuffer = new ushort[3505684];
		this.m_finalHeights = new ushort[1168561];
		this.m_blockHeights = new ushort[1168561];
		this.m_blockHeights2 = new ushort[1168561];
		this.m_rawSurface = new TerrainManager.SurfaceCell[1166400];
		this.m_patches = new TerrainPatch[81];
		this.m_tempHeights = new TerrainModify.HeightModification[263169];
		this.m_tempSurface = new TerrainManager.SurfaceCell[263169];
		this.m_tempZones = new TerrainModify.ZoneModification[263169];
		this.m_rawBounds = new TerrainManager.CellBounds[5][];
		int num = 81;
		for (int i = 0; i <= 4; i++)
		{
			this.m_rawBounds[i] = new TerrainManager.CellBounds[num];
			num <<= 2;
		}
		this.m_heightsModified = new TerrainArea(0, 0, 1081);
		this.m_surfaceModified = new TerrainArea(0, 0, 1080);
		this.m_zonesModified = new TerrainArea(0, 0, 1080);
		this.m_tmpSurfaceModified = new TerrainArea(0, 0, 129);
		this.m_tmpZonesModified = new TerrainArea(0, 0, 128);
		this.m_modifiedBlockMinX = new short[1081];
		this.m_modifiedBlockMaxX = new short[1081];
		this.m_terrainLayer = LayerMask.NameToLayer("Terrain");
		this.m_decorationLayer = LayerMask.NameToLayer("Decoration");
		this.m_waterLayer = LayerMask.NameToLayer("Water");
		this.ID_MainTex = Shader.PropertyToID("_MainTex");
		this.ID_SurfaceTexA = Shader.PropertyToID("_SurfaceTexA");
		this.ID_SurfaceTexB = Shader.PropertyToID("_SurfaceTexB");
		this.ID_SurfaceANew = Shader.PropertyToID("_SurfaceANew");
		this.ID_SurfaceAOld = Shader.PropertyToID("_SurfaceAOld");
		this.ID_SurfaceBNew = Shader.PropertyToID("_SurfaceBNew");
		this.ID_SurfaceBOld = Shader.PropertyToID("_SurfaceBOld");
		this.ID_TerrainHeight = Shader.PropertyToID("_TerrainHeight");
		this.ID_ZoneLayout = Shader.PropertyToID("_ZoneLayout");
		this.ID_DecorationDiffuse = Shader.PropertyToID("_DecorationDiffuse");
		this.ID_DecorationXYCA = Shader.PropertyToID("_DecorationXYCA");
		this.ID_HeightMapping = Shader.PropertyToID("_HeightMapping");
		this.ID_SurfaceMapping = Shader.PropertyToID("_SurfaceMapping");
		this.ID_TerrainMapping = Shader.PropertyToID("_TerrainMapping");
		this.m_materialBlock1 = new MaterialPropertyBlock();
		this.m_materialBlock2 = new MaterialPropertyBlock();
		for (int j = 0; j <= 1080; j++)
		{
			for (int k = 0; k <= 1080; k++)
			{
				float num2 = 60f;
				this.m_rawHeights[j * 1081 + k] = (ushort)(num2 * 64f);
				this.m_rawHeights2[j * 1081 + k] = (ushort)(num2 * 64f);
				this.m_finalHeights[j * 1081 + k] = (ushort)(num2 * 64f);
				this.m_blockHeights[j * 1081 + k] = (ushort)(num2 * 64f);
				this.m_blockHeights2[j * 1081 + k] = 0;
			}
		}
		TerrainMesh.GenerateMeshes();
		DecorationMesh.GenerateMesh();
		this.m_updateLock = new object();
		for (int l = 0; l < 9; l++)
		{
			for (int m = 0; m < 9; m++)
			{
				this.m_patches[l * 9 + m] = new TerrainPatch(m, l);
			}
		}
		this.m_waterSimulation = base.get_gameObject().AddComponent<WaterSimulation>();
		this.m_waterSimulation.Initialize(this.m_blockHeights);
		this.m_decoRenderer = base.get_gameObject().AddComponent<DecorationRenderer>();
		Singleton<LoadingManager>.get_instance().m_metaDataReady += new LoadingManager.MetaDataReadyHandler(this.CreateRelay);
		Singleton<LoadingManager>.get_instance().m_levelUnloaded += new LoadingManager.LevelUnloadedHandler(this.ReleaseRelay);
	}

	private void OnDestroy()
	{
		this.ReleaseRelay();
		if (this.m_patches != null)
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					this.m_patches[i * 9 + j].DestroyPatch();
				}
			}
		}
		if (this.m_terrainMaterial != null)
		{
			Object.Destroy(this.m_terrainMaterial);
			this.m_terrainMaterial = null;
		}
		if (this.m_decorationMaterial != null)
		{
			Object.Destroy(this.m_decorationMaterial);
			this.m_decorationMaterial = null;
		}
		if (this.m_waterMaterial != null)
		{
			Object.Destroy(this.m_waterMaterial);
			this.m_waterMaterial = null;
		}
		if (this.m_waterTransparentMaterial != null)
		{
			Object.Destroy(this.m_waterTransparentMaterial);
			this.m_waterTransparentMaterial = null;
		}
		TerrainMesh.DestroyMeshes();
		DecorationMesh.DestroyMesh();
	}

	private void CreateRelay()
	{
		if (this.m_TerrainWrapper == null)
		{
			this.m_TerrainWrapper = new TerrainWrapper(this);
		}
	}

	private void ReleaseRelay()
	{
		if (this.m_TerrainWrapper != null)
		{
			this.m_TerrainWrapper.Release();
			this.m_TerrainWrapper = null;
		}
	}

	public static void RegisterTerrainManager(ITerrainManager manager)
	{
		if (manager != null)
		{
			TerrainManager.m_managers.Add(manager);
		}
	}

	public override void InitializeProperties(TerrainProperties properties)
	{
		base.InitializeProperties(properties);
		this.m_terrainMaterial = new Material(this.m_properties.m_terrainShader);
		this.m_topographyMaterial = new Material(this.m_properties.m_topographyShader);
		this.m_decorationMaterial = new Material(this.m_properties.m_decorationShader);
		this.m_waterMaterial = new Material(this.m_properties.m_waterShader);
		this.m_waterTransparentMaterial = new Material(this.m_properties.m_waterTransparentShader);
	}

	public override void DestroyProperties(TerrainProperties properties)
	{
		if (properties == this.m_properties)
		{
			if (this.m_terrainMaterial != null)
			{
				Object.Destroy(this.m_terrainMaterial);
				this.m_terrainMaterial = null;
			}
			if (this.m_topographyMaterial != null)
			{
				Object.Destroy(this.m_topographyMaterial);
				this.m_topographyMaterial = null;
			}
			if (this.m_decorationMaterial != null)
			{
				Object.Destroy(this.m_decorationMaterial);
				this.m_decorationMaterial = null;
			}
			if (this.m_waterMaterial != null)
			{
				Object.Destroy(this.m_waterMaterial);
				this.m_waterMaterial = null;
			}
			if (this.m_waterTransparentMaterial != null)
			{
				Object.Destroy(this.m_waterTransparentMaterial);
				this.m_waterTransparentMaterial = null;
			}
		}
		base.DestroyProperties(properties);
	}

	[DebuggerHidden]
	private IEnumerator UpdateTerrainPatch(int index)
	{
		TerrainManager.<UpdateTerrainPatch>c__Iterator0 <UpdateTerrainPatch>c__Iterator = new TerrainManager.<UpdateTerrainPatch>c__Iterator0();
		<UpdateTerrainPatch>c__Iterator.index = index;
		<UpdateTerrainPatch>c__Iterator.$this = this;
		return <UpdateTerrainPatch>c__Iterator;
	}

	protected override void EndRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		uint num;
		uint num2;
		this.m_waterSimulation.BeginTerrainUpdate(out num, out num2);
		int num3 = (int)((num & 63u) * 81u) >> 6;
		int num4 = (int)((num2 & 63u) * 81u) >> 6;
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				int num5 = i * 9 + j;
				bool flag = num5 >= num3;
				bool flag2 = num5 < num4;
				bool flag3 = num4 < num3;
				bool updateWater = (flag && flag2) || ((flag || flag2) && flag3);
				uint waterFrame = (!flag2) ? num : num2;
				this.m_patches[num5].Refresh(updateWater, waterFrame);
				this.m_patches[num5].Render(cameraInfo);
			}
		}
		this.m_waterSimulation.EndTerrainUpdate(num2);
		uint num6 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		float num7 = ((num6 & 63u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.015625f;
		Shader.SetGlobalVector("_TerrainStatus", new Vector4((!this.m_renderZones) ? 0f : 1f, num7, 0f, this.m_waterSimulation.m_nextSeaLevel));
	}

	protected override void BeginRenderingImpl(RenderManager.CameraInfo cameraInfo)
	{
		this.m_decoRenderer.BeginRendering(cameraInfo);
	}

	protected override void BeginOverlayImpl(RenderManager.CameraInfo cameraInfo)
	{
		if (this.m_renderTopography || this.m_renderTopographyInfo)
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					int num = i * 9 + j;
					this.m_patches[num].RenderOverlay(cameraInfo, this.m_topographyMaterial, false);
				}
			}
		}
		if (this.m_renderZones)
		{
			for (int k = 0; k < 9; k++)
			{
				for (int l = 0; l < 9; l++)
				{
					int num2 = k * 9 + l;
					this.m_patches[num2].RenderOverlay(cameraInfo, Singleton<ZoneManager>.get_instance().m_zoneMaterial, true);
				}
			}
		}
	}

	public void GetSurfaceMapping(Vector3 worldPos, out Texture _SurfaceTexA, out Texture _SurfaceTexB, out Vector4 _SurfaceMapping)
	{
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((worldPos.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((worldPos.z + num * 0.5f) / num2), 0, 8);
		TerrainPatch terrainPatch = this.m_patches[num4 * 9 + num3];
		_SurfaceTexA = terrainPatch.m_surfaceMapA;
		_SurfaceTexB = terrainPatch.m_surfaceMapB;
		_SurfaceMapping = ((terrainPatch.m_rndDetailIndex == 0) ? terrainPatch.m_surfaceMappingRaw : terrainPatch.m_surfaceMappingDetail);
	}

	public void GetHeightMapping(Vector3 worldPos, out Texture _HeightMap, out Vector4 _HeightMapping, out Vector4 _SurfaceMapping)
	{
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((worldPos.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((worldPos.z + num * 0.5f) / num2), 0, 8);
		TerrainPatch terrainPatch = this.m_patches[num4 * 9 + num3];
		_HeightMap = terrainPatch.m_heightMap;
		_HeightMapping = ((terrainPatch.m_rndDetailIndex == 0) ? terrainPatch.m_heightMappingRaw : terrainPatch.m_heightMappingDetail);
		_SurfaceMapping = ((terrainPatch.m_rndDetailIndex == 0) ? terrainPatch.m_surfaceMappingRaw : terrainPatch.m_surfaceMappingDetail);
	}

	public void GetWaterMapping(Vector3 worldPos, out Texture _HeightMap, out Vector4 _HeightMapping, out Vector4 _SurfaceMapping)
	{
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((worldPos.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((worldPos.z + num * 0.5f) / num2), 0, 8);
		TerrainPatch terrainPatch = this.m_patches[num4 * 9 + num3];
		uint num5 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
		uint num6 = num5 - 64u >> 6 & 1u;
		_HeightMap = terrainPatch.m_waterHeight[(int)((UIntPtr)num6)].m_waterHeight;
		_HeightMapping = terrainPatch.m_heightMappingRaw;
		_SurfaceMapping = terrainPatch.m_surfaceMappingRaw;
	}

	public float GetUnlockableTerrainFlatness()
	{
		float num = 0f;
		int num2 = 2;
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				num += this.GetTileFlatness(j + num2, i + num2);
			}
		}
		return num / 25f;
	}

	public float GetTileFlatness(int x, int z)
	{
		int num = 2;
		int num2 = x + num;
		int num3 = z + num;
		TerrainPatch terrainPatch = this.m_patches[num3 * 9 + num2];
		return terrainPatch.m_flatness;
	}

	public TerrainManager.ZoneCell GetZoneCell(int x, int z)
	{
		int num = Mathf.Min(x / 480, 8);
		int num2 = Mathf.Min(z / 480, 8);
		int num3 = num2 * 9 + num;
		int simDetailIndex = this.m_patches[num3].m_simDetailIndex;
		if (simDetailIndex != 0)
		{
			int num4 = x - num * 480;
			int num5 = z - num2 * 480;
			int num6 = (simDetailIndex - 1) * 480 * 480;
			return this.m_detailZones[num6 + num5 * 480 + num4];
		}
		return new TerrainManager.ZoneCell
		{
			m_zone = 15
		};
	}

	public TerrainManager.SurfaceCell GetSurfaceCell(int x, int z)
	{
		int num = Mathf.Min(x / 480, 8);
		int num2 = Mathf.Min(z / 480, 8);
		int num3 = num2 * 9 + num;
		int simDetailIndex = this.m_patches[num3].m_simDetailIndex;
		if (simDetailIndex == 0)
		{
			return this.SampleRawSurface((float)x * 0.25f, (float)z * 0.25f);
		}
		int num4 = (simDetailIndex - 1) * 480 * 480;
		int num5 = x - num * 480;
		int num6 = z - num2 * 480;
		if ((num5 == 0 && num != 0 && this.m_patches[num3 - 1].m_simDetailIndex == 0) || (num6 == 0 && num2 != 0 && this.m_patches[num3 - 9].m_simDetailIndex == 0))
		{
			TerrainManager.SurfaceCell result = this.SampleRawSurface((float)x * 0.25f, (float)z * 0.25f);
			result.m_clipped = this.m_detailSurface[num4 + num6 * 480 + num5].m_clipped;
			return result;
		}
		if ((num5 == 479 && num != 8 && this.m_patches[num3 + 1].m_simDetailIndex == 0) || (num6 == 479 && num2 != 8 && this.m_patches[num3 + 9].m_simDetailIndex == 0))
		{
			TerrainManager.SurfaceCell result2 = this.SampleRawSurface((float)x * 0.25f, (float)z * 0.25f);
			result2.m_clipped = this.m_detailSurface[num4 + num6 * 480 + num5].m_clipped;
			return result2;
		}
		return this.m_detailSurface[num4 + num6 * 480 + num5];
	}

	public void SetTerrainMaterialProperties(Vector3 worldPos, MaterialPropertyBlock materialBlock)
	{
		this.m_patches[this.GetPatchIndex(worldPos)].SetTerrainMaterialProperties(materialBlock);
	}

	public void SetWaterMaterialProperties(Vector3 worldPos, MaterialPropertyBlock materialBlock)
	{
		this.m_patches[this.GetPatchIndex(worldPos)].SetWaterMaterialProperties(materialBlock);
	}

	public void SetWaterMaterialProperties(Vector3 worldPos, Material material)
	{
		this.m_patches[this.GetPatchIndex(worldPos)].SetWaterMaterialProperties(material);
	}

	public int GetPatchIndex(Vector3 worldPos)
	{
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((worldPos.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((worldPos.z + num * 0.5f) / num2), 0, 8);
		return num4 * 9 + num3;
	}

	public float CalculateWaterProximity(Vector3 position, float radius, out float velocity)
	{
		int num = Mathf.Max((int)((position.x - radius + 8640f) / 16f + 0.5f), 0);
		int num2 = Mathf.Max((int)((position.z - radius + 8640f) / 16f + 0.5f), 0);
		int num3 = Mathf.Min((int)((position.x + radius + 8640f) / 16f + 0.5f), 1080);
		int num4 = Mathf.Min((int)((position.z + radius + 8640f) / 16f + 0.5f), 1080);
		radius *= radius;
		float num5 = 0f;
		float num6 = 0f;
		float num7 = 0f;
		float num8 = 0f;
		float num9 = 0f;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			for (int i = num2; i <= num4; i += 2)
			{
				float num10 = ((float)i - 540f + 0.5f) * 16f - position.z;
				num10 *= num10;
				for (int j = num; j <= num3; j += 2)
				{
					float num11 = ((float)j - 540f + 0.5f) * 16f - position.x;
					num11 *= num11;
					if (num10 + num11 < radius)
					{
						WaterSimulation.Cell cell = array[i * 1081 + j];
						float num12 = 1f - (num10 + num11) / radius;
						num5 += (float)Mathf.Min((int)cell.m_height, 128) * num12;
						num7 += (float)Mathf.Abs((int)cell.m_velocityX) * num12;
						num8 += (float)Mathf.Abs((int)cell.m_velocityZ) * num12;
						num6 += 128f * num12;
						num9 += (float)cell.m_height * num12;
					}
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num6 != 0f)
		{
			num5 /= num6;
		}
		num5 = Mathf.Clamp01(num5 * 2f);
		velocity = num7 + num8;
		if (num9 != 0f)
		{
			velocity /= num9;
		}
		velocity = Mathf.Clamp01(velocity * 4f);
		return num5;
	}

	public bool SetDetailedPatch(int x, int z)
	{
		int maxAreaCount = Singleton<GameAreaManager>.get_instance().MaxAreaCount;
		if (this.m_detailPatchCount < maxAreaCount)
		{
			int num = 231361 * (this.m_detailPatchCount + 1);
			if (this.m_detailHeights.Length < num)
			{
				CODebugBase<LogChannel>.Log(LogChannel.Core, "Resizing terrain detail buffers");
				ushort[] array = new ushort[231361 * maxAreaCount];
				for (int i = 0; i < this.m_detailHeights.Length; i++)
				{
					array[i] = this.m_detailHeights[i];
				}
				this.m_detailHeights = array;
				ushort[] array2 = new ushort[231361 * maxAreaCount];
				for (int j = 0; j < this.m_detailHeights2.Length; j++)
				{
					array2[j] = this.m_detailHeights2[j];
				}
				this.m_detailHeights2 = array2;
				TerrainManager.SurfaceCell[] array3 = new TerrainManager.SurfaceCell[230400 * maxAreaCount];
				for (int k = 0; k < this.m_detailSurface.Length; k++)
				{
					array3[k] = this.m_detailSurface[k];
				}
				this.m_detailSurface = array3;
				TerrainManager.ZoneCell[] array4 = new TerrainManager.ZoneCell[230400 * maxAreaCount];
				for (int l = 0; l < this.m_detailZones.Length; l++)
				{
					array4[l] = this.m_detailZones[l];
				}
				this.m_detailZones = array4;
			}
			int num2 = z * 9 + x;
			this.m_patches[num2].m_simDetailIndex = ++this.m_detailPatchCount;
			if (Singleton<LoadingManager>.get_instance().m_loadingComplete)
			{
				int num3 = 120;
				TerrainModify.UpdateArea(x * num3 - 4, z * num3 - 4, (x + 1) * num3 + 4, (z + 1) * num3 + 4, true, true, true);
			}
			return true;
		}
		return false;
	}

	public bool HasDetailMapping(Vector3 worldPos)
	{
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((worldPos.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((worldPos.z + num * 0.5f) / num2), 0, 8);
		TerrainPatch terrainPatch = this.m_patches[num4 * 9 + num3];
		return terrainPatch.m_simDetailIndex != 0;
	}

	public void SetHeightMap(Color32[] map, int scale)
	{
		for (int i = 0; i <= 1080; i++)
		{
			for (int j = 0; j <= 1080; j++)
			{
				Color32 color = map[i * 1081 + j];
				int num = (int)(color.r + color.g + color.b) * scale / 765;
				this.m_rawHeights[i * 1081 + j] = (ushort)Mathf.Min(num, 65535);
			}
		}
		int num2 = 120;
		for (int k = 0; k < 9; k++)
		{
			for (int l = 0; l < 9; l++)
			{
				TerrainModify.UpdateArea(l * num2, k * num2, (l + 1) * num2, (k + 1) * num2, true, true, true);
			}
		}
	}

	public void SetHeightMap8(byte[] map, int scale)
	{
		for (int i = 0; i <= 1080; i++)
		{
			for (int j = 0; j <= 1080; j++)
			{
				int num = (int)map[i * 1081 + j] * scale / 255;
				this.m_rawHeights[i * 1081 + j] = (ushort)Mathf.Min(num, 65535);
			}
		}
		int num2 = 120;
		for (int k = 0; k < 9; k++)
		{
			for (int l = 0; l < 9; l++)
			{
				TerrainModify.UpdateArea(l * num2, k * num2, (l + 1) * num2, (k + 1) * num2, true, true, true);
			}
		}
	}

	public byte[] GetHeightmap()
	{
		byte[] array = new byte[2337122];
		int num = 0;
		for (int i = 0; i <= 1080; i++)
		{
			int num2 = i * 1081 * 2;
			for (int j = 0; j <= 1080; j++)
			{
				ushort num3 = this.m_rawHeights[num++];
				array[num2++] = (byte)num3;
				array[num2++] = (byte)(num3 >> 8);
			}
		}
		return array;
	}

	public void SetHeightMap16(byte[] map)
	{
		int num = 0;
		for (int i = 0; i <= 1080; i++)
		{
			int num2 = i * 1081 * 2;
			for (int j = 0; j <= 1080; j++)
			{
				uint num3 = (uint)map[num2++];
				num3 |= (uint)((uint)map[num2++] << 8);
				this.m_rawHeights[num++] = (ushort)num3;
			}
		}
		int num4 = 120;
		for (int k = 0; k < 9; k++)
		{
			for (int l = 0; l < 9; l++)
			{
				TerrainModify.UpdateArea(l * num4, k * num4, (l + 1) * num4, (k + 1) * num4, true, true, true);
			}
		}
	}

	public void SetRawHeightMap(byte[] map)
	{
		int num = 0;
		for (int i = 0; i <= 1080; i++)
		{
			int num2 = (1080 - i) * 1081 * 2;
			for (int j = 0; j <= 1080; j++)
			{
				uint num3 = (uint)map[num2++];
				num3 |= (uint)((uint)map[num2++] << 8);
				this.m_rawHeights[num++] = (ushort)num3;
			}
		}
		int num4 = 120;
		for (int k = 0; k < 9; k++)
		{
			for (int l = 0; l < 9; l++)
			{
				TerrainModify.UpdateArea(l * num4, k * num4, (l + 1) * num4, (k + 1) * num4, true, true, true);
			}
		}
	}

	public bool RayCast(Segment3 ray, out Vector3 hit)
	{
		Segment3 ray2;
		ray2..ctor(ray.a, ray.a);
		Segment3 ray3;
		ray3..ctor(ray.b, ray.b);
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(17280f, 1024f, 17280f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			int num = Mathf.FloorToInt(ray.a.x / 16f + 540f);
			int num2 = Mathf.FloorToInt(ray.a.z / 16f + 540f);
			int num3 = Mathf.FloorToInt(ray.b.x / 16f + 540f);
			int num4 = Mathf.FloorToInt(ray.b.z / 16f + 540f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 != 0f)
				{
					vector *= 16f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 != 0f)
				{
					vector *= 16f / num6;
				}
			}
			Vector3 vector2 = ray.a;
			Vector3 vector3 = ray.a;
			Vector3 vector4;
			float num15;
			while (true)
			{
				vector4 = vector3 + vector;
				int num9;
				int num10;
				int num11;
				int num12;
				if (num7 != 0)
				{
					num9 = Mathf.Max(num, 0);
					num10 = Mathf.Min(num, 1079);
					num11 = Mathf.Max(Mathf.FloorToInt(Mathf.Min(vector2.z, vector4.z) / 16f + 540f), 0);
					num12 = Mathf.Min(Mathf.FloorToInt(Mathf.Max(vector2.z, vector4.z) / 16f + 540f), 1079);
				}
				else
				{
					num11 = Mathf.Max(num2, 0);
					num12 = Mathf.Min(num2, 1079);
					num9 = Mathf.Max(Mathf.FloorToInt(Mathf.Min(vector2.x, vector4.x) / 16f + 540f), 0);
					num10 = Mathf.Min(Mathf.FloorToInt(Mathf.Max(vector2.x, vector4.x) / 16f + 540f), 1079);
				}
				float num13 = Mathf.Min(vector2.y, vector4.y);
				float num14 = Mathf.Max(vector2.y, vector4.y);
				num15 = 2f;
				for (int i = num11; i <= num12; i++)
				{
					for (int j = num9; j <= num10; j++)
					{
						float num16 = (float)this.m_finalHeights[i * 1081 + j] * 0.015625f;
						float num17 = (float)this.m_finalHeights[i * 1081 + j + 1] * 0.015625f;
						float num18 = (float)this.m_finalHeights[(i + 1) * 1081 + j] * 0.015625f;
						float num19 = (float)this.m_finalHeights[(i + 1) * 1081 + j + 1] * 0.015625f;
						if (Mathf.Min(Mathf.Min(num16, num17), Mathf.Min(num18, num19)) <= num14 && Mathf.Max(Mathf.Max(num16, num17), Mathf.Max(num18, num19)) >= num13)
						{
							float num20 = ((float)j - 540f) * 16f;
							float num21 = ((float)i - 540f) * 16f;
							float num22 = (num16 + num17 + num18 + num19) * 0.25f;
							Vector3 vector5;
							vector5..ctor(num20, num16, num21);
							Vector3 vector6;
							vector6..ctor(num20 + 16f, num17, num21);
							Vector3 vector7;
							vector7..ctor(num20, num18, num21 + 16f);
							Vector3 vector8;
							vector8..ctor(num20 + 16f, num19, num21 + 16f);
							Vector3 vector9;
							vector9..ctor(num20 + 8f, num22, num21 + 8f);
							float num23;
							float num24;
							float num25;
							if (Triangle3.Intersect(vector9, vector5, vector6, vector2, vector4, ref num23, ref num24, ref num25))
							{
								num15 = Mathf.Min(num15, num25);
							}
							if (Triangle3.Intersect(vector9, vector6, vector8, vector2, vector4, ref num23, ref num24, ref num25))
							{
								num15 = Mathf.Min(num15, num25);
							}
							if (Triangle3.Intersect(vector9, vector8, vector7, vector2, vector4, ref num23, ref num24, ref num25))
							{
								num15 = Mathf.Min(num15, num25);
							}
							if (Triangle3.Intersect(vector9, vector7, vector5, vector2, vector4, ref num23, ref num24, ref num25))
							{
								num15 = Mathf.Min(num15, num25);
							}
						}
					}
				}
				if (num15 != 2f)
				{
					break;
				}
				vector2 = vector3;
				vector3 = vector4;
				num += num7;
				num2 += num8;
				if ((num > num3 && num7 > 0) || (num < num3 && num7 < 0) || (num2 > num4 && num8 > 0) || (num2 < num4 && num8 < 0))
				{
					goto IL_564;
				}
			}
			hit = vector2 + (vector4 - vector2) * num15;
			return true;
			IL_564:
			ray2.b = ray.a;
			ray3.a = ray.b;
		}
		else
		{
			ray2.b = ray3.b;
		}
		if (this.RayCastEdge(ray2, out hit))
		{
			return true;
		}
		if (this.RayCastEdge(ray3, out hit))
		{
			return true;
		}
		hit = Vector3.get_zero();
		return false;
	}

	private bool RayCastEdge(Segment3 ray, out Vector3 hit)
	{
		Bounds bounds;
		bounds..ctor(new Vector3(0f, 512f, 0f), new Vector3(172800f, 1024f, 172800f));
		if (ray.Clip(bounds))
		{
			Vector3 vector = ray.b - ray.a;
			int num = Mathf.FloorToInt(ray.a.x / 16f + 540f);
			int num2 = Mathf.FloorToInt(ray.a.z / 16f + 540f);
			int num3 = Mathf.FloorToInt(ray.b.x / 16f + 540f);
			int num4 = Mathf.FloorToInt(ray.b.z / 16f + 540f);
			float num5 = Mathf.Abs(vector.x);
			float num6 = Mathf.Abs(vector.z);
			int num7;
			int num8;
			if (num5 >= num6)
			{
				num7 = ((vector.x <= 0f) ? -1 : 1);
				num8 = 0;
				if (num5 != 0f)
				{
					vector *= 16f / num5;
				}
			}
			else
			{
				num7 = 0;
				num8 = ((vector.z <= 0f) ? -1 : 1);
				if (num6 != 0f)
				{
					vector *= 16f / num6;
				}
			}
			Vector3 vector2 = ray.a;
			Vector3 vector3 = ray.a;
			Vector3 vector4;
			float num15;
			while (true)
			{
				vector4 = vector3 + vector;
				int num9;
				int num10;
				int num11;
				int num12;
				if (num7 != 0)
				{
					num9 = num;
					num10 = num;
					num11 = Mathf.FloorToInt(Mathf.Min(vector2.z, vector4.z) / 16f + 540f);
					num12 = Mathf.FloorToInt(Mathf.Max(vector2.z, vector4.z) / 16f + 540f);
				}
				else
				{
					num11 = num2;
					num12 = num2;
					num9 = Mathf.FloorToInt(Mathf.Min(vector2.x, vector4.x) / 16f + 540f);
					num10 = Mathf.FloorToInt(Mathf.Max(vector2.x, vector4.x) / 16f + 540f);
				}
				float num13 = Mathf.Min(vector2.y, vector4.y);
				float num14 = Mathf.Max(vector2.y, vector4.y);
				num15 = 2f;
				for (int i = num11; i <= num12; i++)
				{
					int j = num9;
					while (j <= num10)
					{
						float num16;
						float num17;
						float num18;
						float num19;
						if (j < 0)
						{
							if (i < 0)
							{
								num16 = (float)this.m_finalHeights[0] * 0.015625f;
								num17 = num16;
								num18 = num16;
								num19 = num16;
							}
							else if (i >= 1080)
							{
								num16 = (float)this.m_finalHeights[1167480] * 0.015625f;
								num17 = num16;
								num18 = num16;
								num19 = num16;
							}
							else
							{
								num16 = (float)this.m_finalHeights[i * 1081] * 0.015625f;
								num17 = num16;
								num18 = (float)this.m_finalHeights[(i + 1) * 1081] * 0.015625f;
								num19 = num18;
							}
							goto IL_458;
						}
						if (j >= 1080)
						{
							if (i < 0)
							{
								num16 = (float)this.m_finalHeights[1080] * 0.015625f;
								num17 = num16;
								num18 = num16;
								num19 = num16;
							}
							else if (i >= 1080)
							{
								num16 = (float)this.m_finalHeights[1168560] * 0.015625f;
								num17 = num16;
								num18 = num16;
								num19 = num16;
							}
							else
							{
								num16 = (float)this.m_finalHeights[i * 1081 + 1080] * 0.015625f;
								num17 = num16;
								num18 = (float)this.m_finalHeights[(i + 1) * 1081 + 1080] * 0.015625f;
								num19 = num18;
							}
							goto IL_458;
						}
						if (i < 0)
						{
							num16 = (float)this.m_finalHeights[j] * 0.015625f;
							num17 = (float)this.m_finalHeights[j + 1] * 0.015625f;
							num18 = num16;
							num19 = num17;
							goto IL_458;
						}
						if (i >= 1080)
						{
							num16 = (float)this.m_finalHeights[1167480 + j] * 0.015625f;
							num17 = (float)this.m_finalHeights[1167480 + j + 1] * 0.015625f;
							num18 = num16;
							num19 = num17;
							goto IL_458;
						}
						IL_54C:
						j++;
						continue;
						IL_458:
						if (Mathf.Min(Mathf.Min(num16, num17), Mathf.Min(num18, num19)) > num14 || Mathf.Max(Mathf.Max(num16, num17), Mathf.Max(num18, num19)) < num13)
						{
							goto IL_54C;
						}
						float num20 = ((float)j - 540f) * 16f;
						float num21 = ((float)i - 540f) * 16f;
						Vector3 vector5;
						vector5..ctor(num20, num16, num21);
						Vector3 vector6;
						vector6..ctor(num20 + 16f, num17, num21);
						Vector3 vector7;
						vector7..ctor(num20, num18, num21 + 16f);
						Vector3 vector8;
						vector8..ctor(num20 + 16f, num19, num21 + 16f);
						float num22;
						float num23;
						float num24;
						if (Triangle3.Intersect(vector5, vector7, vector6, vector2, vector4, ref num22, ref num23, ref num24))
						{
							num15 = Mathf.Min(num15, num24);
						}
						if (Triangle3.Intersect(vector8, vector6, vector7, vector2, vector4, ref num22, ref num23, ref num24))
						{
							num15 = Mathf.Min(num15, num24);
							goto IL_54C;
						}
						goto IL_54C;
					}
				}
				if (num15 != 2f)
				{
					break;
				}
				vector2 = vector3;
				vector3 = vector4;
				num += num7;
				num2 += num8;
				if ((num > num3 && num7 > 0) || (num < num3 && num7 < 0) || (num2 > num4 && num8 > 0) || (num2 < num4 && num8 < 0))
				{
					goto IL_5E7;
				}
			}
			hit = vector2 + (vector4 - vector2) * num15;
			return true;
		}
		IL_5E7:
		hit = Vector3.get_zero();
		return false;
	}

	public void CalculateAreaHeight(float minX, float minZ, float maxX, float maxZ, out int min, out int avg, out int max)
	{
		int num = Mathf.Max((int)(minX / 16f + 540f), 0);
		int num2 = Mathf.Max((int)(minZ / 16f + 540f), 0);
		int num3 = Mathf.Min((int)(maxX / 16f + 540f), 1080);
		int num4 = Mathf.Min((int)(maxZ / 16f + 540f), 1080);
		min = 65535;
		avg = 0;
		max = 0;
		for (int i = num2; i <= num4; i++)
		{
			int num5 = i * 1081;
			for (int j = num; j <= num3; j++)
			{
				int num6 = num5 + j;
				int num7 = (int)this.m_blockHeights2[num6];
				if (num7 < min)
				{
					min = num7;
				}
				avg += num7;
				if (num7 > max)
				{
					max = num7;
				}
			}
		}
		if (avg != 0)
		{
			avg /= (num3 - num + 1) * (num4 - num2 + 1);
		}
	}

	public float SampleOriginalRawHeightSmooth(Vector3 worldPos)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		return this.SampleOriginalRawHeightSmooth(x, z) * 0.015625f;
	}

	public float SampleRawHeightSmooth(Vector3 worldPos)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		return this.SampleRawHeightSmooth(x, z) * 0.015625f;
	}

	public float SampleBlockHeightSmooth(Vector3 worldPos)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		return this.SampleBlockHeightSmooth(x, z) * 0.015625f;
	}

	public float SampleRawHeightSmoothWithWater(Vector3 worldPos, bool timeLerp, float waterOffset)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		return this.SampleRawHeightSmoothWithWater(x, z, timeLerp, waterOffset) * 0.015625f;
	}

	public float SampleBlockHeightSmoothWithWater(Vector3 worldPos, bool timeLerp, float waterOffset)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		bool flag;
		return this.SampleBlockHeightSmoothWithWater(x, z, timeLerp, waterOffset, out flag) * 0.015625f;
	}

	public float SampleBlockHeightSmoothWithWater(Vector3 worldPos, bool timeLerp, float waterOffset, out bool hasWater)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		return this.SampleBlockHeightSmoothWithWater(x, z, timeLerp, waterOffset, out hasWater) * 0.015625f;
	}

	public float SampleRawHeightWithWater(Vector3 worldPos, bool timeLerp, float waterOffset)
	{
		float x = worldPos.x / 16f + 540f;
		float z = worldPos.z / 16f + 540f;
		return this.SampleRawHeightWithWater(x, z, timeLerp, waterOffset) * 0.015625f;
	}

	public float SampleDetailHeight(Vector3 worldPos)
	{
		float x = worldPos.x / 4f + 2160f;
		float z = worldPos.z / 4f + 2160f;
		return this.SampleDetailHeight(x, z) * 0.015625f;
	}

	public float SampleDetailHeightSmooth(Vector3 worldPos)
	{
		float x = worldPos.x / 4f + 2160f;
		float z = worldPos.z / 4f + 2160f;
		return this.SampleDetailHeightSmooth(x, z) * 0.015625f;
	}

	public float SampleDetailHeight(Vector3 worldPos, out float slopeX, out float slopeZ)
	{
		float x = worldPos.x / 4f + 2160f;
		float z = worldPos.z / 4f + 2160f;
		float num = this.SampleDetailHeight(x, z, out slopeX, out slopeZ);
		num *= 0.015625f;
		slopeX *= 0.00390625f;
		slopeZ *= 0.00390625f;
		return num;
	}

	public TerrainManager.SurfaceCell SampleDetailSurface(Vector3 worldPos)
	{
		float x = worldPos.x / 4f + 2160f;
		float z = worldPos.z / 4f + 2160f;
		return this.SampleDetailSurface(x, z);
	}

	public float SampleRawHeight(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num, 0, 1080);
		int num4 = Mathf.Clamp(num + 1, 0, 1080);
		int num5 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num6 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		float num7 = x - (float)num;
		float num8 = z - (float)num2;
		float num9 = Mathf.Lerp((float)this.m_rawHeights2[num5 + num3], (float)this.m_rawHeights2[num5 + num4], num7);
		float num10 = Mathf.Lerp((float)this.m_rawHeights2[num6 + num3], (float)this.m_rawHeights2[num6 + num4], num7);
		return Mathf.Lerp(num9, num10, num8);
	}

	public float SampleFinalHeight(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num, 0, 1080);
		int num4 = Mathf.Clamp(num + 1, 0, 1080);
		int num5 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num6 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		float num7 = x - (float)num;
		float num8 = z - (float)num2;
		float num9 = Mathf.Lerp((float)this.m_finalHeights[num5 + num3], (float)this.m_finalHeights[num5 + num4], num7);
		float num10 = Mathf.Lerp((float)this.m_finalHeights[num6 + num3], (float)this.m_finalHeights[num6 + num4], num7);
		return Mathf.Lerp(num9, num10, num8);
	}

	public float SampleBlockHeight(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num, 0, 1080);
		int num4 = Mathf.Clamp(num + 1, 0, 1080);
		int num5 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num6 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		float num7 = x - (float)num;
		float num8 = z - (float)num2;
		float num9 = Mathf.Lerp((float)this.m_blockHeights2[num5 + num3], (float)this.m_blockHeights2[num5 + num4], num7);
		float num10 = Mathf.Lerp((float)this.m_blockHeights2[num6 + num3], (float)this.m_blockHeights2[num6 + num4], num7);
		return Mathf.Lerp(num9, num10, num8);
	}

	public TerrainManager.SurfaceCell SampleRawSurface(float x, float z)
	{
		int num = Mathf.Clamp(Mathf.RoundToInt(x * 2048f), 0, 2211839);
		int num2 = Mathf.Clamp(Mathf.RoundToInt(z * 2048f), 0, 2211839);
		int num3 = num & 2047;
		int num4 = num2 & 2047;
		num >>= 11;
		num2 >>= 11;
		int num5 = Mathf.Min(num + 1, 1079);
		int num6 = Mathf.Min(num2 + 1, 1079) * 1080;
		TerrainManager.SurfaceCell surfaceCell = this.m_rawSurface[num2 + num];
		TerrainManager.SurfaceCell surfaceCell2 = this.m_rawSurface[num6 + num];
		TerrainManager.SurfaceCell surfaceCell3 = this.m_rawSurface[num2 + num5];
		TerrainManager.SurfaceCell surfaceCell4 = this.m_rawSurface[num6 + num5];
		int num7 = ((int)surfaceCell.m_clipped << 11) + (int)(surfaceCell3.m_clipped - surfaceCell.m_clipped) * num3;
		int num8 = ((int)surfaceCell.m_ruined << 11) + (int)(surfaceCell3.m_ruined - surfaceCell.m_ruined) * num3;
		int num9 = ((int)surfaceCell.m_gravel << 11) + (int)(surfaceCell3.m_gravel - surfaceCell.m_gravel) * num3;
		int num10 = ((int)surfaceCell.m_field << 11) + (int)(surfaceCell3.m_field - surfaceCell.m_field) * num3;
		int num11 = ((int)surfaceCell.m_pavementA << 11) + (int)(surfaceCell3.m_pavementA - surfaceCell.m_pavementA) * num3;
		int num12 = ((int)surfaceCell.m_pavementB << 11) + (int)(surfaceCell3.m_pavementB - surfaceCell.m_pavementB) * num3;
		int num13 = ((int)surfaceCell2.m_clipped << 11) + (int)(surfaceCell4.m_clipped - surfaceCell2.m_clipped) * num3;
		int num14 = ((int)surfaceCell2.m_ruined << 11) + (int)(surfaceCell4.m_ruined - surfaceCell2.m_ruined) * num3;
		int num15 = ((int)surfaceCell2.m_gravel << 11) + (int)(surfaceCell4.m_gravel - surfaceCell2.m_gravel) * num3;
		int num16 = ((int)surfaceCell2.m_field << 11) + (int)(surfaceCell4.m_field - surfaceCell2.m_field) * num3;
		int num17 = ((int)surfaceCell2.m_pavementA << 11) + (int)(surfaceCell4.m_pavementA - surfaceCell2.m_pavementA) * num3;
		int num18 = ((int)surfaceCell2.m_pavementB << 11) + (int)(surfaceCell4.m_pavementB - surfaceCell2.m_pavementB) * num3;
		TerrainManager.SurfaceCell result;
		result.m_clipped = (byte)((num7 << 11) + (num13 - num7) * num4 >> 22);
		result.m_pavementA = (byte)((num11 << 11) + (num17 - num11) * num4 >> 22);
		result.m_pavementB = (byte)((num12 << 11) + (num18 - num12) * num4 >> 22);
		result.m_ruined = (byte)((num8 << 11) + (num14 - num8) * num4 >> 22);
		result.m_gravel = (byte)((num9 << 11) + (num15 - num9) * num4 >> 22);
		result.m_field = (byte)((num10 << 11) + (num16 - num10) * num4 >> 22);
		return result;
	}

	public float SampleRawHeightSmooth(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num - 1, 0, 1080);
		int num4 = Mathf.Clamp(num, 0, 1080);
		int num5 = Mathf.Clamp(num + 1, 0, 1080);
		int num6 = Mathf.Clamp(num + 2, 0, 1080);
		int num7 = Mathf.Clamp(num2 - 1, 0, 1080) * 1081;
		int num8 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num9 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		int num10 = Mathf.Clamp(num2 + 2, 0, 1080) * 1081;
		float t = x - (float)num;
		float t2 = z - (float)num2;
		float h = TerrainManager.SmoothSample((float)this.m_rawHeights2[num7 + num3], (float)this.m_rawHeights2[num7 + num4], (float)this.m_rawHeights2[num7 + num5], (float)this.m_rawHeights2[num7 + num6], t);
		float h2 = TerrainManager.SmoothSample((float)this.m_rawHeights2[num8 + num3], (float)this.m_rawHeights2[num8 + num4], (float)this.m_rawHeights2[num8 + num5], (float)this.m_rawHeights2[num8 + num6], t);
		float h3 = TerrainManager.SmoothSample((float)this.m_rawHeights2[num9 + num3], (float)this.m_rawHeights2[num9 + num4], (float)this.m_rawHeights2[num9 + num5], (float)this.m_rawHeights2[num9 + num6], t);
		float h4 = TerrainManager.SmoothSample((float)this.m_rawHeights2[num10 + num3], (float)this.m_rawHeights2[num10 + num4], (float)this.m_rawHeights2[num10 + num5], (float)this.m_rawHeights2[num10 + num6], t);
		return TerrainManager.SmoothSample(h, h2, h3, h4, t2);
	}

	public float SampleOriginalRawHeightSmooth(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num - 1, 0, 1080);
		int num4 = Mathf.Clamp(num, 0, 1080);
		int num5 = Mathf.Clamp(num + 1, 0, 1080);
		int num6 = Mathf.Clamp(num + 2, 0, 1080);
		int num7 = Mathf.Clamp(num2 - 1, 0, 1080) * 1081;
		int num8 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num9 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		int num10 = Mathf.Clamp(num2 + 2, 0, 1080) * 1081;
		float t = x - (float)num;
		float t2 = z - (float)num2;
		float h = TerrainManager.SmoothSample((float)this.m_rawHeights[num7 + num3], (float)this.m_rawHeights[num7 + num4], (float)this.m_rawHeights[num7 + num5], (float)this.m_rawHeights[num7 + num6], t);
		float h2 = TerrainManager.SmoothSample((float)this.m_rawHeights[num8 + num3], (float)this.m_rawHeights[num8 + num4], (float)this.m_rawHeights[num8 + num5], (float)this.m_rawHeights[num8 + num6], t);
		float h3 = TerrainManager.SmoothSample((float)this.m_rawHeights[num9 + num3], (float)this.m_rawHeights[num9 + num4], (float)this.m_rawHeights[num9 + num5], (float)this.m_rawHeights[num9 + num6], t);
		float h4 = TerrainManager.SmoothSample((float)this.m_rawHeights[num10 + num3], (float)this.m_rawHeights[num10 + num4], (float)this.m_rawHeights[num10 + num5], (float)this.m_rawHeights[num10 + num6], t);
		return TerrainManager.SmoothSample(h, h2, h3, h4, t2);
	}

	public float SampleFinalHeightSmooth(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num - 1, 0, 1080);
		int num4 = Mathf.Clamp(num, 0, 1080);
		int num5 = Mathf.Clamp(num + 1, 0, 1080);
		int num6 = Mathf.Clamp(num + 2, 0, 1080);
		int num7 = Mathf.Clamp(num2 - 1, 0, 1080) * 1081;
		int num8 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num9 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		int num10 = Mathf.Clamp(num2 + 2, 0, 1080) * 1081;
		float t = x - (float)num;
		float t2 = z - (float)num2;
		float h = TerrainManager.SmoothSample((float)this.m_finalHeights[num7 + num3], (float)this.m_finalHeights[num7 + num4], (float)this.m_finalHeights[num7 + num5], (float)this.m_finalHeights[num7 + num6], t);
		float h2 = TerrainManager.SmoothSample((float)this.m_finalHeights[num8 + num3], (float)this.m_finalHeights[num8 + num4], (float)this.m_finalHeights[num8 + num5], (float)this.m_finalHeights[num8 + num6], t);
		float h3 = TerrainManager.SmoothSample((float)this.m_finalHeights[num9 + num3], (float)this.m_finalHeights[num9 + num4], (float)this.m_finalHeights[num9 + num5], (float)this.m_finalHeights[num9 + num6], t);
		float h4 = TerrainManager.SmoothSample((float)this.m_finalHeights[num10 + num3], (float)this.m_finalHeights[num10 + num4], (float)this.m_finalHeights[num10 + num5], (float)this.m_finalHeights[num10 + num6], t);
		return TerrainManager.SmoothSample(h, h2, h3, h4, t2);
	}

	public float SampleBlockHeightSmooth(float x, float z)
	{
		int num = (int)x;
		int num2 = (int)z;
		int num3 = Mathf.Clamp(num - 1, 0, 1080);
		int num4 = Mathf.Clamp(num, 0, 1080);
		int num5 = Mathf.Clamp(num + 1, 0, 1080);
		int num6 = Mathf.Clamp(num + 2, 0, 1080);
		int num7 = Mathf.Clamp(num2 - 1, 0, 1080) * 1081;
		int num8 = Mathf.Clamp(num2, 0, 1080) * 1081;
		int num9 = Mathf.Clamp(num2 + 1, 0, 1080) * 1081;
		int num10 = Mathf.Clamp(num2 + 2, 0, 1080) * 1081;
		float t = x - (float)num;
		float t2 = z - (float)num2;
		float h = TerrainManager.SmoothSample((float)this.m_blockHeights2[num7 + num3], (float)this.m_blockHeights2[num7 + num4], (float)this.m_blockHeights2[num7 + num5], (float)this.m_blockHeights2[num7 + num6], t);
		float h2 = TerrainManager.SmoothSample((float)this.m_blockHeights2[num8 + num3], (float)this.m_blockHeights2[num8 + num4], (float)this.m_blockHeights2[num8 + num5], (float)this.m_blockHeights2[num8 + num6], t);
		float h3 = TerrainManager.SmoothSample((float)this.m_blockHeights2[num9 + num3], (float)this.m_blockHeights2[num9 + num4], (float)this.m_blockHeights2[num9 + num5], (float)this.m_blockHeights2[num9 + num6], t);
		float h4 = TerrainManager.SmoothSample((float)this.m_blockHeights2[num10 + num3], (float)this.m_blockHeights2[num10 + num4], (float)this.m_blockHeights2[num10 + num5], (float)this.m_blockHeights2[num10 + num6], t);
		return TerrainManager.SmoothSample(h, h2, h3, h4, t2);
	}

	public float SampleRawHeightSmoothWithWater(float x, float z, bool timeLerp, float waterOffset)
	{
		int num = (int)x;
		int num2 = (int)z;
		int x2 = Mathf.Clamp(num - 1, 0, 1080);
		int x3 = Mathf.Clamp(num, 0, 1080);
		int x4 = Mathf.Clamp(num + 1, 0, 1080);
		int x5 = Mathf.Clamp(num + 2, 0, 1080);
		int z2 = Mathf.Clamp(num2 - 1, 0, 1080);
		int z3 = Mathf.Clamp(num2, 0, 1080);
		int z4 = Mathf.Clamp(num2 + 1, 0, 1080);
		int z5 = Mathf.Clamp(num2 + 2, 0, 1080);
		float t = x - (float)num;
		float t2 = z - (float)num2;
		if (timeLerp)
		{
			uint num3 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
			float time = ((num3 & 63u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.015625f;
			uint heightFrame = num3 - 64u >> 6 & 1u;
			float h = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z2, heightFrame, time, waterOffset), this.GetSurfaceHeight(x3, z2, heightFrame, time, waterOffset), this.GetSurfaceHeight(x4, z2, heightFrame, time, waterOffset), this.GetSurfaceHeight(x5, z2, heightFrame, time, waterOffset), t);
			float h2 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z3, heightFrame, time, waterOffset), this.GetSurfaceHeight(x3, z3, heightFrame, time, waterOffset), this.GetSurfaceHeight(x4, z3, heightFrame, time, waterOffset), this.GetSurfaceHeight(x5, z3, heightFrame, time, waterOffset), t);
			float h3 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z4, heightFrame, time, waterOffset), this.GetSurfaceHeight(x3, z4, heightFrame, time, waterOffset), this.GetSurfaceHeight(x4, z4, heightFrame, time, waterOffset), this.GetSurfaceHeight(x5, z4, heightFrame, time, waterOffset), t);
			float h4 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z5, heightFrame, time, waterOffset), this.GetSurfaceHeight(x3, z5, heightFrame, time, waterOffset), this.GetSurfaceHeight(x4, z5, heightFrame, time, waterOffset), this.GetSurfaceHeight(x5, z5, heightFrame, time, waterOffset), t);
			return TerrainManager.SmoothSample(h, h2, h3, h4, t2);
		}
		WaterSimulation.Cell[] cells = this.m_waterSimulation.BeginRead();
		float result;
		try
		{
			float h5 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z2, cells, waterOffset), this.GetSurfaceHeight(x3, z2, cells, waterOffset), this.GetSurfaceHeight(x4, z2, cells, waterOffset), this.GetSurfaceHeight(x5, z2, cells, waterOffset), t);
			float h6 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z3, cells, waterOffset), this.GetSurfaceHeight(x3, z3, cells, waterOffset), this.GetSurfaceHeight(x4, z3, cells, waterOffset), this.GetSurfaceHeight(x5, z3, cells, waterOffset), t);
			float h7 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z4, cells, waterOffset), this.GetSurfaceHeight(x3, z4, cells, waterOffset), this.GetSurfaceHeight(x4, z4, cells, waterOffset), this.GetSurfaceHeight(x5, z4, cells, waterOffset), t);
			float h8 = TerrainManager.SmoothSample(this.GetSurfaceHeight(x2, z5, cells, waterOffset), this.GetSurfaceHeight(x3, z5, cells, waterOffset), this.GetSurfaceHeight(x4, z5, cells, waterOffset), this.GetSurfaceHeight(x5, z5, cells, waterOffset), t);
			result = TerrainManager.SmoothSample(h5, h6, h7, h8, t2);
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		return result;
	}

	public float SampleBlockHeightSmoothWithWater(float x, float z, bool timeLerp, float waterOffset, out bool hasWater)
	{
		int num = (int)x;
		int num2 = (int)z;
		int x2 = Mathf.Clamp(num - 1, 0, 1080);
		int x3 = Mathf.Clamp(num, 0, 1080);
		int x4 = Mathf.Clamp(num + 1, 0, 1080);
		int x5 = Mathf.Clamp(num + 2, 0, 1080);
		int z2 = Mathf.Clamp(num2 - 1, 0, 1080);
		int z3 = Mathf.Clamp(num2, 0, 1080);
		int z4 = Mathf.Clamp(num2 + 1, 0, 1080);
		int z5 = Mathf.Clamp(num2 + 2, 0, 1080);
		float t = x - (float)num;
		float t2 = z - (float)num2;
		hasWater = false;
		bool flag = false;
		if (timeLerp)
		{
			uint num3 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
			float time = ((num3 & 63u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.015625f;
			uint heightFrame = num3 - 64u >> 6 & 1u;
			float h = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z2, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z2, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x4, z2, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x5, z2, heightFrame, time, waterOffset, ref flag), t);
			float h2 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z3, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z3, heightFrame, time, waterOffset, ref hasWater), this.GetSurfaceHeight2(x4, z3, heightFrame, time, waterOffset, ref hasWater), this.GetSurfaceHeight2(x5, z3, heightFrame, time, waterOffset, ref flag), t);
			float h3 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z4, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z4, heightFrame, time, waterOffset, ref hasWater), this.GetSurfaceHeight2(x4, z4, heightFrame, time, waterOffset, ref hasWater), this.GetSurfaceHeight2(x5, z4, heightFrame, time, waterOffset, ref flag), t);
			float h4 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z5, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z5, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x4, z5, heightFrame, time, waterOffset, ref flag), this.GetSurfaceHeight2(x5, z5, heightFrame, time, waterOffset, ref flag), t);
			return TerrainManager.SmoothSample(h, h2, h3, h4, t2);
		}
		WaterSimulation.Cell[] cells = this.m_waterSimulation.BeginRead();
		float result;
		try
		{
			float h5 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z2, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z2, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x4, z2, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x5, z2, cells, waterOffset, ref flag), t);
			float h6 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z3, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z3, cells, waterOffset, ref hasWater), this.GetSurfaceHeight2(x4, z3, cells, waterOffset, ref hasWater), this.GetSurfaceHeight2(x5, z3, cells, waterOffset, ref flag), t);
			float h7 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z4, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z4, cells, waterOffset, ref hasWater), this.GetSurfaceHeight2(x4, z4, cells, waterOffset, ref hasWater), this.GetSurfaceHeight2(x5, z4, cells, waterOffset, ref flag), t);
			float h8 = TerrainManager.SmoothSample(this.GetSurfaceHeight2(x2, z5, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x3, z5, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x4, z5, cells, waterOffset, ref flag), this.GetSurfaceHeight2(x5, z5, cells, waterOffset, ref flag), t);
			result = TerrainManager.SmoothSample(h5, h6, h7, h8, t2);
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		return result;
	}

	public float SampleRawHeightWithWater(float x, float z, bool timeLerp, float waterOffset)
	{
		int num = (int)x;
		int num2 = (int)z;
		int x2 = Mathf.Clamp(num, 0, 1080);
		int x3 = Mathf.Clamp(num + 1, 0, 1080);
		int z2 = Mathf.Clamp(num2, 0, 1080);
		int z3 = Mathf.Clamp(num2 + 1, 0, 1080);
		float num3 = x - (float)num;
		float num4 = z - (float)num2;
		if (timeLerp)
		{
			uint num5 = Singleton<SimulationManager>.get_instance().m_referenceFrameIndex + 32u;
			float time = ((num5 & 63u) + Singleton<SimulationManager>.get_instance().m_referenceTimer) * 0.015625f;
			uint heightFrame = num5 - 64u >> 6 & 1u;
			float num6 = Mathf.Lerp(this.GetSurfaceHeight(x2, z2, heightFrame, time, waterOffset), this.GetSurfaceHeight(x3, z2, heightFrame, time, waterOffset), num3);
			float num7 = Mathf.Lerp(this.GetSurfaceHeight(x2, z3, heightFrame, time, waterOffset), this.GetSurfaceHeight(x3, z3, heightFrame, time, waterOffset), num3);
			return Mathf.Lerp(num6, num7, num4);
		}
		WaterSimulation.Cell[] cells = this.m_waterSimulation.BeginRead();
		float result;
		try
		{
			float num8 = Mathf.Lerp(this.GetSurfaceHeight(x2, z2, cells, waterOffset), this.GetSurfaceHeight(x3, z2, cells, waterOffset), num3);
			float num9 = Mathf.Lerp(this.GetSurfaceHeight(x2, z3, cells, waterOffset), this.GetSurfaceHeight(x3, z3, cells, waterOffset), num3);
			result = Mathf.Lerp(num8, num9, num4);
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		return result;
	}

	public float GetSurfaceHeight(int x, int z, uint heightFrame, float time, float waterOffset)
	{
		float num = (float)this.m_rawHeights2[z * 1081 + x];
		if (!this.m_waterSimulation.WaterExists(x, z))
		{
			return num;
		}
		int num2 = 120;
		int num3 = Mathf.Min(x / num2, 8);
		int num4 = Mathf.Min(z / num2, 8);
		int num5 = num4 * 9 + num3;
		int num6 = 64 - num2 * num3 - (num2 >> 1);
		int num7 = 64 - num2 * num4 - (num2 >> 1);
		Texture2D waterHeight = this.m_patches[num5].m_waterHeight[(int)((UIntPtr)heightFrame)].m_waterHeight;
		Color pixel = waterHeight.GetPixel(x + num6, z + num7);
		float num8 = pixel.r * 65280f + pixel.g * 255f;
		float num9 = pixel.b * 65280f + pixel.a * 255f;
		float num10 = num8 + (num9 - num8) * time - num;
		if (num10 < 64f)
		{
			return num + Mathf.Max(0f, num10);
		}
		return num + num10 + waterOffset * 64f;
	}

	public float GetSurfaceHeight2(int x, int z, uint heightFrame, float time, float waterOffset, ref bool hasWater)
	{
		float num = (float)this.m_blockHeights[z * 1081 + x];
		if (!this.m_waterSimulation.WaterExists(x, z))
		{
			return num;
		}
		int num2 = 120;
		int num3 = Mathf.Min(x / num2, 8);
		int num4 = Mathf.Min(z / num2, 8);
		int num5 = num4 * 9 + num3;
		int num6 = 64 - num2 * num3 - (num2 >> 1);
		int num7 = 64 - num2 * num4 - (num2 >> 1);
		Texture2D waterHeight = this.m_patches[num5].m_waterHeight[(int)((UIntPtr)heightFrame)].m_waterHeight;
		Color pixel = waterHeight.GetPixel(x + num6, z + num7);
		float num8 = pixel.r * 65280f + pixel.g * 255f;
		float num9 = pixel.b * 65280f + pixel.a * 255f;
		float num10 = num8 + (num9 - num8) * time - num;
		if (num10 < 64f)
		{
			return num + Mathf.Max(0f, num10);
		}
		hasWater = true;
		return num + num10 + waterOffset * 64f;
	}

	public float GetSurfaceHeight(int x, int z, WaterSimulation.Cell[] cells, float waterOffset)
	{
		float num = (float)cells[z * 1081 + x].m_height;
		if (num < 64f)
		{
			float num2 = (float)this.m_rawHeights2[z * 1081 + x];
			return num2 + Mathf.Max(0f, num);
		}
		float num3 = (float)this.m_blockHeights[z * 1081 + x];
		return num3 + num + waterOffset * 64f;
	}

	public float GetSurfaceHeight2(int x, int z, WaterSimulation.Cell[] cells, float waterOffset, ref bool hasWater)
	{
		float num = (float)cells[z * 1081 + x].m_height;
		if (num < 64f)
		{
			float num2 = (float)this.m_blockHeights[z * 1081 + x];
			return num2 + Mathf.Max(0f, num);
		}
		hasWater = true;
		float num3 = (float)this.m_blockHeights[z * 1081 + x];
		return num3 + num + waterOffset * 64f;
	}

	public void HeightMap_sampleWaterHeightAndNormal(Vector3 worldPos, float waveMultiplier, out float h, out Vector3 normal)
	{
		float num = 17280f;
		float num2 = 1920f;
		int num3 = Mathf.Clamp((int)((worldPos.x + num * 0.5f) / num2), 0, 8);
		int num4 = Mathf.Clamp((int)((worldPos.z + num * 0.5f) / num2), 0, 8);
		TerrainPatch terrainPatch = this.m_patches[num4 * 9 + num3];
		Texture2D heightMap = terrainPatch.m_heightMap;
		Vector4 vector = (terrainPatch.m_rndDetailIndex == 0) ? terrainPatch.m_heightMappingRaw : terrainPatch.m_heightMappingDetail;
		Vector4 vector2 = (terrainPatch.m_rndDetailIndex == 0) ? terrainPatch.m_surfaceMappingRaw : terrainPatch.m_surfaceMappingDetail;
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		uint num5 = instance.m_referenceFrameIndex + 32u;
		float num6 = ((num5 & 63u) + instance.m_referenceTimer) * 0.015625f;
		uint num7 = num5 - 64u >> 6 & 1u;
		Texture2D waterHeight = terrainPatch.m_waterHeight[(int)((UIntPtr)num7)].m_waterHeight;
		Vector4 heightMappingRaw = terrainPatch.m_heightMappingRaw;
		float num8 = worldPos.x * vector2.z + vector2.x;
		float num9 = worldPos.z * vector2.z + vector2.y;
		float num10 = num8 / heightMappingRaw.x;
		float num11 = num9 / heightMappingRaw.z;
		int num12 = Mathf.Clamp(Mathf.FloorToInt(num10), 0, waterHeight.get_width() - 2);
		int num13 = Mathf.Clamp(Mathf.FloorToInt(num11), 0, waterHeight.get_height() - 2);
		float num14 = Mathf.Clamp01(num10 - (float)num12);
		float num15 = Mathf.Clamp01(num11 - (float)num13);
		Color pixel = waterHeight.GetPixel(num12, num13);
		Color pixel2 = waterHeight.GetPixel(num12 + 1, num13);
		Color pixel3 = waterHeight.GetPixel(num12, num13 + 1);
		Color pixel4 = waterHeight.GetPixel(num12 + 1, num13 + 1);
		float num16 = pixel.r * 0.99609375f + pixel.g * 0.00389099121f;
		float num17 = pixel.b * 0.99609375f + pixel.a * 0.00389099121f;
		float num18 = pixel2.r * 0.99609375f + pixel2.g * 0.00389099121f;
		float num19 = pixel2.b * 0.99609375f + pixel2.a * 0.00389099121f;
		float num20 = pixel3.r * 0.99609375f + pixel3.g * 0.00389099121f;
		float num21 = pixel3.b * 0.99609375f + pixel3.a * 0.00389099121f;
		float num22 = pixel4.r * 0.99609375f + pixel4.g * 0.00389099121f;
		float num23 = pixel4.b * 0.99609375f + pixel4.a * 0.00389099121f;
		float num24 = Mathf.Lerp(Mathf.Lerp(num16, num18, num14), Mathf.Lerp(num20, num22, num14), num15) * heightMappingRaw.y;
		float num25 = Mathf.Lerp(Mathf.Lerp(num17, num19, num14), Mathf.Lerp(num21, num23, num14), num15) * heightMappingRaw.y;
		h = Mathf.Lerp(num24, num25, num6);
		num10 = num8 / vector.x;
		num11 = num9 / vector.z;
		num12 = Mathf.Clamp(Mathf.FloorToInt(num10), 0, heightMap.get_width() - 2);
		num13 = Mathf.Clamp(Mathf.FloorToInt(num11), 0, heightMap.get_height() - 2);
		num14 = Mathf.Clamp01(num10 - (float)num12);
		num15 = Mathf.Clamp01(num11 - (float)num13);
		pixel = heightMap.GetPixel(num12, num13);
		pixel2 = heightMap.GetPixel(num12 + 1, num13);
		pixel3 = heightMap.GetPixel(num12, num13 + 1);
		pixel4 = heightMap.GetPixel(num12 + 1, num13 + 1);
		float num26 = pixel.r * 0.99609375f + pixel.g * 0.00389099121f + pixel.b * 1.51991844E-05f;
		float num27 = pixel2.r * 0.99609375f + pixel2.g * 0.00389099121f + pixel2.b * 1.51991844E-05f;
		float num28 = pixel3.r * 0.99609375f + pixel3.g * 0.00389099121f + pixel3.b * 1.51991844E-05f;
		float num29 = pixel4.r * 0.99609375f + pixel4.g * 0.00389099121f + pixel4.b * 1.51991844E-05f;
		float num30 = Mathf.Lerp(Mathf.Lerp(num26, num27, num14), Mathf.Lerp(num28, num29, num14), num15) * vector.y;
		float num31 = instance.m_simulationTimer * 0.104719758f;
		Vector4 vector3;
		vector3.x = num31 * 15f + worldPos.x * 0.07f;
		vector3.y = num31 * 27f + worldPos.z * 0.07f;
		vector3.z = num31 * 7f - worldPos.x * 0.11f;
		vector3.w = num31 * 12f - worldPos.z * 0.11f;
		float num32 = MathUtils.SmoothStep(0.8f, 16f, h - num30);
		Vector4 vector4;
		vector4.x = 0.19f * num32;
		vector4.y = 0.21f * num32;
		vector4.z = 0.25f * num32;
		vector4.w = 0.23f * num32;
		Vector4 vector5;
		vector5.x = Mathf.Cos(vector3.x) * 0.5f;
		vector5.y = Mathf.Cos(vector3.y) * 0.5f;
		vector5.z = Mathf.Cos(vector3.z) * 0.5f;
		vector5.w = Mathf.Cos(vector3.w) * 0.5f;
		Vector3 vector6;
		vector6.x = vector5.x * vector4.x + vector5.z * vector4.z;
		vector6.y = Mathf.Sin(vector3.x) * vector4.x + Mathf.Sin(vector3.y) * vector4.y + Mathf.Sin(vector3.z) * vector4.z + Mathf.Sin(vector3.w) * vector4.w;
		vector6.z = vector5.y * vector4.y + vector5.w * vector4.w;
		h = Mathf.Max(h + vector6.y, num30);
		normal.x = -vector6.x * waveMultiplier;
		normal.z = vector6.z * waveMultiplier;
		normal.y = Mathf.Sqrt(Mathf.Max(0f, 1f - normal.x * normal.x - normal.z * normal.z));
	}

	public bool HasWater(Vector2 position)
	{
		int num = Mathf.FloorToInt((position.x + 8640f) * 16f);
		int num2 = Mathf.FloorToInt((position.y + 8640f) * 16f);
		int num3 = Mathf.Clamp(num >> 8, 0, 1080);
		int num4 = Mathf.Clamp((num >> 8) + 1, 0, 1080);
		int num5 = Mathf.Clamp(num2 >> 8, 0, 1080);
		int num6 = Mathf.Clamp((num2 >> 8) + 1, 0, 1080);
		int num7 = num5 * 1081 + num3;
		int num8 = num6 * 1081 + num3;
		int num9 = num5 * 1081 + num4;
		int num10 = num6 * 1081 + num4;
		int num11 = 1000000;
		int num12 = 0;
		int num13 = 0;
		int num14 = 0;
		int num15 = 0;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			int height = (int)array[num7].m_height;
			if (height != 0)
			{
				num12 = (int)this.m_blockHeights[num7] + height;
				if (num12 < num11)
				{
					num11 = num12;
				}
			}
			height = (int)array[num8].m_height;
			if (height != 0)
			{
				num13 = (int)this.m_blockHeights[num8] + height;
				if (num13 < num11)
				{
					num11 = num13;
				}
			}
			height = (int)array[num9].m_height;
			if (height != 0)
			{
				num14 = (int)this.m_blockHeights[num9] + height;
				if (num14 < num11)
				{
					num11 = num14;
				}
			}
			height = (int)array[num10].m_height;
			if (height != 0)
			{
				num15 = (int)this.m_blockHeights[num10] + height;
				if (num15 < num11)
				{
					num11 = num15;
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num11 == 1000000)
		{
			return false;
		}
		if (num12 == 0)
		{
			num12 = num11;
		}
		if (num13 == 0)
		{
			num13 = num11;
		}
		if (num14 == 0)
		{
			num14 = num11;
		}
		if (num15 == 0)
		{
			num15 = num11;
		}
		int num16 = num12 + ((num13 - num12) * (num2 & 255) >> 8);
		int num17 = num14 + ((num15 - num14) * (num2 & 255) >> 8);
		int num18 = num16 + ((num17 - num16) * (num & 255) >> 8);
		int num19 = (int)this.m_rawHeights2[num7];
		int num20 = (int)this.m_rawHeights2[num8];
		int num21 = (int)this.m_rawHeights2[num9];
		int num22 = (int)this.m_rawHeights2[num10];
		int num23 = num19 + ((num20 - num19) * (num2 & 255) >> 8);
		int num24 = num21 + ((num22 - num21) * (num2 & 255) >> 8);
		int num25 = num23 + ((num24 - num23) * (num & 255) >> 8);
		return num18 - num25 >= 8;
	}

	public float WaterLevel(Vector2 position)
	{
		int num = Mathf.FloorToInt((position.x + 8640f) * 16f);
		int num2 = Mathf.FloorToInt((position.y + 8640f) * 16f);
		int num3 = Mathf.Clamp(num >> 8, 0, 1080);
		int num4 = Mathf.Clamp((num >> 8) + 1, 0, 1080);
		int num5 = Mathf.Clamp(num2 >> 8, 0, 1080);
		int num6 = Mathf.Clamp((num2 >> 8) + 1, 0, 1080);
		int num7 = num5 * 1081 + num3;
		int num8 = num6 * 1081 + num3;
		int num9 = num5 * 1081 + num4;
		int num10 = num6 * 1081 + num4;
		int num11 = 1000000;
		int num12 = 0;
		int num13 = 0;
		int num14 = 0;
		int num15 = 0;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			int height = (int)array[num7].m_height;
			if (height != 0)
			{
				num12 = (int)this.m_blockHeights[num7] + height;
				if (num12 < num11)
				{
					num11 = num12;
				}
			}
			height = (int)array[num8].m_height;
			if (height != 0)
			{
				num13 = (int)this.m_blockHeights[num8] + height;
				if (num13 < num11)
				{
					num11 = num13;
				}
			}
			height = (int)array[num9].m_height;
			if (height != 0)
			{
				num14 = (int)this.m_blockHeights[num9] + height;
				if (num14 < num11)
				{
					num11 = num14;
				}
			}
			height = (int)array[num10].m_height;
			if (height != 0)
			{
				num15 = (int)this.m_blockHeights[num10] + height;
				if (num15 < num11)
				{
					num11 = num15;
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num11 == 1000000)
		{
			return 0f;
		}
		if (num12 == 0)
		{
			num12 = num11;
		}
		if (num13 == 0)
		{
			num13 = num11;
		}
		if (num14 == 0)
		{
			num14 = num11;
		}
		if (num15 == 0)
		{
			num15 = num11;
		}
		int num16 = num12 + ((num13 - num12) * (num2 & 255) >> 8);
		int num17 = num14 + ((num15 - num14) * (num2 & 255) >> 8);
		int num18 = num16 + ((num17 - num16) * (num & 255) >> 8);
		int num19 = (int)this.m_rawHeights2[num7];
		int num20 = (int)this.m_rawHeights2[num8];
		int num21 = (int)this.m_rawHeights2[num9];
		int num22 = (int)this.m_rawHeights2[num10];
		int num23 = num19 + ((num20 - num19) * (num2 & 255) >> 8);
		int num24 = num21 + ((num22 - num21) * (num2 & 255) >> 8);
		int num25 = num23 + ((num24 - num23) * (num & 255) >> 8);
		if (num18 - num25 >= 8)
		{
			return (float)num18 * 0.015625f;
		}
		return 0f;
	}

	public bool SampleWaterData(Vector2 position, out float terrainHeight, out float waterHeight, out Vector3 velocity, out Vector3 normal)
	{
		int num = Mathf.FloorToInt((position.x + 8640f) * 16f);
		int num2 = Mathf.FloorToInt((position.y + 8640f) * 16f);
		int num3 = Mathf.Clamp(num >> 8, 0, 1080);
		int num4 = Mathf.Clamp((num >> 8) + 1, 0, 1080);
		int num5 = Mathf.Clamp(num2 >> 8, 0, 1080);
		int num6 = Mathf.Clamp((num2 >> 8) + 1, 0, 1080);
		int num7 = num5 * 1081 + num3;
		int num8 = num6 * 1081 + num3;
		int num9 = num5 * 1081 + num4;
		int num10 = num6 * 1081 + num4;
		int num11 = 1000000;
		int num12 = 0;
		int num13 = 0;
		int num14 = 0;
		int num15 = 0;
		int num16 = 0;
		int num17 = 0;
		int num18 = 0;
		int num19 = 0;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			int height = (int)array[num7].m_height;
			if (height != 0)
			{
				num16 = Mathf.Clamp((int)array[num7].m_velocityX * 65536 / height, -65536, 65536);
				num18 = Mathf.Clamp((int)array[num7].m_velocityZ * 65536 / height, -65536, 65536);
				num12 = (int)this.m_blockHeights[num7] + height;
				if (num12 < num11)
				{
					num11 = num12;
				}
			}
			height = (int)array[num8].m_height;
			if (height != 0)
			{
				num17 = Mathf.Clamp((int)array[num8].m_velocityX * 65536 / height, -65536, 65536);
				num13 = (int)this.m_blockHeights[num8] + height;
				if (num13 < num11)
				{
					num11 = num13;
				}
			}
			height = (int)array[num9].m_height;
			if (height != 0)
			{
				num19 = Mathf.Clamp((int)array[num9].m_velocityZ * 65536 / height, -65536, 65536);
				num14 = (int)this.m_blockHeights[num9] + height;
				if (num14 < num11)
				{
					num11 = num14;
				}
			}
			height = (int)array[num10].m_height;
			if (height != 0)
			{
				num15 = (int)this.m_blockHeights[num10] + height;
				if (num15 < num11)
				{
					num11 = num15;
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num12 == 0)
		{
			num12 = num11;
		}
		if (num13 == 0)
		{
			num13 = num11;
		}
		if (num14 == 0)
		{
			num14 = num11;
		}
		if (num15 == 0)
		{
			num15 = num11;
		}
		int num20 = num12 + ((num13 - num12) * (num2 & 255) >> 8);
		int num21 = num14 + ((num15 - num14) * (num2 & 255) >> 8);
		int num22 = num20 + ((num21 - num20) * (num & 255) >> 8);
		int num23 = (int)this.m_blockHeights[num7];
		int num24 = (int)this.m_blockHeights[num8];
		int num25 = (int)this.m_blockHeights[num9];
		int num26 = (int)this.m_blockHeights[num10];
		int num27 = num23 + ((num24 - num23) * (num2 & 255) >> 8);
		int num28 = num25 + ((num26 - num25) * (num2 & 255) >> 8);
		int num29 = num27 + ((num28 - num27) * (num & 255) >> 8);
		if (num11 != 1000000 && num22 - num29 >= 8)
		{
			terrainHeight = (float)num29 * 0.015625f;
			waterHeight = (float)num22 * 0.015625f;
			normal.y = 1f;
			normal.x = (float)(num20 - num21) * 0.0009765625f;
			num20 = num12 + ((num14 - num12) * (num & 255) >> 8);
			num21 = num13 + ((num15 - num13) * (num & 255) >> 8);
			normal.z = (float)(num20 - num21) * 0.0009765625f;
			normal.Normalize();
			int num30 = num16 + ((num17 - num16) * (num2 & 255) >> 8);
			int num31 = num18 + ((num19 - num18) * (num & 255) >> 8);
			velocity.x = (float)num30 * 0.000228881836f;
			velocity.z = (float)num31 * 0.000228881836f;
			velocity.y = 0f;
			velocity -= Vector3.Dot(velocity, normal) * normal;
			return true;
		}
		terrainHeight = (float)num29 * 0.015625f;
		waterHeight = 0f;
		normal.y = 1f;
		normal.x = (float)(num27 - num28) * 0.0009765625f;
		num27 = num23 + ((num25 - num23) * (num & 255) >> 8);
		num28 = num24 + ((num26 - num24) * (num & 255) >> 8);
		normal.z = (float)(num27 - num28) * 0.0009765625f;
		normal.Normalize();
		velocity = Vector3.get_zero();
		return false;
	}

	public bool HasWater(Segment2 segment, float radius, bool any)
	{
		int num = Mathf.Max(1, Mathf.RoundToInt(segment.Length() * 0.0078125f));
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			for (int i = 0; i < num; i++)
			{
				Segment2 segment2 = segment.Cut((float)i / (float)num, (float)(i + 1) / (float)num);
				Vector2 vector = segment2.Min();
				Vector2 vector2 = segment2.Max();
				int num2 = Mathf.Max((int)((vector.x - radius + 8640f) / 16f + 0.5f), 0);
				int num3 = Mathf.Max((int)((vector.y - radius + 8640f) / 16f + 0.5f), 0);
				int num4 = Mathf.Min((int)((vector2.x + radius + 8640f) / 16f + 0.5f), 1080);
				int num5 = Mathf.Min((int)((vector2.y + radius + 8640f) / 16f + 0.5f), 1080);
				for (int j = num3; j <= num5; j++)
				{
					Vector2 vector3;
					vector3.y = (float)j * 16f - 8640f;
					for (int k = num2; k <= num4; k++)
					{
						vector3.x = (float)k * 16f - 8640f;
						int height = (int)array[j * 1081 + k].m_height;
						float num7;
						if (any)
						{
							float num6;
							if (height >= 8 && segment2.DistanceSqr(vector3, ref num6) < radius * radius)
							{
								bool result = true;
								return result;
							}
						}
						else if (height < 8 && segment2.DistanceSqr(vector3, ref num7) < radius * radius)
						{
							bool result = false;
							return result;
						}
					}
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		return !any;
	}

	public void CountWaterCoverage(Vector3 position, float maxDistance, out int water, out int shore, out int pollution)
	{
		int num = Mathf.Max((int)((position.x - maxDistance + 8640f) / 16f + 0.5f), 0);
		int num2 = Mathf.Max((int)((position.z - maxDistance + 8640f) / 16f + 0.5f), 0);
		int num3 = Mathf.Min((int)((position.x + maxDistance + 8640f) / 16f + 0.5f), 1080);
		int num4 = Mathf.Min((int)((position.z + maxDistance + 8640f) / 16f + 0.5f), 1080);
		water = 0;
		shore = 0;
		pollution = 0;
		int num5 = (num3 - num + 1) * (num4 - num2 + 1);
		if (num5 == 0)
		{
			return;
		}
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			for (int i = num2; i <= num4; i++)
			{
				for (int j = num; j <= num3; j++)
				{
					int num9 = i * 1081 + j;
					WaterSimulation.Cell cell = array[num9];
					int height = (int)cell.m_height;
					ushort num10 = this.m_finalHeights[num9];
					num8 += (int)num10;
					if (height >= 8)
					{
						ushort num11 = this.m_blockHeights[num9];
						num7 += (int)(num11 + 256) + Mathf.Min(255, height);
						water += Mathf.Min(255, height);
						pollution += Mathf.Min(255, (int)cell.m_pollution);
						num6++;
					}
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num6 != 0)
		{
			shore = Mathf.Clamp((num7 + (num6 >> 1)) / num6 - (num8 + (num5 >> 1)) / num5, 0, 255);
			water = (water + (num6 >> 1)) / num6;
			pollution = (pollution + (num6 >> 1)) / num6;
		}
	}

	public bool GetClosestWaterPos(ref Vector3 position, float maxDistance)
	{
		int num = Mathf.Max((int)((position.x - maxDistance + 8640f) / 16f + 0.5f), 0);
		int num2 = Mathf.Max((int)((position.z - maxDistance + 8640f) / 16f + 0.5f), 0);
		int num3 = Mathf.Min((int)((position.x + maxDistance + 8640f) / 16f + 0.5f), 1080);
		int num4 = Mathf.Min((int)((position.z + maxDistance + 8640f) / 16f + 0.5f), 1080);
		bool flag = false;
		Vector3 vector = position;
		float num5 = maxDistance * maxDistance / 8f;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			for (int i = num2; i <= num4; i++)
			{
				float num6 = (float)i * 16f - 8640f - position.z;
				for (int j = num; j <= num3; j++)
				{
					int num7 = i * 1081 + j;
					int height = (int)array[num7].m_height;
					if (height >= 8)
					{
						float num8 = (float)j * 16f - 8640f - position.x;
						float num9 = (num6 * num6 + num8 * num8) / (float)height;
						if (num9 < num5)
						{
							int num10 = (int)this.m_blockHeights[num7] + height;
							if ((float)num10 * 0.015625f >= position.y)
							{
								flag = true;
								vector = position + new Vector3(num8, 0f, num6);
								num5 = num9;
							}
						}
					}
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (flag)
		{
			position = vector;
		}
		return flag;
	}

	public bool GetShorePos(Vector3 refPos, float maxDistance, out Vector3 position, out Vector3 direction, out float waterHeight)
	{
		int num = Mathf.Max((int)((refPos.x - maxDistance + 8640f) / 16f + 0.5f), 0);
		int num2 = Mathf.Max((int)((refPos.z - maxDistance + 8640f) / 16f + 0.5f), 0);
		int num3 = Mathf.Min((int)((refPos.x + maxDistance + 8640f) / 16f + 0.5f), 1080);
		int num4 = Mathf.Min((int)((refPos.z + maxDistance + 8640f) / 16f + 0.5f), 1080);
		Vector3 vector = Vector3.get_zero();
		Vector3 vector2 = Vector3.get_zero();
		float num5 = 0f;
		float num6 = 0f;
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			for (int i = num2; i <= num4; i++)
			{
				Vector3 vector3;
				vector3.z = (float)i * 16f - 8640f;
				for (int j = num; j <= num3; j++)
				{
					vector3.x = (float)j * 16f - 8640f;
					float num7 = (vector3.x - refPos.x) / maxDistance;
					float num8 = (vector3.z - refPos.z) / maxDistance;
					float num9 = 1f - (num8 * num8 + num7 * num7);
					if (num9 > 0.001f)
					{
						int height = (int)array[i * 1081 + j].m_height;
						if (height >= 8)
						{
							vector3.y = (float)((int)this.m_blockHeights[i * 1081 + j] + height) * 0.015625f;
							vector += vector3 * num9;
							num5 += num9;
						}
						else if (height == 0)
						{
							vector3.y = (float)this.m_rawHeights2[i * 1081 + j] * 0.015625f;
							vector2 += vector3 * num9;
							num6 += num9;
						}
					}
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num5 != 0f && num6 != 0f)
		{
			vector *= 0.5f / num5;
			vector2 *= 0.5f / num6;
			position = vector + vector2;
			direction = vector - vector2;
			direction.y = 0f;
			waterHeight = vector.y * 2f;
			return direction.get_sqrMagnitude() >= 1f;
		}
		position = Vector3.get_zero();
		direction = Vector3.get_zero();
		waterHeight = 0f;
		return false;
	}

	public bool CalculateWaterFlow(Segment2 segment, float radius, out Vector3 position, out Vector3 flow)
	{
		Vector3 vector = Vector3.get_zero();
		Vector3 zero = Vector3.get_zero();
		float num = 0f;
		float num2 = 0f;
		int num3 = Mathf.Max(1, Mathf.RoundToInt(segment.Length() * 0.0078125f));
		WaterSimulation.Cell[] array = this.m_waterSimulation.BeginRead();
		try
		{
			for (int i = 0; i < num3; i++)
			{
				Segment2 segment2 = segment.Cut((float)i / (float)num3, (float)(i + 1) / (float)num3);
				Vector2 vector2 = segment2.Min();
				Vector2 vector3 = segment2.Max();
				int num4 = Mathf.Max((int)((vector2.x - radius + 8640f) / 16f + 0.5f), 0);
				int num5 = Mathf.Max((int)((vector2.y - radius + 8640f) / 16f + 0.5f), 0);
				int num6 = Mathf.Min((int)((vector3.x + radius + 8640f) / 16f + 0.5f), 1080);
				int num7 = Mathf.Min((int)((vector3.y + radius + 8640f) / 16f + 0.5f), 1080);
				Vector3 vector4;
				vector4.y = 0f;
				for (int j = num5; j <= num7; j++)
				{
					vector4.z = (float)j * 16f - 8640f;
					for (int k = num4; k <= num6; k++)
					{
						vector4.x = (float)k * 16f - 8640f;
						float num9;
						float num8 = 1f - segment2.DistanceSqr(VectorUtils.XZ(vector4), ref num9) / (radius * radius);
						if (num8 > 0.001f)
						{
							int num10 = j * 1081 + k;
							WaterSimulation.Cell cell = array[num10];
							int height = (int)cell.m_height;
							num2 += num8;
							if (height >= 8)
							{
								vector4.y = (float)((int)this.m_blockHeights[num10] + height) * 0.015625f;
								zero.x += (float)cell.m_velocityX * (0.00048828125f * num8);
								zero.z += (float)cell.m_velocityZ * (0.00048828125f * num8);
								num8 *= (float)height;
								vector += vector4 * num8;
								num += num8;
							}
						}
					}
				}
			}
		}
		finally
		{
			this.m_waterSimulation.EndRead();
		}
		if (num != 0f)
		{
			position = vector / num;
			flow = zero / num2;
			return true;
		}
		position = Vector3.get_zero();
		flow = Vector3.get_zero();
		return false;
	}

	public float SampleDetailHeight(float x, float z, out float slopeX, out float slopeZ)
	{
		int num = 4320;
		int num2 = (int)x;
		int num3 = (int)z;
		int x2 = Mathf.Clamp(num2, 0, num);
		int z2 = Mathf.Clamp(num3, 0, num);
		int x3 = Mathf.Clamp(num2 + 1, 0, num);
		int z3 = Mathf.Clamp(num3 + 1, 0, num);
		float num4 = x - (float)num2;
		float num5 = z - (float)num3;
		float detailHeight = this.GetDetailHeight(x2, z2);
		float detailHeight2 = this.GetDetailHeight(x3, z2);
		float detailHeight3 = this.GetDetailHeight(x2, z3);
		float detailHeight4 = this.GetDetailHeight(x3, z3);
		slopeX = (detailHeight2 + detailHeight4 - detailHeight - detailHeight3) * 0.5f;
		slopeZ = (detailHeight3 + detailHeight4 - detailHeight - detailHeight2) * 0.5f;
		float num6 = Mathf.Lerp(detailHeight, detailHeight2, num4);
		float num7 = Mathf.Lerp(detailHeight3, detailHeight4, num4);
		return Mathf.Lerp(num6, num7, num5);
	}

	public float SampleDetailHeight(float x, float z)
	{
		int num = 4320;
		int num2 = (int)x;
		int num3 = (int)z;
		int num4 = Mathf.Clamp(num2, 0, num);
		int num5 = Mathf.Clamp(num3, 0, num);
		int num6 = Mathf.Min(num4 / 480, 8);
		int num7 = Mathf.Min(num5 / 480, 8);
		if (this.m_patches[num7 * 9 + num6].m_simDetailIndex == 0)
		{
			return this.SampleFinalHeight(x * 0.25f, z * 0.25f);
		}
		int x2 = Mathf.Clamp(num2 + 1, 0, num);
		int z2 = Mathf.Clamp(num3 + 1, 0, num);
		float num8 = x - (float)num2;
		float num9 = z - (float)num3;
		float detailHeight = this.GetDetailHeight(num4, num5);
		float detailHeight2 = this.GetDetailHeight(x2, num5);
		float detailHeight3 = this.GetDetailHeight(num4, z2);
		float detailHeight4 = this.GetDetailHeight(x2, z2);
		float num10 = Mathf.Lerp(detailHeight, detailHeight2, num8);
		float num11 = Mathf.Lerp(detailHeight3, detailHeight4, num8);
		return Mathf.Lerp(num10, num11, num9);
	}

	public float SampleDetailHeightSmooth(float x, float z)
	{
		int num = 4320;
		int num2 = (int)x;
		int num3 = (int)z;
		int num4 = Mathf.Clamp(num2, 0, num);
		int num5 = Mathf.Clamp(num3, 0, num);
		int num6 = Mathf.Min(num4 / 480, 8);
		int num7 = Mathf.Min(num5 / 480, 8);
		if (this.m_patches[num7 * 9 + num6].m_simDetailIndex == 0)
		{
			return this.SampleFinalHeightSmooth(x * 0.25f, z * 0.25f);
		}
		int x2 = Mathf.Clamp(num2 + 1, 0, num);
		int z2 = Mathf.Clamp(num3 + 1, 0, num);
		float num8 = x - (float)num2;
		float num9 = z - (float)num3;
		float detailHeightSmooth = this.GetDetailHeightSmooth(num4, num5);
		float detailHeightSmooth2 = this.GetDetailHeightSmooth(x2, num5);
		float detailHeightSmooth3 = this.GetDetailHeightSmooth(num4, z2);
		float detailHeightSmooth4 = this.GetDetailHeightSmooth(x2, z2);
		float num10 = Mathf.Lerp(detailHeightSmooth, detailHeightSmooth2, num8);
		float num11 = Mathf.Lerp(detailHeightSmooth3, detailHeightSmooth4, num8);
		return Mathf.Lerp(num10, num11, num9);
	}

	public float GetDetailHeight(int x, int z)
	{
		int num = Mathf.Min(x / 480, 8);
		int num2 = Mathf.Min(z / 480, 8);
		int num3 = num2 * 9 + num;
		int simDetailIndex = this.m_patches[num3].m_simDetailIndex;
		if (simDetailIndex == 0)
		{
			return this.SampleFinalHeight((float)x * 0.25f, (float)z * 0.25f);
		}
		int num4 = x - num * 480;
		int num5 = z - num2 * 480;
		if ((num4 == 0 && num != 0 && this.m_patches[num3 - 1].m_simDetailIndex == 0) || (num5 == 0 && num2 != 0 && this.m_patches[num3 - 9].m_simDetailIndex == 0))
		{
			return this.SampleFinalHeight((float)x * 0.25f, (float)z * 0.25f);
		}
		int num6 = (simDetailIndex - 1) * 481 * 481;
		return (float)this.m_detailHeights[num6 + num5 * 481 + num4];
	}

	public float GetBlockHeight(int x, int z)
	{
		int num = Mathf.Min(x / 480, 8);
		int num2 = Mathf.Min(z / 480, 8);
		int num3 = num2 * 9 + num;
		int simDetailIndex = this.m_patches[num3].m_simDetailIndex;
		if (simDetailIndex == 0)
		{
			return this.SampleBlockHeight((float)x * 0.25f, (float)z * 0.25f);
		}
		int num4 = x - num * 480;
		int num5 = z - num2 * 480;
		if ((num4 == 0 && num != 0 && this.m_patches[num3 - 1].m_simDetailIndex == 0) || (num5 == 0 && num2 != 0 && this.m_patches[num3 - 9].m_simDetailIndex == 0))
		{
			return this.SampleBlockHeight((float)x * 0.25f, (float)z * 0.25f);
		}
		int num6 = (simDetailIndex - 1) * 481 * 481;
		return (float)this.m_detailHeights2[num6 + num5 * 481 + num4];
	}

	public float GetDetailHeightSmooth(int x, int z)
	{
		int num = Mathf.Min(x / 480, 8);
		int num2 = Mathf.Min(z / 480, 8);
		int num3 = num2 * 9 + num;
		int simDetailIndex = this.m_patches[num3].m_simDetailIndex;
		if (simDetailIndex == 0)
		{
			return this.SampleFinalHeightSmooth((float)x * 0.25f, (float)z * 0.25f);
		}
		int num4 = x - num * 480;
		int num5 = z - num2 * 480;
		if ((num4 == 0 && num != 0 && this.m_patches[num3 - 1].m_simDetailIndex == 0) || (num5 == 0 && num2 != 0 && this.m_patches[num3 - 9].m_simDetailIndex == 0))
		{
			return this.SampleFinalHeightSmooth((float)x * 0.25f, (float)z * 0.25f);
		}
		int num6 = (simDetailIndex - 1) * 481 * 481;
		return (float)this.m_detailHeights[num6 + num5 * 481 + num4];
	}

	public static float SmoothSample(float h1, float h2, float h3, float h4, float t)
	{
		float num;
		if (h3 > h1)
		{
			num = h2 + Mathf.Min((h3 - h1) * 0.166666672f, Mathf.Max(h2 - h1, h3 - h2));
		}
		else
		{
			num = h2 + Mathf.Max((h3 - h1) * 0.166666672f, Mathf.Min(h2 - h1, h3 - h2));
		}
		float num2;
		if (h2 > h4)
		{
			num2 = h3 + Mathf.Min((h2 - h4) * 0.166666672f, Mathf.Max(h3 - h4, h2 - h3));
		}
		else
		{
			num2 = h3 + Mathf.Max((h2 - h4) * 0.166666672f, Mathf.Min(h3 - h4, h2 - h3));
		}
		float num3 = 1f - t;
		return h2 * num3 * num3 * num3 + 3f * num * t * num3 * num3 + 3f * num2 * t * t * num3 + h3 * t * t * t;
	}

	public TerrainManager.SurfaceCell SampleDetailSurface(float x, float z)
	{
		int num = 4320;
		int num2 = Mathf.Clamp(Mathf.RoundToInt(x * 2048f), 0, (num << 11) - 1);
		int num3 = Mathf.Clamp(Mathf.RoundToInt(z * 2048f), 0, (num << 11) - 1);
		int num4 = num2 & 2047;
		int num5 = num3 & 2047;
		num2 >>= 11;
		num3 >>= 11;
		int x2 = Mathf.Min(num2 + 1, num - 1);
		int z2 = Mathf.Min(num3 + 1, num - 1) * num;
		TerrainManager.SurfaceCell surfaceCell = this.GetSurfaceCell(num2, num3);
		TerrainManager.SurfaceCell surfaceCell2 = this.GetSurfaceCell(x2, num3);
		TerrainManager.SurfaceCell surfaceCell3 = this.GetSurfaceCell(num2, z2);
		TerrainManager.SurfaceCell surfaceCell4 = this.GetSurfaceCell(x2, z2);
		int num6 = ((int)surfaceCell.m_clipped << 11) + (int)(surfaceCell3.m_clipped - surfaceCell.m_clipped) * num4;
		int num7 = ((int)surfaceCell.m_ruined << 11) + (int)(surfaceCell3.m_ruined - surfaceCell.m_ruined) * num4;
		int num8 = ((int)surfaceCell.m_gravel << 11) + (int)(surfaceCell3.m_gravel - surfaceCell.m_gravel) * num4;
		int num9 = ((int)surfaceCell.m_field << 11) + (int)(surfaceCell3.m_field - surfaceCell.m_field) * num4;
		int num10 = ((int)surfaceCell.m_pavementA << 11) + (int)(surfaceCell3.m_pavementA - surfaceCell.m_pavementA) * num4;
		int num11 = ((int)surfaceCell.m_pavementB << 11) + (int)(surfaceCell3.m_pavementB - surfaceCell.m_pavementB) * num4;
		int num12 = ((int)surfaceCell2.m_clipped << 11) + (int)(surfaceCell4.m_clipped - surfaceCell2.m_clipped) * num4;
		int num13 = ((int)surfaceCell2.m_ruined << 11) + (int)(surfaceCell4.m_ruined - surfaceCell2.m_ruined) * num4;
		int num14 = ((int)surfaceCell2.m_gravel << 11) + (int)(surfaceCell4.m_gravel - surfaceCell2.m_gravel) * num4;
		int num15 = ((int)surfaceCell2.m_field << 11) + (int)(surfaceCell4.m_field - surfaceCell2.m_field) * num4;
		int num16 = ((int)surfaceCell2.m_pavementA << 11) + (int)(surfaceCell4.m_pavementA - surfaceCell2.m_pavementA) * num4;
		int num17 = ((int)surfaceCell2.m_pavementB << 11) + (int)(surfaceCell4.m_pavementB - surfaceCell2.m_pavementB) * num4;
		TerrainManager.SurfaceCell result;
		result.m_clipped = (byte)((num6 << 11) + (num12 - num6) * num5 >> 22);
		result.m_pavementA = (byte)((num10 << 11) + (num16 - num10) * num5 >> 22);
		result.m_pavementB = (byte)((num11 << 11) + (num17 - num11) * num5 >> 22);
		result.m_ruined = (byte)((num7 << 11) + (num13 - num7) * num5 >> 22);
		result.m_gravel = (byte)((num8 << 11) + (num14 - num8) * num5 >> 22);
		result.m_field = (byte)((num9 << 11) + (num15 - num9) * num5 >> 22);
		return result;
	}

	public void UpdateBounds(int minX, int minZ, int maxX, int maxZ)
	{
		int i = 4;
		int num = 144;
		int num2 = Mathf.Max(0, minX * num / 1080);
		int num3 = Mathf.Max(0, minZ * num / 1080);
		int num4 = Mathf.Min(num - 1, (maxX * num + num - 1) / 1080);
		int num5 = Mathf.Min(num - 1, (maxZ * num + num - 1) / 1080);
		for (int j = num3; j <= num5; j++)
		{
			minZ = j * 1080 / num;
			maxZ = (j * 1080 + 1080 + num - 1) / num;
			for (int k = num2; k <= num4; k++)
			{
				minX = k * 1080 / num;
				maxX = (k * 1080 + 1080 + num - 1) / num;
				int num6 = 65535;
				int num7 = 0;
				for (int l = minZ; l <= maxZ; l++)
				{
					for (int m = minX; m <= maxX; m++)
					{
						int num8 = (int)this.m_finalHeights[l * 1081 + m];
						if (num8 < num6)
						{
							num6 = num8;
						}
						if (num8 > num7)
						{
							num7 = num8;
						}
					}
				}
				TerrainManager.CellBounds cellBounds;
				cellBounds.m_minHeight = (ushort)num6;
				cellBounds.m_maxHeight = (ushort)num7;
				this.m_rawBounds[i][j * num + k] = cellBounds;
			}
		}
		while (i > 0)
		{
			int num9 = i - 1;
			int num10 = num >> 1;
			num2 >>= 1;
			num3 >>= 1;
			num4 >>= 1;
			num5 >>= 1;
			for (int n = num3; n <= num5; n++)
			{
				for (int num11 = num2; num11 <= num4; num11++)
				{
					int num12 = num11 << 1;
					int num13 = n << 1;
					TerrainManager.CellBounds cellBounds2 = this.m_rawBounds[i][num13 * num + num12];
					TerrainManager.CellBounds cellBounds3 = this.m_rawBounds[i][num13 * num + num12 + 1];
					TerrainManager.CellBounds cellBounds4 = this.m_rawBounds[i][num13 * num + num12 + num];
					TerrainManager.CellBounds cellBounds5 = this.m_rawBounds[i][num13 * num + num12 + num + 1];
					TerrainManager.CellBounds cellBounds6;
					cellBounds6.m_minHeight = (ushort)Mathf.Min(Mathf.Min((int)cellBounds2.m_minHeight, (int)cellBounds3.m_minHeight), Mathf.Min((int)cellBounds4.m_minHeight, (int)cellBounds5.m_minHeight));
					cellBounds6.m_maxHeight = (ushort)Mathf.Max(Mathf.Max((int)cellBounds2.m_maxHeight, (int)cellBounds3.m_maxHeight), Mathf.Max((int)cellBounds4.m_maxHeight, (int)cellBounds5.m_maxHeight));
					this.m_rawBounds[num9][n * num10 + num11] = cellBounds6;
				}
			}
			i = num9;
			num = num10;
		}
	}

	public TerrainManager.CellBounds GetBounds(int x, int z, int level)
	{
		int num = 9 << level;
		return this.m_rawBounds[level][z * num + x];
	}

	public static void Managers_TerrainUpdated(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		for (int i = 0; i < TerrainManager.m_managers.m_size; i++)
		{
			TerrainManager.m_managers.m_buffer[i].TerrainUpdated(heightArea, surfaceArea, zoneArea);
		}
	}

	public static void Managers_AfterTerrainUpdate(TerrainArea heightArea, TerrainArea surfaceArea, TerrainArea zoneArea)
	{
		for (int i = 0; i < TerrainManager.m_managers.m_size; i++)
		{
			TerrainManager.m_managers.m_buffer[i].AfterTerrainUpdate(heightArea, surfaceArea, zoneArea);
		}
	}

	protected override void SimulationStepImpl(int subStep)
	{
		this.m_waterSimulation.SimulationStep(subStep);
		if (subStep != 0)
		{
			int num = (int)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex & 63u);
			int num2 = num * 1081 >> 6;
			int num3 = ((num + 1) * 1081 >> 6) - 1;
			int num4 = ((Singleton<ToolManager>.get_instance().m_properties.m_mode & ItemClass.Availability.Game) == ItemClass.Availability.None) ? 512 : 128;
			int num5 = 512;
			for (int i = num2; i <= num3; i++)
			{
				int num6 = i * 1081;
				int num7 = (int)this.m_modifiedBlockMinX[i];
				int num8 = (int)this.m_modifiedBlockMaxX[i];
				int num9 = 10000;
				int num10 = -10000;
				for (int j = num7; j <= num8; j++)
				{
					int num11 = (int)this.m_blockHeights[num6 + j];
					int num12 = (int)this.m_blockHeights2[num6 + j];
					if (num12 != num11)
					{
						if (num12 > num11)
						{
							num11 = Mathf.Min(num12, num11 + num4);
						}
						else
						{
							num11 = Mathf.Max(num12, num11 - num5);
						}
						if (num12 != num11)
						{
							if (j < num9)
							{
								num9 = j;
							}
							num10 = j;
						}
						this.m_blockHeights[num6 + j] = (ushort)num11;
					}
				}
				this.m_modifiedBlockMinX[i] = (short)num9;
				this.m_modifiedBlockMaxX[i] = (short)num10;
			}
		}
	}

	private void RefreshPatchFlatness()
	{
		ushort[] rawHeights = this.m_rawHeights;
		int num = 120;
		int num2 = Mathf.RoundToInt(1024f) >> 1;
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				int num3 = 0;
				int num4 = j * num;
				int num5 = i * num;
				int num6 = (j + 1) * num - 1;
				int num7 = (i + 1) * num - 1;
				for (int k = num5; k <= num7; k++)
				{
					for (int l = num4; l <= num6; l++)
					{
						int num8 = k * 1081 + l;
						int num9 = (int)rawHeights[num8];
						int num10 = (int)rawHeights[num8 + 1080 + 1];
						int num11 = (int)rawHeights[num8 + 1];
						int num12 = (int)rawHeights[num8 + 1080 + 2];
						int num13 = Mathf.Min(Mathf.Min(num9, num10), Mathf.Min(num11, num12));
						int num14 = Mathf.Max(Mathf.Max(num9, num10), Mathf.Max(num11, num12));
						int num15 = Mathf.Min(num14 - num13, 32768);
						num15 = num15 * num15 / num2;
						num3 += (num2 << 12) / (num2 + num15);
					}
				}
				TerrainPatch terrainPatch = this.m_patches[i * 9 + j];
				terrainPatch.m_flatness = (float)num3 / ((float)(num * num) * 4096f);
			}
		}
	}

	public override void UpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("TerrainManager.UpdateData");
		base.UpdateData(mode);
		this.m_lastUpdatedPatch = -1;
		int num = 120;
		ushort[] blockHeights = this.m_blockHeights;
		ushort[] blockHeights2 = this.m_blockHeights2;
		for (int i = 0; i <= 9; i++)
		{
			for (int j = 0; j <= 9; j++)
			{
				if (i < 9 && j < 9)
				{
					int num2 = j * num;
					int num3 = i * num;
					int num4 = (j + 1) * num;
					int num5 = (i + 1) * num;
					TerrainModify.UpdateArea(num2, num3, num4, num5, true, true, true);
					if (j > 0)
					{
						num2++;
					}
					if (i > 0)
					{
						num3++;
					}
					for (int k = num3; k <= num5; k++)
					{
						int num6 = k * 1081;
						for (int l = num2; l <= num4; l++)
						{
							int num7 = num6 + l;
							blockHeights[num7] += blockHeights2[num7];
						}
					}
				}
				if (i > 0 && j > 0)
				{
					int index = (i - 1) * 9 + (j - 1);
					Singleton<LoadingManager>.get_instance().QueueLoadingAction(this.UpdateTerrainPatch(index));
				}
			}
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewGameFromScenario)
		{
			this.m_dirtBuffer = 262144;
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_terrainToolNotUsed == null)
		{
			this.m_terrainToolNotUsed = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_notEnoughDirt == null)
		{
			this.m_notEnoughDirt = new GenericGuide();
		}
		if (mode == SimulationManager.UpdateMode.NewGameFromMap || mode == SimulationManager.UpdateMode.NewScenarioFromMap || mode == SimulationManager.UpdateMode.UpdateScenarioFromMap || this.m_tooMuchDirt == null)
		{
			this.m_tooMuchDirt = new GenericGuide();
		}
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void LateUpdateData(SimulationManager.UpdateMode mode)
	{
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginLoading("TerrainManager.LateUpdateData");
		base.LateUpdateData(mode);
		if (this.m_lastUpdatedPatch != 80)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.PauseLoading();
			while (!Monitor.TryEnter(this.m_updateLock, SimulationManager.SYNCHRONIZE_TIMEOUT))
			{
			}
			try
			{
				while (this.m_lastUpdatedPatch != 80 && !Singleton<SimulationManager>.get_instance().Terminated)
				{
					Monitor.Wait(this.m_updateLock, 1);
				}
			}
			finally
			{
				Monitor.Exit(this.m_updateLock);
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.ContinueLoading();
		}
		this.m_waterSimulation.EnableWaterFlow();
		Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndLoading();
	}

	public override void GetData(FastList<IDataContainer> data)
	{
		base.GetData(data);
		data.Add(new TerrainManager.Data());
		data.Add(new WaterSimulation.Data());
	}

	string ISimulationManager.GetName()
	{
		return base.GetName();
	}

	ThreadProfiler ISimulationManager.GetSimulationProfiler()
	{
		return base.GetSimulationProfiler();
	}

	void ISimulationManager.SimulationStep(int subStep)
	{
		base.SimulationStep(subStep);
	}

	string IRenderableManager.GetName()
	{
		return base.GetName();
	}

	DrawCallData IRenderableManager.GetDrawCallData()
	{
		return base.GetDrawCallData();
	}

	void IRenderableManager.BeginRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginRendering(cameraInfo);
	}

	void IRenderableManager.EndRendering(RenderManager.CameraInfo cameraInfo)
	{
		base.EndRendering(cameraInfo);
	}

	void IRenderableManager.BeginOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.BeginOverlay(cameraInfo);
	}

	void IRenderableManager.EndOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.EndOverlay(cameraInfo);
	}

	void IRenderableManager.UndergroundOverlay(RenderManager.CameraInfo cameraInfo)
	{
		base.UndergroundOverlay(cameraInfo);
	}

	public const float RAW_CELL_SIZE = 16f;

	public const float DETAIL_CELL_SIZE = 4f;

	public const float TERRAIN_HEIGHT = 1024f;

	public const float TERRAIN_LEVEL = 60f;

	public const int RAW_RESOLUTION = 1080;

	public const int RAW_MAP_RESOLUTION = 128;

	public const int DETAIL_MAP_RESOLUTION = 512;

	public const int PATCH_RESOLUTION = 9;

	public const int MAX_SUBPATCH_LEVELS = 4;

	public const int DETAIL_RESOLUTION = 480;

	public const int DETAIL_SHIFT = 2;

	public const int MIN_WATER_AMOUNT = 8;

	public const int DIRT_BUFFER_SIZE = 524288;

	[NonSerialized]
	public TerrainArea m_tmpSurfaceModified;

	[NonSerialized]
	public TerrainArea m_tmpZonesModified;

	private ushort[] m_rawHeights;

	private ushort[] m_rawHeights2;

	private ushort[] m_finalHeights;

	private ushort[] m_blockHeights;

	private ushort[] m_blockHeights2;

	private TerrainManager.CellBounds[][] m_rawBounds;

	private TerrainManager.SurfaceCell[] m_rawSurface;

	public int m_detailPatchCount;

	private WaterSimulation m_waterSimulation;

	private DecorationRenderer m_decoRenderer;

	private bool m_renderZones;

	private bool m_renderTopography;

	private bool m_renderTopographyInfo;

	private bool m_transparentWater;

	private int m_dirtBuffer;

	private volatile int m_lastUpdatedPatch;

	private object m_updateLock;

	private static FastList<ITerrainManager> m_managers = new FastList<ITerrainManager>();

	[NonSerialized]
	private ushort[] m_backupHeights;

	[NonSerialized]
	private ushort[] m_undoBuffer;

	[NonSerialized]
	public GenericGuide m_terrainToolNotUsed;

	[NonSerialized]
	public GenericGuide m_notEnoughDirt;

	[NonSerialized]
	public GenericGuide m_tooMuchDirt;

	[NonSerialized]
	public int m_modifyingLevel;

	[NonSerialized]
	public bool m_modifyingHeights;

	[NonSerialized]
	public bool m_modifyingSurface;

	[NonSerialized]
	public bool m_modifyingZones;

	[NonSerialized]
	public TerrainArea m_heightsModified;

	[NonSerialized]
	public TerrainArea m_surfaceModified;

	[NonSerialized]
	public TerrainArea m_zonesModified;

	[NonSerialized]
	public TerrainPatch[] m_patches;

	[NonSerialized]
	public TerrainModify.HeightModification[] m_tempHeights;

	[NonSerialized]
	public TerrainManager.SurfaceCell[] m_tempSurface;

	[NonSerialized]
	public TerrainModify.ZoneModification[] m_tempZones;

	[NonSerialized]
	public ushort[] m_detailHeights;

	[NonSerialized]
	public ushort[] m_detailHeights2;

	[NonSerialized]
	public TerrainManager.SurfaceCell[] m_detailSurface;

	[NonSerialized]
	public TerrainManager.ZoneCell[] m_detailZones;

	[NonSerialized]
	public short[] m_modifiedBlockMinX;

	[NonSerialized]
	public short[] m_modifiedBlockMaxX;

	[NonSerialized]
	public int ID_MainTex;

	[NonSerialized]
	public int ID_SurfaceTexA;

	[NonSerialized]
	public int ID_SurfaceTexB;

	[NonSerialized]
	public int ID_SurfaceANew;

	[NonSerialized]
	public int ID_SurfaceAOld;

	[NonSerialized]
	public int ID_SurfaceBNew;

	[NonSerialized]
	public int ID_SurfaceBOld;

	[NonSerialized]
	public int ID_TerrainHeight;

	[NonSerialized]
	public int ID_ZoneLayout;

	[NonSerialized]
	public int ID_DecorationDiffuse;

	[NonSerialized]
	public int ID_DecorationXYCA;

	[NonSerialized]
	public int ID_HeightMapping;

	[NonSerialized]
	public int ID_SurfaceMapping;

	[NonSerialized]
	public int ID_TerrainMapping;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock1;

	[NonSerialized]
	public MaterialPropertyBlock m_materialBlock2;

	[NonSerialized]
	public Material m_terrainMaterial;

	[NonSerialized]
	public Material m_topographyMaterial;

	[NonSerialized]
	public Material m_decorationMaterial;

	[NonSerialized]
	public Material m_waterMaterial;

	[NonSerialized]
	public Material m_waterTransparentMaterial;

	[NonSerialized]
	public int m_terrainLayer;

	[NonSerialized]
	public int m_decorationLayer;

	[NonSerialized]
	public int m_waterLayer;

	[NonSerialized]
	public TerrainWrapper m_TerrainWrapper;

	public struct ZoneCell
	{
		public byte m_angle;

		public byte m_zone;

		public sbyte m_offsetX;

		public sbyte m_offsetZ;
	}

	public struct SurfaceCell
	{
		public byte m_clipped;

		public byte m_ruined;

		public byte m_gravel;

		public byte m_field;

		public byte m_pavementA;

		public byte m_pavementB;
	}

	public struct CellBounds
	{
		public ushort m_minHeight;

		public ushort m_maxHeight;
	}

	public class Data : IDataContainer
	{
		public void Serialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginSerialize(s, "TerrainManager");
			TerrainManager instance = Singleton<TerrainManager>.get_instance();
			ushort[] rawHeights = instance.m_rawHeights;
			ushort[] blockHeights = instance.m_blockHeights;
			ushort[] blockHeights2 = instance.m_blockHeights2;
			int num = rawHeights.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginWrite(s);
			for (int i = 0; i < num; i++)
			{
				uShort.Write(rawHeights[i]);
			}
			uShort.EndWrite();
			EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginWrite(s);
			for (int j = 0; j < num; j++)
			{
				if (rawHeights[j] != 0)
				{
					uShort2.Write(blockHeights[j] - blockHeights2[j]);
				}
			}
			uShort2.EndWrite();
			s.WriteInt32(instance.m_dirtBuffer);
			s.WriteObject<GenericGuide>(instance.m_terrainToolNotUsed);
			s.WriteObject<GenericGuide>(instance.m_notEnoughDirt);
			s.WriteObject<GenericGuide>(instance.m_tooMuchDirt);
			instance.RefreshPatchFlatness();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndSerialize(s, "TerrainManager");
		}

		public void Deserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginDeserialize(s, "TerrainManager");
			TerrainManager instance = Singleton<TerrainManager>.get_instance();
			ushort[] rawHeights = instance.m_rawHeights;
			ushort[] blockHeights = instance.m_blockHeights;
			int num = rawHeights.Length;
			EncodedArray.UShort uShort = EncodedArray.UShort.BeginRead(s);
			for (int i = 0; i < num; i++)
			{
				rawHeights[i] = uShort.Read();
			}
			uShort.EndRead();
			if (s.get_version() >= 96u)
			{
				EncodedArray.UShort uShort2 = EncodedArray.UShort.BeginRead(s);
				for (int j = 0; j < num; j++)
				{
					if (rawHeights[j] != 0)
					{
						blockHeights[j] = uShort2.Read();
					}
					else
					{
						blockHeights[j] = 0;
					}
				}
				uShort2.EndRead();
			}
			else
			{
				for (int k = 0; k < num; k++)
				{
					blockHeights[k] = 0;
				}
			}
			if (s.get_version() >= 243u)
			{
				instance.m_dirtBuffer = s.ReadInt32();
			}
			else
			{
				instance.m_dirtBuffer = 262144;
			}
			if (s.get_version() >= 244u)
			{
				instance.m_terrainToolNotUsed = s.ReadObject<GenericGuide>();
				instance.m_notEnoughDirt = s.ReadObject<GenericGuide>();
				instance.m_tooMuchDirt = s.ReadObject<GenericGuide>();
			}
			else
			{
				instance.m_terrainToolNotUsed = null;
				instance.m_notEnoughDirt = null;
				instance.m_tooMuchDirt = null;
			}
			instance.RefreshPatchFlatness();
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndDeserialize(s, "TerrainManager");
		}

		public void AfterDeserialize(DataSerializer s)
		{
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.BeginAfterDeserialize(s, "TerrainManager");
			TerrainManager instance = Singleton<TerrainManager>.get_instance();
			instance.m_detailPatchCount = 0;
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					instance.m_patches[i * 9 + j].m_simDetailIndex = 0;
				}
			}
			Singleton<LoadingManager>.get_instance().m_loadingProfilerSimulation.EndAfterDeserialize(s, "TerrainManager");
		}
	}
}
