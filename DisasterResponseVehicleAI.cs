﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.Math;
using UnityEngine;

public class DisasterResponseVehicleAI : CarAI
{
	public override Color GetColor(ushort vehicleID, ref Vehicle data, InfoManager.InfoMode infoMode)
	{
		if (infoMode == InfoManager.InfoMode.Destruction)
		{
			return Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[(int)infoMode].m_activeColor;
		}
		return base.GetColor(vehicleID, ref data, infoMode);
	}

	public override string GetLocalizedStatus(ushort vehicleID, ref Vehicle data, out InstanceID target)
	{
		if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_DISASTER_RESPONSE_RETURN");
		}
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_DISASTER_RESPONSE_WAIT");
		}
		if ((data.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
		{
			if (data.m_targetBuilding != 0)
			{
				target = InstanceID.Empty;
				target.Building = data.m_targetBuilding;
				return Locale.Get("VEHICLE_STATUS_DISASTER_RESPONSE_RESPONDING");
			}
		}
		else if ((data.m_flags & Vehicle.Flags.Emergency1) != (Vehicle.Flags)0 && data.m_targetBuilding != 0)
		{
			target = InstanceID.Empty;
			return Locale.Get("VEHICLE_STATUS_DISASTER_RESPONSE_SEARCHING");
		}
		target = InstanceID.Empty;
		return Locale.Get("VEHICLE_STATUS_CONFUSED");
	}

	public override void GetBufferStatus(ushort vehicleID, ref Vehicle data, out string localeKey, out int current, out int max)
	{
		localeKey = "Default";
		current = 0;
		max = 0;
	}

	public override void CreateVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.CreateVehicle(vehicleID, ref data);
		data.m_flags |= Vehicle.Flags.WaitingTarget;
		Singleton<CitizenManager>.get_instance().CreateUnits(out data.m_citizenUnits, ref Singleton<SimulationManager>.get_instance().m_randomizer, 0, vehicleID, 0, 0, 0, this.m_workerCount, 0);
	}

	public override void ReleaseVehicle(ushort vehicleID, ref Vehicle data)
	{
		this.RemoveOffers(vehicleID, ref data);
		this.RemoveSource(vehicleID, ref data);
		this.RemoveTarget(vehicleID, ref data);
		base.ReleaseVehicle(vehicleID, ref data);
	}

	public override void LoadVehicle(ushort vehicleID, ref Vehicle data)
	{
		base.LoadVehicle(vehicleID, ref data);
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle data, Vector3 physicsLodRefPos)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0 && (data.m_waitCounter += 1) > 20)
		{
			this.RemoveOffers(vehicleID, ref data);
			data.m_flags &= ~(Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2 | Vehicle.Flags.WaitingTarget);
			data.m_flags |= Vehicle.Flags.GoingBack;
			data.m_waitCounter = 0;
			if (!this.StartPathFind(vehicleID, ref data))
			{
				data.Unspawn(vehicleID);
			}
		}
		base.SimulationStep(vehicleID, ref data, physicsLodRefPos);
	}

	protected override void PathfindFailure(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Building[] expr_25_cp_0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer;
			ushort expr_25_cp_1 = data.m_targetBuilding;
			expr_25_cp_0[(int)expr_25_cp_1].m_flags = (expr_25_cp_0[(int)expr_25_cp_1].m_flags | Building.Flags.RoadAccessFailed);
		}
		base.PathfindFailure(vehicleID, ref data);
	}

	public override void SetSource(ushort vehicleID, ref Vehicle data, ushort sourceBuilding)
	{
		this.RemoveSource(vehicleID, ref data);
		data.m_sourceBuilding = sourceBuilding;
		if (sourceBuilding != 0)
		{
			data.Unspawn(vehicleID);
			data.m_frame0 = new Vehicle.Frame(Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].m_position, Quaternion.get_identity());
			data.m_frame1 = data.m_frame0;
			data.m_frame2 = data.m_frame0;
			data.m_frame3 = data.m_frame0;
			data.m_targetPos0 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].CalculateSidewalkPosition();
			data.m_targetPos0.w = 2f;
			data.m_targetPos1 = data.m_targetPos0;
			data.m_targetPos2 = data.m_targetPos0;
			data.m_targetPos3 = data.m_targetPos0;
			this.FrameDataUpdated(vehicleID, ref data, ref data.m_frame0);
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)sourceBuilding].AddOwnVehicle(vehicleID, ref data);
		}
	}

	public override void SetTarget(ushort vehicleID, ref Vehicle data, ushort targetBuilding)
	{
		this.RemoveTarget(vehicleID, ref data);
		data.m_targetBuilding = targetBuilding;
		data.m_flags &= ~Vehicle.Flags.WaitingTarget;
		data.m_waitCounter = 0;
		if (targetBuilding != 0)
		{
			data.m_flags &= ~Vehicle.Flags.GoingBack;
			data.m_flags |= Vehicle.Flags.Emergency2;
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)targetBuilding].AddGuestVehicle(vehicleID, ref data);
		}
		else
		{
			data.m_flags &= ~(Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2);
			if (!this.ShouldReturnToSource(vehicleID, ref data))
			{
				TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
				offer.Priority = 3;
				offer.Vehicle = vehicleID;
				if (data.m_sourceBuilding != 0)
				{
					offer.Position = (data.GetLastFramePosition() + Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].m_position) * 0.5f;
				}
				else
				{
					offer.Position = data.GetLastFramePosition();
				}
				offer.Amount = 1;
				offer.Active = true;
				Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
				data.m_flags |= Vehicle.Flags.WaitingTarget;
			}
			else
			{
				data.m_flags |= Vehicle.Flags.GoingBack;
			}
		}
		if (!this.StartPathFind(vehicleID, ref data))
		{
			data.Unspawn(vehicleID);
		}
	}

	public override void BuildingRelocated(ushort vehicleID, ref Vehicle data, ushort building)
	{
		base.BuildingRelocated(vehicleID, ref data, building);
		if (building == data.m_sourceBuilding)
		{
			if ((data.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
			{
				this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
			}
		}
		else if (building == data.m_targetBuilding && (data.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			this.InvalidPath(vehicleID, ref data, vehicleID, ref data);
		}
	}

	public override void StartTransfer(ushort vehicleID, ref Vehicle data, TransferManager.TransferReason material, TransferManager.TransferOffer offer)
	{
		if (material == (TransferManager.TransferReason)data.m_transferType)
		{
			if ((data.m_flags & (Vehicle.Flags.GoingBack | Vehicle.Flags.WaitingTarget)) != (Vehicle.Flags)0)
			{
				this.SetTarget(vehicleID, ref data, offer.Building);
			}
		}
		else
		{
			base.StartTransfer(vehicleID, ref data, material, offer);
		}
	}

	public override void GetSize(ushort vehicleID, ref Vehicle data, out int size, out int max)
	{
		size = 0;
		max = this.m_efficiency;
	}

	public override void SimulationStep(ushort vehicleID, ref Vehicle vehicleData, ref Vehicle.Frame frameData, ushort leaderID, ref Vehicle leaderData, int lodPhysics)
	{
		base.SimulationStep(vehicleID, ref vehicleData, ref frameData, leaderID, ref leaderData, lodPhysics);
		bool flag = false;
		if (vehicleData.m_targetBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			Vector3 vector = instance.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].CalculateSidewalkPosition();
			flag = ((vector - frameData.m_position).get_sqrMagnitude() < 4096f);
			bool flag2 = (vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 || frameData.m_velocity.get_sqrMagnitude() < 0.0100000007f;
			if (flag && (vehicleData.m_flags & Vehicle.Flags.Emergency2) != (Vehicle.Flags)0)
			{
				vehicleData.m_flags = ((vehicleData.m_flags & ~Vehicle.Flags.Emergency2) | Vehicle.Flags.Emergency1);
			}
			if (flag && flag2)
			{
				if (vehicleData.m_blockCounter > 8)
				{
					vehicleData.m_blockCounter = 8;
				}
				if (vehicleData.m_blockCounter == 8 && (vehicleData.m_flags & Vehicle.Flags.Stopped) == (Vehicle.Flags)0)
				{
					this.ArriveAtTarget(leaderID, ref leaderData);
				}
				if (this.CheckBuilding(vehicleID, ref vehicleData, vehicleData.m_targetBuilding, ref Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding]))
				{
					this.SetTarget(vehicleID, ref vehicleData, 0);
				}
			}
			else if ((instance.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].m_flags & Building.Flags.Collapsed) == Building.Flags.None || instance.m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].m_levelUpProgress == 255)
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
		}
		if ((vehicleData.m_flags & Vehicle.Flags.Stopped) != (Vehicle.Flags)0 && !flag && this.CanLeave(vehicleID, ref vehicleData))
		{
			vehicleData.m_flags &= ~Vehicle.Flags.Stopped;
			vehicleData.m_flags |= Vehicle.Flags.Leaving;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) == (Vehicle.Flags)0)
		{
			if (this.ShouldReturnToSource(vehicleID, ref vehicleData))
			{
				this.SetTarget(vehicleID, ref vehicleData, 0);
			}
		}
		else if ((ulong)(Singleton<SimulationManager>.get_instance().m_currentFrameIndex >> 4 & 15u) == (ulong)((long)(vehicleID & 15)) && !this.ShouldReturnToSource(vehicleID, ref vehicleData))
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Priority = 3;
			offer.Vehicle = vehicleID;
			offer.Position = frameData.m_position;
			offer.Amount = 1;
			offer.Active = true;
			Singleton<TransferManager>.get_instance().AddIncomingOffer((TransferManager.TransferReason)vehicleData.m_transferType, offer);
		}
	}

	private bool ShouldReturnToSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			if ((instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_flags & Building.Flags.Active) == Building.Flags.None && instance.m_buildings.m_buffer[(int)data.m_sourceBuilding].m_fireIntensity == 0)
			{
				return true;
			}
		}
		return false;
	}

	private void RemoveOffers(ushort vehicleID, ref Vehicle data)
	{
		if ((data.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			TransferManager.TransferOffer offer = default(TransferManager.TransferOffer);
			offer.Vehicle = vehicleID;
			Singleton<TransferManager>.get_instance().RemoveIncomingOffer((TransferManager.TransferReason)data.m_transferType, offer);
		}
	}

	private void RemoveSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_sourceBuilding].RemoveOwnVehicle(vehicleID, ref data);
			data.m_sourceBuilding = 0;
		}
	}

	private void RemoveTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding != 0)
		{
			Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)data.m_targetBuilding].RemoveGuestVehicle(vehicleID, ref data);
			data.m_targetBuilding = 0;
		}
	}

	private bool ArriveAtTarget(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_targetBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		for (int i = 0; i < this.m_workerCount; i++)
		{
			this.CreateWorker(vehicleID, ref data, Citizen.AgePhase.Senior0, this.m_animalCount > i);
		}
		data.m_flags |= Vehicle.Flags.Stopped;
		return false;
	}

	public override bool CanLeave(ushort vehicleID, ref Vehicle vehicleData)
	{
		CitizenManager instance = Singleton<CitizenManager>.get_instance();
		bool result = true;
		uint num = vehicleData.m_citizenUnits;
		int num2 = 0;
		while (num != 0u)
		{
			uint nextUnit = instance.m_units.m_buffer[(int)((UIntPtr)num)].m_nextUnit;
			for (int i = 0; i < 5; i++)
			{
				uint citizen = instance.m_units.m_buffer[(int)((UIntPtr)num)].GetCitizen(i);
				if (citizen != 0u)
				{
					ushort instance2 = instance.m_citizens.m_buffer[(int)((UIntPtr)citizen)].m_instance;
					if (instance2 != 0)
					{
						CitizenInfo info = instance.m_instances.m_buffer[(int)instance2].Info;
						if (info.m_class.m_service == this.m_info.m_class.m_service)
						{
							if ((instance.m_instances.m_buffer[(int)instance2].m_flags & CitizenInstance.Flags.EnteringVehicle) == CitizenInstance.Flags.None)
							{
								info.m_citizenAI.SetTarget(instance2, ref instance.m_instances.m_buffer[(int)instance2], 0);
							}
							result = false;
						}
					}
				}
			}
			num = nextUnit;
			if (++num2 > 524288)
			{
				CODebugBase<LogChannel>.Error(LogChannel.Core, "Invalid list detected!\n" + Environment.StackTrace);
				break;
			}
		}
		return result;
	}

	private void CreateWorker(ushort vehicleID, ref Vehicle data, Citizen.AgePhase agePhase, bool animal)
	{
		SimulationManager instance = Singleton<SimulationManager>.get_instance();
		CitizenManager instance2 = Singleton<CitizenManager>.get_instance();
		CitizenInfo groupCitizenInfo = instance2.GetGroupCitizenInfo(ref instance.m_randomizer, this.m_info.m_class.m_service, Citizen.Gender.Female, Citizen.SubCulture.Generic, agePhase);
		if (groupCitizenInfo != null)
		{
			int family = instance.m_randomizer.Int32(256u);
			uint num = 0u;
			if (instance2.CreateCitizen(out num, 90, family, ref instance.m_randomizer, groupCitizenInfo.m_gender))
			{
				ushort num2;
				if (instance2.CreateCitizenInstance(out num2, ref instance.m_randomizer, groupCitizenInfo, num))
				{
					if (!animal)
					{
						CitizenInstance[] expr_91_cp_0 = instance2.m_instances.m_buffer;
						ushort expr_91_cp_1 = num2;
						expr_91_cp_0[(int)expr_91_cp_1].m_flags = (expr_91_cp_0[(int)expr_91_cp_1].m_flags | CitizenInstance.Flags.CannotUseTaxi);
					}
					Vector3 randomDoorPosition = data.GetRandomDoorPosition(ref instance.m_randomizer, VehicleInfo.DoorType.Exit);
					groupCitizenInfo.m_citizenAI.SetCurrentVehicle(num2, ref instance2.m_instances.m_buffer[(int)num2], 0, 0u, randomDoorPosition);
					groupCitizenInfo.m_citizenAI.SetTarget(num2, ref instance2.m_instances.m_buffer[(int)num2], data.m_targetBuilding);
					instance2.m_citizens.m_buffer[(int)((UIntPtr)num)].SetVehicle(num, vehicleID, 0u);
				}
				else
				{
					instance2.ReleaseCitizen(num);
				}
			}
		}
	}

	private bool CheckBuilding(ushort vehicleID, ref Vehicle data, ushort buildingID, ref Building buildingData)
	{
		if ((buildingData.m_flags & Building.Flags.Collapsed) == Building.Flags.None)
		{
			return true;
		}
		int num = (int)buildingData.m_levelUpProgress;
		if (num != 255)
		{
			DistrictManager instance = Singleton<DistrictManager>.get_instance();
			Vector3 position = buildingData.m_position;
			byte district = instance.GetDistrict(position);
			DistrictPolicies.CityPlanning cityPlanningPolicies = instance.m_districts.m_buffer[(int)district].m_cityPlanningPolicies;
			int num2 = this.m_efficiency;
			bool flag = false;
			if ((cityPlanningPolicies & DistrictPolicies.CityPlanning.FastRecovery) != DistrictPolicies.CityPlanning.None)
			{
				District[] expr_7E_cp_0 = instance.m_districts.m_buffer;
				byte expr_7E_cp_1 = district;
				expr_7E_cp_0[(int)expr_7E_cp_1].m_cityPlanningPoliciesEffect = (expr_7E_cp_0[(int)expr_7E_cp_1].m_cityPlanningPoliciesEffect | DistrictPolicies.CityPlanning.FastRecovery);
				num2 *= 2;
				flag = true;
			}
			num = Mathf.Clamp(num + num2, 0, 255);
			buildingData.m_levelUpProgress = (byte)num;
			if (num == 255)
			{
				if (flag)
				{
					InstanceID empty = InstanceID.Empty;
					empty.Building = buildingID;
					InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(empty);
					DisasterHelpers.RemovePeople(group, buildingID, ref buildingData.m_citizenUnits, 100, ref Singleton<SimulationManager>.get_instance().m_randomizer);
				}
				else
				{
					int num3 = DisasterHelpers.SavePeople(buildingID, ref buildingData.m_citizenUnits);
					if (num3 != 0)
					{
						Singleton<MessageManager>.get_instance().TryCreateMessage("CHIRP_SURVIVORFOUND", null, Singleton<MessageManager>.get_instance().GetRandomResidentID());
					}
				}
				Notification.Problem problems = buildingData.m_problems;
				if (buildingData.Info.m_placementStyle == ItemClass.Placement.Automatic)
				{
					buildingData.m_problems = (Notification.Problem.StructureVisited | Notification.Problem.FatalProblem);
				}
				else
				{
					buildingData.m_problems = (Notification.Problem.StructureVisitedService | Notification.Problem.FatalProblem);
				}
				Singleton<BuildingManager>.get_instance().UpdateBuildingColors(buildingID);
				if (buildingData.m_problems != problems)
				{
					Singleton<BuildingManager>.get_instance().UpdateNotifications(buildingID, problems, buildingData.m_problems);
				}
			}
		}
		return num == 255;
	}

	private bool ArriveAtSource(ushort vehicleID, ref Vehicle data)
	{
		if (data.m_sourceBuilding == 0)
		{
			Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
			return true;
		}
		this.RemoveSource(vehicleID, ref data);
		Singleton<VehicleManager>.get_instance().ReleaseVehicle(vehicleID);
		return true;
	}

	public override bool ArriveAtDestination(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return false;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			return this.ArriveAtSource(vehicleID, ref vehicleData);
		}
		return this.ArriveAtTarget(vehicleID, ref vehicleData);
	}

	protected override float CalculateTargetSpeed(ushort vehicleID, ref Vehicle data, float speedLimit, float curve)
	{
		if ((data.m_flags & Vehicle.Flags.Emergency2) == (Vehicle.Flags)0)
		{
			return base.CalculateTargetSpeed(vehicleID, ref data, speedLimit, curve);
		}
		float num = 1000f / (1f + curve * 500f / this.m_info.m_turning) + 2f;
		float num2 = 16f * speedLimit;
		return Mathf.Min(Mathf.Min(num, num2), this.m_info.m_maxSpeed * 1.5f);
	}

	public override void UpdateBuildingTargetPositions(ushort vehicleID, ref Vehicle vehicleData, Vector3 refPos, ushort leaderID, ref Vehicle leaderData, ref int index, float minSqrDistance)
	{
		if ((leaderData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return;
		}
		if ((leaderData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0 && leaderData.m_sourceBuilding != 0)
		{
			BuildingManager instance = Singleton<BuildingManager>.get_instance();
			BuildingInfo info = instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding].Info;
			Randomizer randomizer;
			randomizer..ctor((int)vehicleID);
			Vector3 vector;
			Vector3 targetPos;
			info.m_buildingAI.CalculateUnspawnPosition(vehicleData.m_sourceBuilding, ref instance.m_buildings.m_buffer[(int)leaderData.m_sourceBuilding], ref randomizer, this.m_info, out vector, out targetPos);
			vehicleData.SetTargetPos(index++, base.CalculateTargetPoint(refPos, targetPos, minSqrDistance, 2f));
			return;
		}
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData)
	{
		if ((vehicleData.m_flags & Vehicle.Flags.WaitingTarget) != (Vehicle.Flags)0)
		{
			return true;
		}
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			if (vehicleData.m_sourceBuilding != 0)
			{
				Vector3 endPos = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_sourceBuilding].CalculateSidewalkPosition();
				return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos);
			}
		}
		else if (vehicleData.m_targetBuilding != 0)
		{
			Vector3 endPos2 = Singleton<BuildingManager>.get_instance().m_buildings.m_buffer[(int)vehicleData.m_targetBuilding].CalculateSidewalkPosition();
			return this.StartPathFind(vehicleID, ref vehicleData, vehicleData.m_targetPos3, endPos2);
		}
		return false;
	}

	protected override bool StartPathFind(ushort vehicleID, ref Vehicle vehicleData, Vector3 startPos, Vector3 endPos, bool startBothWays, bool endBothWays, bool undergroundTarget)
	{
		VehicleInfo info = this.m_info;
		bool allowUnderground = (vehicleData.m_flags & (Vehicle.Flags.Underground | Vehicle.Flags.Transition)) != (Vehicle.Flags)0;
		PathUnit.Position startPosA;
		PathUnit.Position startPosB;
		float num;
		float num2;
		PathUnit.Position endPosA;
		PathUnit.Position endPosB;
		float num3;
		float num4;
		if (PathManager.FindPathPosition(startPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, allowUnderground, false, 32f, out startPosA, out startPosB, out num, out num2) && PathManager.FindPathPosition(endPos, ItemClass.Service.Road, NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, undergroundTarget, false, 32f, out endPosA, out endPosB, out num3, out num4))
		{
			if (!startBothWays || num < 10f)
			{
				startPosB = default(PathUnit.Position);
			}
			if (!endBothWays || num3 < 10f)
			{
				endPosB = default(PathUnit.Position);
			}
			uint path;
			if (Singleton<PathManager>.get_instance().CreatePath(out path, ref Singleton<SimulationManager>.get_instance().m_randomizer, Singleton<SimulationManager>.get_instance().m_currentBuildIndex, startPosA, startPosB, endPosA, endPosB, default(PathUnit.Position), NetInfo.LaneType.Vehicle | NetInfo.LaneType.TransportVehicle, info.m_vehicleType, 20000f, this.IsHeavyVehicle(), this.IgnoreBlocked(vehicleID, ref vehicleData), false, false, false, false, this.CombustionEngine()))
			{
				if (vehicleData.m_path != 0u)
				{
					Singleton<PathManager>.get_instance().ReleasePath(vehicleData.m_path);
				}
				vehicleData.m_path = path;
				vehicleData.m_flags |= Vehicle.Flags.WaitingPath;
				return true;
			}
		}
		else
		{
			this.PathfindFailure(vehicleID, ref vehicleData);
		}
		return false;
	}

	public override InstanceID GetTargetID(ushort vehicleID, ref Vehicle vehicleData)
	{
		InstanceID result = default(InstanceID);
		if ((vehicleData.m_flags & Vehicle.Flags.GoingBack) != (Vehicle.Flags)0)
		{
			result.Building = vehicleData.m_sourceBuilding;
		}
		else
		{
			result.Building = vehicleData.m_targetBuilding;
		}
		return result;
	}

	protected override bool IgnoreBlocked(ushort vehicleID, ref Vehicle vehicleData)
	{
		return (vehicleData.m_flags & (Vehicle.Flags.Emergency1 | Vehicle.Flags.Emergency2)) != (Vehicle.Flags)0;
	}

	public override int GetNoiseLevel()
	{
		return 6;
	}

	public int m_workerCount = 2;

	public int m_animalCount = 1;

	[CustomizableProperty("Efficiency")]
	public int m_efficiency = 5;
}
