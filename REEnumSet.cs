﻿using System;
using System.Reflection;
using ColossalFramework.UI;

public class REEnumSet : REPropertySet
{
	protected override void Initialize(object target, FieldInfo targetField, string labelText)
	{
		base.get_component().Disable();
		this.m_Label.set_text(labelText);
		Type fieldType = targetField.FieldType;
		this.m_DropDown.set_items(Enum.GetNames(fieldType));
		this.m_DropDown.set_selectedValue(Enum.GetName(fieldType, targetField.GetValue(target)));
		this.UpdateButton();
		base.get_component().Enable();
	}

	protected override bool Validate(object target, FieldInfo targetField)
	{
		return target != null && targetField != null && targetField.FieldType.IsEnum;
	}

	private void OnEnable()
	{
		if (this.m_DropDown != null)
		{
			this.m_DropDown.add_eventDropdownClose(new UIDropDown.PopupEventHandler(this.DropdownClose));
		}
	}

	private void OnDisable()
	{
		if (this.m_DropDown != null)
		{
			this.m_DropDown.add_eventDropdownClose(new UIDropDown.PopupEventHandler(this.DropdownClose));
		}
	}

	private void DropdownClose(UIDropDown dropdown, UIListBox popup, ref bool overridden)
	{
		base.SetValue(Enum.Parse(this.m_TargetField.FieldType, this.m_DropDown.get_selectedValue(), true));
		this.UpdateButton();
	}

	private void UpdateButton()
	{
		((UIButton)this.m_DropDown.get_triggerButton()).set_text(this.m_DropDown.get_selectedValue());
	}

	public UILabel m_Label;

	public UIDropDown m_DropDown;
}
