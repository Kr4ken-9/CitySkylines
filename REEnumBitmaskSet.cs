﻿using System;
using System.Collections;
using System.Reflection;
using ColossalFramework.UI;

public class REEnumBitmaskSet : REPropertySet
{
	protected override void Initialize(object target, FieldInfo targetField, string labelText)
	{
		base.get_component().Disable();
		this.m_Label.set_text(labelText);
		this.m_DropDown.Clear();
		Type fieldType = targetField.FieldType;
		int num = (int)targetField.GetValue(target);
		IEnumerator enumerator = Enum.GetValues(fieldType).GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				int num2 = (int)enumerator.Current;
				if (this.IsUniqueFlag(num2))
				{
					string name = Enum.GetName(fieldType, num2);
					this.m_DropDown.AddItem(name, (num & num2) != 0);
					this.m_DropDown.SetItemUserData(this.m_DropDown.GetIndex(name), num2);
				}
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		this.UpdateDropdownButton();
		base.get_component().Enable();
	}

	protected override bool Validate(object target, FieldInfo targetField)
	{
		return target != null && targetField != null && targetField.FieldType.IsEnum && targetField.GetCustomAttributes(typeof(BitMaskAttribute), true).Length > 0;
	}

	private void OnEnable()
	{
		if (this.m_DropDown != null)
		{
			this.m_DropDown.add_eventAfterDropdownClose(new UICheckboxDropDown.AfterPopupClosedHandler(this.DropdownClose));
		}
	}

	private void OnDisable()
	{
		if (this.m_DropDown != null)
		{
			this.m_DropDown.add_eventAfterDropdownClose(new UICheckboxDropDown.AfterPopupClosedHandler(this.DropdownClose));
		}
	}

	private void DropdownClose(UICheckboxDropDown checkboxdropdown)
	{
		base.SetValue(this.GetFlags());
		this.UpdateDropdownButton();
	}

	private bool IsUniqueFlag(int flag)
	{
		return flag != 0 && (flag & flag - 1) == 0;
	}

	private int GetFlags()
	{
		int num = 0;
		Type fieldType = this.m_TargetField.FieldType;
		int num2 = this.m_DropDown.get_items().Length;
		for (int i = 0; i < num2; i++)
		{
			if (this.m_DropDown.GetChecked(i))
			{
				num |= (int)this.m_DropDown.GetItemUserData(i);
			}
		}
		return num;
	}

	private void UpdateDropdownButton()
	{
		int flags = this.GetFlags();
		UIButton uIButton = (UIButton)this.m_DropDown.get_triggerButton();
		if (flags == 0)
		{
			uIButton.set_text("None");
		}
		else if (this.IsUniqueFlag(flags))
		{
			uIButton.set_text(Enum.GetName(this.m_TargetField.FieldType, flags));
		}
		else
		{
			uIButton.set_text("Mixed");
		}
	}

	public UILabel m_Label;

	public UICheckboxDropDown m_DropDown;
}
