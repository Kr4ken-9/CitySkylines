﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColossalFramework;
using ColossalFramework.Plugins;
using ICities;
using UnityEngine;

public class EffectsWrapper : IEffects
{
	public EffectsWrapper(EffectManager effectManager)
	{
		this.m_EffectManager = effectManager;
		this.GetImplementations();
		Singleton<PluginManager>.get_instance().add_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().add_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.m_BuiltinEffects = new Dictionary<string, EffectInfo>();
		this.m_BuiltinParticleMaterials = new Dictionary<string, Material>();
		this.m_BuiltinAudioClips = new Dictionary<string, AudioClip>();
	}

	public void Release()
	{
		Singleton<PluginManager>.get_instance().remove_eventPluginsChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		Singleton<PluginManager>.get_instance().remove_eventPluginsStateChanged(new PluginManager.PluginsChangedHandler(this.GetImplementations));
		this.OnEffectsExtensionsReleased();
		this.EmptyEffectCollection();
	}

	private void GetImplementations()
	{
	}

	private void OnEffectsExtensionsReleased()
	{
	}

	private void OnEffectsExtensionsCreated()
	{
	}

	public IManagers managers
	{
		get
		{
			return Singleton<SimulationManager>.get_instance().m_ManagersWrapper;
		}
	}

	public void InitEffectCollection()
	{
		this.m_BuiltinEffects = new Dictionary<string, EffectInfo>();
		this.m_BuiltinParticleMaterials = new Dictionary<string, Material>();
		this.m_BuiltinAudioClips = new Dictionary<string, AudioClip>();
		EffectInfo[] array = Resources.FindObjectsOfTypeAll<EffectInfo>();
		for (int i = 0; i < array.Length; i++)
		{
			EffectInfo effectInfo = array[i];
			this.m_BuiltinEffects[effectInfo.get_name()] = effectInfo;
			if (effectInfo.GetType() == typeof(ParticleEffect))
			{
				ParticleSystemRenderer component = effectInfo.GetComponent<ParticleSystemRenderer>();
				if (component != null)
				{
					Material sharedMaterial = component.get_sharedMaterial();
					if (sharedMaterial != null)
					{
						this.m_BuiltinParticleMaterials[sharedMaterial.get_name()] = sharedMaterial;
					}
				}
			}
			if (effectInfo.GetType() == typeof(SoundEffect))
			{
				AudioClip clip = ((SoundEffect)effectInfo).m_audioInfo.m_clip;
				this.m_BuiltinAudioClips[clip.get_name()] = clip;
			}
		}
	}

	private void EmptyEffectCollection()
	{
		this.m_BuiltinEffects = new Dictionary<string, EffectInfo>();
		this.m_BuiltinParticleMaterials = new Dictionary<string, Material>();
		this.m_BuiltinAudioClips = new Dictionary<string, AudioClip>();
	}

	public string[] GetEffectList()
	{
		return this.m_BuiltinEffects.Keys.ToArray<string>();
	}

	public string[] GetParticleList()
	{
		return this.m_BuiltinParticleMaterials.Keys.ToArray<string>();
	}

	public string[] GetAudioList()
	{
		return this.m_BuiltinAudioClips.Keys.ToArray<string>();
	}

	public object GetBuiltinEffect(string name)
	{
		EffectInfo result;
		if (this.m_BuiltinEffects.TryGetValue(name, out result))
		{
			return result;
		}
		return null;
	}

	public void PlayEffect(object effect, ref UserEffectParameters p)
	{
		EffectInfo effectInfo;
		try
		{
			effectInfo = (EffectInfo)effect;
		}
		catch
		{
			Debug.Log("Invalid effect object");
			return;
		}
		if (effectInfo.RequireRender())
		{
			this.RenderEffect(effectInfo, ref p);
		}
		if (effectInfo.RequirePlay())
		{
			this.PlayAudio(effectInfo, ref p);
		}
	}

	private void RenderEffect(EffectInfo info, ref UserEffectParameters p)
	{
		Vector3 position;
		position..ctor(p.positionX, p.positionY, p.positionZ);
		Vector3 velocity;
		velocity..ctor(p.velocityX, p.velocityY, p.velocityZ);
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position, Vector3.get_up(), 0f);
		InstanceID id = default(InstanceID);
		float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
		info.RenderEffect(id, area, velocity, p.acceleration, p.magnitude, 0f, simulationTimeDelta, Singleton<RenderManager>.get_instance().CurrentCameraInfo);
	}

	private void PlayAudio(EffectInfo info, ref UserEffectParameters p)
	{
		Vector3 position;
		position..ctor(p.positionX, p.positionY, p.positionZ);
		Vector3 velocity;
		velocity..ctor(p.velocityX, p.velocityY, p.velocityZ);
		EffectInfo.SpawnArea area = new EffectInfo.SpawnArea(position, Vector3.get_up(), 0f);
		info.PlayEffect(default(InstanceID), area, velocity, p.acceleration, p.magnitude, Singleton<AudioManager>.get_instance().CurrentListenerInfo, Singleton<AudioManager>.get_instance().DefaultGroup);
	}

	public bool DispatchEffect(object effect, ref UserEffectParameters p)
	{
		EffectInfo effect2;
		try
		{
			effect2 = (EffectInfo)effect;
		}
		catch
		{
			Debug.Log("Invalid effect object");
			return false;
		}
		Vector3 position;
		position..ctor(p.positionX, p.positionY, p.positionZ);
		Vector3 velocity;
		velocity..ctor(p.velocityX, p.velocityY, p.velocityZ);
		EffectInfo.SpawnArea spawnArea = new EffectInfo.SpawnArea(position, Vector3.get_up(), 1f);
		this.m_EffectManager.DispatchEffect(effect2, spawnArea, velocity, p.acceleration, p.magnitude, Singleton<AudioManager>.get_instance().DefaultGroup, 0u);
		return true;
	}

	public object CreateLightEffect(Light light, ref UserLightSettings settings)
	{
		LightEffect lightEffect = light.get_gameObject().AddComponent<LightEffect>();
		lightEffect.m_batchedLight = false;
		lightEffect.m_fadeStartDistance = settings.fadeStartDistance;
		lightEffect.m_fadeEndDistance = settings.fadeEndDistance;
		Vector2 offRange;
		offRange..ctor(settings.offMin, settings.offMax);
		lightEffect.m_offRange = offRange;
		lightEffect.m_spotLeaking = settings.spotLeaking;
		lightEffect.m_renderDuration = settings.renderDuration;
		lightEffect.InitializeEffect();
		return lightEffect;
	}

	public object CreateParticleEffect(string materialName, ParticleSystem particleSystem, ref UserParticleSettings settings)
	{
		Material material;
		if (materialName != string.Empty && this.m_BuiltinParticleMaterials.TryGetValue(materialName, out material))
		{
			ParticleSystemRenderer component = particleSystem.get_gameObject().GetComponent<ParticleSystemRenderer>();
			if (component == null)
			{
				particleSystem.get_gameObject().AddComponent<ParticleSystemRenderer>();
				component = particleSystem.get_gameObject().GetComponent<ParticleSystemRenderer>();
			}
			component.set_renderMode(0);
			component.set_material(material);
			component.set_alignment(0);
		}
		ParticleEffect particleEffect = particleSystem.get_gameObject().AddComponent<ParticleEffect>();
		particleEffect.m_maxVisibilityDistance = settings.maxVisibilityDistance;
		particleEffect.m_minLifeTime = settings.minLifetime;
		particleEffect.m_maxLifeTime = settings.maxLifetime;
		particleEffect.m_minStartSpeed = settings.minStartSpeed;
		particleEffect.m_maxStartSpeed = settings.maxStartSpeed;
		particleEffect.m_minSpawnAngle = settings.minSpawnAngle;
		particleEffect.m_maxSpawnAngle = settings.maxSpawnAngle;
		particleEffect.m_renderDuration = settings.renderDuration;
		particleEffect.InitializeEffect();
		return particleEffect;
	}

	public object CreateSoundEffect(string clipName, AudioClip userClip, ref UserAudioSettings settings)
	{
		AudioInfo audioInfo = new AudioInfo();
		audioInfo.m_clip = userClip;
		AudioClip clip;
		if (clipName != string.Empty && this.m_BuiltinAudioClips.TryGetValue(clipName, out clip))
		{
			audioInfo.m_clip = clip;
		}
		audioInfo.m_volume = settings.volume;
		audioInfo.m_pitch = settings.pitch;
		audioInfo.m_fadeLength = settings.fadeLength;
		audioInfo.m_loop = settings.loop;
		audioInfo.m_is3D = settings.is3D;
		audioInfo.m_randomTime = settings.randomTime;
		GameObject gameObject = new GameObject();
		SoundEffect soundEffect = gameObject.AddComponent<SoundEffect>();
		soundEffect.m_audioInfo = audioInfo;
		soundEffect.m_range = settings.range;
		soundEffect.InitializeEffect();
		return soundEffect;
	}

	private EffectManager m_EffectManager;

	public Dictionary<string, EffectInfo> m_BuiltinEffects;

	public Dictionary<string, Material> m_BuiltinParticleMaterials;

	public Dictionary<string, AudioClip> m_BuiltinAudioClips;
}
