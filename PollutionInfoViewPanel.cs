﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class PollutionInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_GroundPollutionBar = base.Find<UISlider>("GroundPollutionBar");
		this.m_GroundPollutionLabel = base.Find<UILabel>("GroundPollutionLabel");
		this.m_WaterPollutionBar = base.Find<UISlider>("WaterPollutionBar");
		this.m_WaterPollutionLabel = base.Find<UILabel>("WaterPollutionLabel");
	}

	public float groundPollution
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetGroundPollution();
			}
			return 0f;
		}
	}

	public float waterPollution
	{
		get
		{
			if (Singleton<DistrictManager>.get_exists())
			{
				return (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetWaterPollution();
			}
			return 0f;
		}
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("Gradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[9].m_activeColor);
					uITextureSprite.get_renderMaterial().SetFloat("_Step", 0.166666672f);
					uITextureSprite.get_renderMaterial().SetFloat("_Scalar", 1.2f);
					uITextureSprite.get_renderMaterial().SetFloat("_Offset", 0f);
					this.m_GroundPollutionBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
					this.m_WaterPollutionBar.Find<UITextureSprite>("Background").get_renderMaterial().CopyPropertiesFromMaterial(uITextureSprite.get_renderMaterial());
				}
			}
			this.m_initialized = true;
		}
		this.m_GroundPollutionBar.set_value(this.groundPollution);
		this.m_GroundPollutionLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_POLLUTION_GROUND"),
			" - ",
			this.m_GroundPollutionBar.get_value(),
			"%"
		}));
		this.m_WaterPollutionBar.set_value(this.waterPollution);
		this.m_WaterPollutionLabel.set_text(string.Concat(new object[]
		{
			Locale.Get("INFO_POLLUTION_WATER"),
			" - ",
			this.m_WaterPollutionBar.get_value(),
			"%"
		}));
	}

	private UISlider m_GroundPollutionBar;

	private UILabel m_GroundPollutionLabel;

	private UISlider m_WaterPollutionBar;

	private UILabel m_WaterPollutionLabel;

	private bool m_initialized;
}
