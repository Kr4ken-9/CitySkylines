﻿using System;
using ColossalFramework;
using ColossalFramework.Math;
using UnityEngine;

public class SinkholeAI : DisasterAI
{
	public override void RenderInstance(RenderManager.CameraInfo cameraInfo, ushort disasterID, ref DisasterData data, ref Vector3 shake)
	{
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			int num = (int)(instance.m_referenceFrameIndex - data.m_activationFrame);
			if (num <= 0)
			{
				return;
			}
			if ((long)num >= (long)((ulong)this.m_activeDuration))
			{
				return;
			}
			Vector3 vector = cameraInfo.m_camera.get_transform().InverseTransformPoint(data.m_targetPosition);
			vector.z *= 0.25f;
			float num2 = 0.2f / (1f + vector.get_magnitude() * 0.005f);
			float num3 = (float)num + instance.m_referenceTimer;
			num2 *= 0.5f - Mathf.Abs((this.m_activeDuration - num3) / this.m_activeDuration - 0.5f);
			Vector3 vector2;
			vector2.x = Mathf.Sin(num3 * 0.53f) + Mathf.Sin(num3 * 0.17f);
			vector2.y = Mathf.Sin(num3 * 0.31f) + Mathf.Sin(num3 * 0.73f);
			vector2.z = Mathf.Sin(num3 * 0.19f) + Mathf.Sin(num3 * 0.49f);
			shake += vector2 * num2;
			BuildingProperties properties = Singleton<BuildingManager>.get_instance().m_properties;
			if (properties != null)
			{
				EffectInfo collapseEffect = properties.m_collapseEffect;
				float num4 = this.m_holeWidth * ((float)data.m_intensity * 0.01f) + 16f;
				float num5 = this.m_holeDepth * ((float)data.m_intensity * 0.01f) + 16f;
				float simulationTimeDelta = Singleton<SimulationManager>.get_instance().m_simulationTimeDelta;
				float magnitude = 1f;
				InstanceID id = default(InstanceID);
				id.Disaster = disasterID;
				EffectInfo.SpawnArea area;
				if (num3 <= this.m_activeDuration * 0.5f)
				{
					float num6 = num3 * 2f / this.m_activeDuration;
					area = new EffectInfo.SpawnArea(data.m_targetPosition, Vector3.get_up(), num4 * 0.5f + 8f);
					magnitude = num6 * num6 * 2f;
				}
				else
				{
					float num7 = (num3 - this.m_activeDuration * 0.5f) * 2f / this.m_activeDuration;
					Vector3 targetPosition = data.m_targetPosition;
					targetPosition.y -= num5 * num7;
					float num8 = num4 * 0.5f + 8f;
					float num9 = num4 * 0.25f;
					float radius = num8 + (num9 - num8) * num7;
					area = new EffectInfo.SpawnArea(targetPosition, Vector3.get_up(), radius);
				}
				collapseEffect.RenderEffect(id, area, Vector3.get_zero(), 0f, magnitude, -1f, simulationTimeDelta, cameraInfo);
			}
		}
	}

	public override void PlayInstance(AudioManager.ListenerInfo listenerInfo, ushort disasterID, ref DisasterData data)
	{
		if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None && this.m_loopSound != null)
		{
			SimulationManager instance = Singleton<SimulationManager>.get_instance();
			int num = (int)(instance.m_referenceFrameIndex - data.m_activationFrame);
			if (num <= 0)
			{
				return;
			}
			if ((long)num >= (long)((ulong)this.m_activeDuration))
			{
				return;
			}
			float num2 = (float)num + instance.m_referenceTimer;
			float num3 = 0.5f - Mathf.Abs((this.m_activeDuration - num2) / this.m_activeDuration - 0.5f);
			InstanceID empty = InstanceID.Empty;
			empty.Disaster = disasterID;
			Vector3 targetPosition = data.m_targetPosition;
			float volume = Mathf.Sqrt(Mathf.Clamp01(num3 * 2f)) * (0.5f + (float)data.m_intensity * 0.005f);
			Singleton<AudioManager>.get_instance().EffectGroup.AddPlayer(listenerInfo, (int)empty.RawData, this.m_loopSound, targetPosition, Vector3.get_zero(), 5000f, volume, 1f);
		}
	}

	public override void UpdateHazardMap(ushort disasterID, ref DisasterData data, byte[] map)
	{
		if ((data.m_flags & DisasterData.Flags.Located) != DisasterData.Flags.None && (data.m_flags & (DisasterData.Flags.Emerging | DisasterData.Flags.Active)) != DisasterData.Flags.None)
		{
			Randomizer randomizer;
			randomizer..ctor(data.m_randomSeed);
			Vector3 vector = data.m_targetPosition;
			float num = 100f;
			Vector3 vector2;
			vector2.x = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector2.y = 0f;
			vector2.z = (float)randomizer.Int32(-1000, 1000) * 0.001f;
			vector += vector2 * num;
			float num2 = this.m_holeWidth * ((float)data.m_intensity * 0.01f) + 16f;
			float num3 = num2 * 0.5f;
			float num4 = num2 * 0.5f + num * 2f;
			float num5 = vector.x - num4;
			float num6 = vector.z - num4;
			float num7 = vector.x + num4;
			float num8 = vector.z + num4;
			int num9 = Mathf.Max((int)(num5 / 38.4f + 128f), 2);
			int num10 = Mathf.Max((int)(num6 / 38.4f + 128f), 2);
			int num11 = Mathf.Min((int)(num7 / 38.4f + 128f), 253);
			int num12 = Mathf.Min((int)(num8 / 38.4f + 128f), 253);
			Vector3 vector3;
			vector3.y = 0f;
			for (int i = num10; i <= num12; i++)
			{
				vector3.z = ((float)i - 128f + 0.5f) * 38.4f - vector.z;
				for (int j = num9; j <= num11; j++)
				{
					vector3.x = ((float)j - 128f + 0.5f) * 38.4f - vector.x;
					float magnitude = vector3.get_magnitude();
					if (magnitude < num4)
					{
						float num13 = Mathf.Min(1f, 1f - (magnitude - num3) / (num4 - num3));
						int num14 = i * 256 + j;
						map[num14] = (byte)(255 - (int)(255 - map[num14]) * (255 - Mathf.RoundToInt(num13 * 255f)) / 255);
					}
				}
			}
		}
	}

	public override void CreateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.CreateDisaster(disasterID, ref data);
	}

	public override void SimulationStep(ushort disasterID, ref DisasterData data)
	{
		base.SimulationStep(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.Emerging) != DisasterData.Flags.None)
		{
			if (data.m_activationFrame != 0u)
			{
				uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
				int num;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.EarthquakeCoverage, data.m_targetPosition, out num);
				num = Mathf.Min(num, 100);
				uint num2 = (uint)(num * 6437 / 100 + 1755);
				if (currentFrameIndex + num2 >= data.m_activationFrame)
				{
					Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, num != 0);
				}
			}
		}
		else if ((data.m_flags & DisasterData.Flags.Active) != DisasterData.Flags.None)
		{
			if ((data.m_flags & DisasterData.Flags.Located) == DisasterData.Flags.None)
			{
				int num3;
				Singleton<ImmaterialResourceManager>.get_instance().CheckLocalResource(ImmaterialResourceManager.Resource.EarthquakeCoverage, data.m_targetPosition, out num3);
				if (num3 != 0)
				{
					Singleton<DisasterManager>.get_instance().DetectDisaster(disasterID, true);
				}
			}
			uint currentFrameIndex2 = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (currentFrameIndex2 > data.m_activationFrame && currentFrameIndex2 <= data.m_activationFrame + 256u)
			{
				InstanceID id = default(InstanceID);
				id.Disaster = disasterID;
				InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
				float num4 = this.m_holeWidth * ((float)data.m_intensity * 0.01f) + 16f;
				float depth = this.m_holeDepth * ((float)data.m_intensity * 0.01f) + 16f;
				float num5 = num4 * 0.5f + 8f;
				DisasterHelpers.DestroyStuff((int)data.m_randomSeed, group, data.m_targetPosition, num5, num5, num5, num5, num5, num5, num5);
				Vector2 position = VectorUtils.XZ(data.m_targetPosition);
				DisasterHelpers.MakeCrater(position, num4 * 0.5f, depth, false);
				DisasterHelpers.BurnGround(position, num4, 0.7f);
			}
		}
	}

	protected override void StartDisaster(ushort disasterID, ref DisasterData data)
	{
		base.StartDisaster(disasterID, ref data);
		if ((data.m_flags & DisasterData.Flags.SelfTrigger) != DisasterData.Flags.None)
		{
			data.m_targetPosition.y = Singleton<TerrainManager>.get_instance().SampleDetailHeight(data.m_targetPosition);
			data.m_activationFrame = data.m_startFrame + this.m_emergingDuration;
			data.m_flags |= DisasterData.Flags.Significant;
			Singleton<DisasterManager>.get_instance().m_randomDisasterCooldown = 0;
		}
	}

	protected override void ActivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.ActivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().FollowDisaster(disasterID);
		InstanceID id = default(InstanceID);
		id.Disaster = disasterID;
		InstanceManager.Group group = Singleton<InstanceManager>.get_instance().GetGroup(id);
		float num = this.m_holeWidth * ((float)data.m_intensity * 0.01f) + 16f;
		float num2 = num * 0.5f + 8f;
		DisasterHelpers.DestroyStuff((int)data.m_randomSeed, group, data.m_targetPosition, 0f, num2, num2, num2, num2, num2, num2);
	}

	protected override void DeactivateDisaster(ushort disasterID, ref DisasterData data)
	{
		base.DeactivateDisaster(disasterID, ref data);
		Singleton<DisasterManager>.get_instance().UnDetectDisaster(disasterID);
	}

	protected override bool IsStillEmerging(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillEmerging(disasterID, ref data))
		{
			return true;
		}
		if (data.m_activationFrame != 0u)
		{
			uint currentFrameIndex = Singleton<SimulationManager>.get_instance().m_currentFrameIndex;
			if (currentFrameIndex >= data.m_activationFrame)
			{
				return false;
			}
		}
		return true;
	}

	protected override bool IsStillActive(ushort disasterID, ref DisasterData data)
	{
		if (base.IsStillActive(disasterID, ref data))
		{
			return true;
		}
		uint num = Singleton<SimulationManager>.get_instance().m_currentFrameIndex - data.m_activationFrame;
		return num < this.m_activeDuration;
	}

	protected override float GetMinimumEdgeDistance()
	{
		float num = this.m_holeWidth + 16f;
		float num2 = num * 0.5f + 8f;
		return num2 + 150f;
	}

	public override bool CanSelfTrigger()
	{
		return true;
	}

	public override bool GetPosition(ushort disasterID, ref DisasterData data, out Vector3 position, out Quaternion rotation, out Vector3 size)
	{
		position = data.m_targetPosition;
		rotation = Quaternion.get_identity();
		float num = this.m_holeWidth * ((float)data.m_intensity * 0.01f) + 16f;
		size..ctor(num * 2f, num * 2f, num * 2f);
		return true;
	}

	public override bool GetHazardSubMode(out InfoManager.SubInfoMode subMode)
	{
		subMode = InfoManager.SubInfoMode.PipeWater;
		return true;
	}

	public float m_holeWidth = 50f;

	public float m_holeDepth = 50f;

	public uint m_emergingDuration = 4096u;

	public uint m_activeDuration = 512u;

	public AudioInfo m_loopSound;
}
