﻿using System;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;

public class LandValueInfoViewPanel : InfoViewPanel
{
	protected override void Start()
	{
		base.Start();
		this.m_AvgLandValue = base.Find<UILabel>("AvgLandValue");
	}

	protected override void UpdatePanel()
	{
		if (!this.m_initialized)
		{
			if (Singleton<LoadingManager>.get_exists())
			{
				if (!Singleton<LoadingManager>.get_instance().m_loadingComplete)
				{
					return;
				}
				UITextureSprite uITextureSprite = base.Find<UITextureSprite>("Gradient");
				if (Singleton<InfoManager>.get_exists())
				{
					uITextureSprite.get_renderMaterial().SetColor("_ColorA", Singleton<InfoManager>.get_instance().m_properties.m_neutralColor);
					uITextureSprite.get_renderMaterial().SetColor("_ColorB", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[11].m_activeColorB);
					uITextureSprite.get_renderMaterial().SetColor("_ColorC", Singleton<InfoManager>.get_instance().m_properties.m_modeProperties[11].m_activeColor);
					uITextureSprite.get_renderMaterial().SetFloat("_Step", 0.222222224f);
					uITextureSprite.get_renderMaterial().SetFloat("_Scalar", 1.1f);
					uITextureSprite.get_renderMaterial().SetFloat("_Offset", 0f);
				}
			}
			this.m_initialized = true;
		}
		float num = 0f;
		if (Singleton<DistrictManager>.get_exists())
		{
			num = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[0].GetLandValue();
		}
		this.m_AvgLandValue.set_text(StringUtils.SafeFormat(Locale.Get("INFO_LANDVALUE_AVERAGE"), new object[]
		{
			num,
			Locale.Get("MONEY_CURRENCY")
		}));
	}

	private UILabel m_AvgLandValue;

	private bool m_initialized;
}
