﻿using System;
using UnityEngine;

[Serializable]
public class BoneTransformHierarchy : ScriptableObject
{
	public void Init(Transform[] bones)
	{
		this.m_Bones = new BoneTransformHierarchy.BoneData[bones.Length];
		for (int i = 0; i < bones.Length; i++)
		{
			this.m_Bones[i] = ScriptableObject.CreateInstance<BoneTransformHierarchy.BoneData>();
			this.m_Bones[i].m_Name = bones[i].get_name();
			this.m_Bones[i].m_Position = bones[i].get_transform().get_localPosition();
			this.m_Bones[i].m_Rotation = bones[i].get_transform().get_localRotation();
			this.m_Bones[i].m_Scale = bones[i].get_transform().get_localScale();
			this.m_Bones[i].m_ParentIndex = -1;
			for (int j = 0; j < bones.Length; j++)
			{
				if (bones[i].get_parent() == bones[j])
				{
					this.m_Bones[i].m_ParentIndex = j;
					break;
				}
			}
		}
	}

	public void ExtractData(GameObject target)
	{
		Transform[] array = new Transform[this.m_Bones.Length];
		for (int i = 0; i < this.m_Bones.Length; i++)
		{
			array[i] = new GameObject().get_transform();
			array[i].set_name(this.m_Bones[i].m_Name);
		}
		for (int j = 0; j < this.m_Bones.Length; j++)
		{
			if (this.m_Bones[j].m_ParentIndex == -1)
			{
				array[j].set_parent(target.get_transform());
			}
			else
			{
				array[j].set_parent(array[this.m_Bones[j].m_ParentIndex]);
			}
		}
		for (int k = 0; k < this.m_Bones.Length; k++)
		{
			array[k].get_transform().set_localPosition(this.m_Bones[k].m_Position);
			array[k].get_transform().set_localRotation(this.m_Bones[k].m_Rotation);
			array[k].get_transform().set_localScale(this.m_Bones[k].m_Scale);
		}
		SkinnedMeshRenderer componentInChildren = target.GetComponentInChildren<SkinnedMeshRenderer>();
		if (componentInChildren != null)
		{
			componentInChildren.set_bones(array);
		}
	}

	public BoneTransformHierarchy.BoneData[] m_Bones;

	[Serializable]
	public class BoneData : ScriptableObject
	{
		public string m_Name;

		public Vector3 m_Position;

		public Quaternion m_Rotation;

		public Vector3 m_Scale;

		public int m_ParentIndex;
	}
}
