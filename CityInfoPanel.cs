﻿using System;
using System.Collections;
using System.Diagnostics;
using ColossalFramework;
using ColossalFramework.Globalization;
using ColossalFramework.UI;
using UnityEngine;

public sealed class CityInfoPanel : ToolsModifierControl
{
	public static CityInfoPanel instance
	{
		get
		{
			return CityInfoPanel.sInstance;
		}
	}

	public bool isVisible
	{
		get
		{
			return base.get_component().get_isVisible();
		}
	}

	private void Awake()
	{
		CityInfoPanel.sInstance = this;
		this.m_CityName = base.Find<UITextField>("CityName");
		this.m_CityName.add_eventTextSubmitted(new PropertyChangedEventHandler<string>(this.OnRename));
		this.m_PoliciesButton = base.Find<UIButton>("PoliciesButton");
		this.m_PoliciesList = base.Find<UIPanel>("PoliciesList");
		this.m_HappinessIcon = base.Find<UISprite>("Happiness");
		this.m_AverageLandValue = base.Find<UILabel>("AverageLandValue");
		this.m_ZoneChart = base.Find<UIRadialChart>("ZoneChart");
		this.m_AgeChart = base.Find<UIRadialChart>("AgeChart");
		UIRadialChart.SliceSettings arg_BA_0 = this.m_AgeChart.GetSlice(0);
		Color32 color = this.m_ChildColor;
		this.m_AgeChart.GetSlice(0).set_outterColor(color);
		arg_BA_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_E5_0 = this.m_AgeChart.GetSlice(1);
		color = this.m_TeenColor;
		this.m_AgeChart.GetSlice(1).set_outterColor(color);
		arg_E5_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_110_0 = this.m_AgeChart.GetSlice(2);
		color = this.m_YoungColor;
		this.m_AgeChart.GetSlice(2).set_outterColor(color);
		arg_110_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_13B_0 = this.m_AgeChart.GetSlice(3);
		color = this.m_AdultColor;
		this.m_AgeChart.GetSlice(3).set_outterColor(color);
		arg_13B_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_166_0 = this.m_AgeChart.GetSlice(4);
		color = this.m_SeniorColor;
		this.m_AgeChart.GetSlice(4).set_outterColor(color);
		arg_166_0.set_innerColor(color);
		this.m_ChildLegend = base.Find<UILabel>("ChildAmount");
		this.m_TeenLegend = base.Find<UILabel>("TeenAmount");
		this.m_YoungLegend = base.Find<UILabel>("YoungAmount");
		this.m_AdultLegend = base.Find<UILabel>("AdultAmount");
		this.m_SeniorLegend = base.Find<UILabel>("SeniorAmount");
		this.m_PopulationAmount = base.Find<UILabel>("PopulationAmount");
		this.m_HouseholdsAmount = base.Find<UILabel>("HouseholdsAmount");
		this.m_WorkersAmount = base.Find<UILabel>("WorkersAmount");
		this.m_TouristsAmount = base.Find<UILabel>("TouristsAmount");
		this.m_NoResidents = base.Find<UILabel>("NoResidents");
		this.m_NoResidents.Hide();
		this.m_ChildLegend.set_color(this.m_ChildColor);
		this.m_TeenLegend.set_color(this.m_TeenColor);
		this.m_YoungLegend.set_color(this.m_YoungColor);
		this.m_AdultLegend.set_color(this.m_AdultColor);
		this.m_SeniorLegend.set_color(this.m_SeniorColor);
		this.m_ResidentialLevelProgress = base.Find<UIPanel>("ResidentialLevelProgress");
		for (int i = 0; i < 5; i++)
		{
			this.m_ResidentialLevels[i] = this.m_ResidentialLevelProgress.Find<UIProgressBar>("Level" + (i + 1).ToString() + "Bar");
		}
		this.m_CommercialLevelProgress = base.Find<UIPanel>("CommercialLevelProgress");
		for (int j = 0; j < 5; j++)
		{
			this.m_CommercialLevels[j] = this.m_CommercialLevelProgress.Find<UIProgressBar>("Level" + (j + 1).ToString() + "Bar");
		}
		this.m_IndustrialLevelProgress = base.Find<UIPanel>("IndustrialLevelProgress");
		for (int k = 0; k < 5; k++)
		{
			this.m_IndustrialLevels[k] = this.m_IndustrialLevelProgress.Find<UIProgressBar>("Level" + (k + 1).ToString() + "Bar");
		}
		this.m_OfficeLevelProgress = base.Find<UIPanel>("OfficeLevelProgress");
		for (int l = 0; l < 5; l++)
		{
			this.m_OfficeLevels[l] = this.m_OfficeLevelProgress.Find<UIProgressBar>("Level" + (l + 1).ToString() + "Bar");
		}
		this.m_ResidentialNo = base.Find("ResidentialThumb").Find<UISprite>("NoNoNo");
		this.m_CommercialNo = base.Find("CommercialThumb").Find<UISprite>("NoNoNo");
		this.m_IndustrialNo = base.Find("IndustrialThumb").Find<UISprite>("NoNoNo");
		this.m_OfficeNo = base.Find("OfficeThumb").Find<UISprite>("NoNoNo");
	}

	private void Start()
	{
		this.CreatePoliciesTemplates();
	}

	private void OnEnable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded += new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void OnDisable()
	{
		if (Singleton<LoadingManager>.get_exists())
		{
			Singleton<LoadingManager>.get_instance().m_levelLoaded -= new LoadingManager.LevelLoadedHandler(this.OnLevelLoaded);
		}
	}

	private void OnLevelLoaded(SimulationManager.UpdateMode mode)
	{
		UIRadialChart.SliceSettings arg_3F_0 = this.m_ZoneChart.GetSlice(0);
		Color32 color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[2];
		this.m_ZoneChart.GetSlice(0).set_outterColor(color);
		arg_3F_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_83_0 = this.m_ZoneChart.GetSlice(1);
		color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[4];
		this.m_ZoneChart.GetSlice(1).set_outterColor(color);
		arg_83_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_C7_0 = this.m_ZoneChart.GetSlice(2);
		color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[6];
		this.m_ZoneChart.GetSlice(2).set_outterColor(color);
		arg_C7_0.set_innerColor(color);
		UIRadialChart.SliceSettings arg_10B_0 = this.m_ZoneChart.GetSlice(3);
		color = Singleton<ZoneManager>.get_instance().m_properties.m_zoneColors[7];
		this.m_ZoneChart.GetSlice(3).set_outterColor(color);
		arg_10B_0.set_innerColor(color);
	}

	public void SetRenameCallback(Action renameAction)
	{
		this.m_RenameAction = renameAction;
	}

	private void OnRename(UIComponent comp, string text)
	{
		base.StartCoroutine(this.RenameCoroutine(text));
	}

	[DebuggerHidden]
	private IEnumerator RenameCoroutine(string newName)
	{
		CityInfoPanel.<RenameCoroutine>c__Iterator0 <RenameCoroutine>c__Iterator = new CityInfoPanel.<RenameCoroutine>c__Iterator0();
		<RenameCoroutine>c__Iterator.newName = newName;
		<RenameCoroutine>c__Iterator.$this = this;
		return <RenameCoroutine>c__Iterator;
	}

	public string GetCityName()
	{
		string text = (!Singleton<SimulationManager>.get_exists()) ? null : Singleton<SimulationManager>.get_instance().m_metaData.m_CityName;
		if (text == null)
		{
			text = "Cities: Skylines";
		}
		return text;
	}

	[DebuggerHidden]
	public IEnumerator SetCityName(string name)
	{
		CityInfoPanel.<SetCityName>c__Iterator1 <SetCityName>c__Iterator = new CityInfoPanel.<SetCityName>c__Iterator1();
		<SetCityName>c__Iterator.name = name;
		return <SetCityName>c__Iterator;
	}

	public void OnCloseButton()
	{
		UIView.get_library().Hide(base.GetType().Name);
	}

	public void OnPoliciesClick()
	{
		if (ToolsModifierControl.GetTool<DistrictTool>() != ToolsModifierControl.GetCurrentTool<DistrictTool>())
		{
			ToolsModifierControl.keepThisWorldInfoPanel = true;
		}
		ToolsModifierControl.mainToolbar.ShowPoliciesPanel(0);
	}

	private void OnVisibilityChanged(UIComponent comp, bool visible)
	{
		if (visible)
		{
			if (Singleton<SimulationManager>.get_exists() && Singleton<SimulationManager>.get_instance().m_metaData != null)
			{
				this.m_CityName.set_text(this.GetCityName());
			}
			base.get_component().Focus();
		}
	}

	private static int GetValue(int value, int total)
	{
		float num = (float)value / (float)total;
		return Mathf.Clamp(Mathf.FloorToInt(num * 100f), 0, 100);
	}

	private void UpdateLevel(UIPanel panel, UIProgressBar[] bars, ItemClass.Zone type, float val, bool exists)
	{
		float width = panel.get_width();
		int maxLevel = ZonedBuildingWorldInfoPanel.GetMaxLevel(type);
		float width2 = width / (float)maxLevel;
		for (int i = 0; i < 5; i++)
		{
			if (i < maxLevel)
			{
				bars[i].set_width(width2);
				bars[i].Show();
				if (exists)
				{
					if (Mathf.FloorToInt(val) >= i)
					{
						bars[i].set_progressColor(this.m_LevelCompleteColor);
						bars[i].set_value(1f);
					}
					else if (Mathf.FloorToInt(val) + 1 == i)
					{
						bars[i].set_progressColor(this.m_LevelProgressColor);
						bars[i].set_value(Mathf.Repeat(val, 1f));
					}
					else
					{
						bars[i].set_value(0f);
					}
				}
				else
				{
					bars[i].set_value(0f);
				}
			}
			else
			{
				bars[i].Hide();
			}
		}
	}

	private void CreatePoliciesTemplates()
	{
		UITemplateManager.ClearInstances(CityInfoPanel.kPolicyEntryTemplate);
		for (int i = 0; i < CityInfoPanel.kPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(CityInfoPanel.kPolicies[i].get_enumValue()))
			{
				if (CityInfoPanel.kPolicies[i].get_enumValue() >> 5 != DistrictPolicies.Policies.Forest)
				{
					UILabel uILabel = this.m_PoliciesList.AttachUIComponent(UITemplateManager.GetAsGameObject(CityInfoPanel.kPolicyEntryTemplate)) as UILabel;
					uILabel.Find<UISprite>("Sprite").set_spriteName("IconPolicy" + CityInfoPanel.kPolicies[i].get_enumName());
					uILabel.set_name(CityInfoPanel.kPolicies[i].get_enumName());
					uILabel.set_tooltip(Locale.Get("POLICIES", CityInfoPanel.kPolicies[i].get_enumName()));
					uILabel.Hide();
				}
			}
		}
	}

	private void UpdatePolicies()
	{
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < CityInfoPanel.kPolicies.Length; i++)
		{
			if (Singleton<DistrictManager>.get_instance().IsPolicyLoaded(CityInfoPanel.kPolicies[i].get_enumValue()))
			{
				if (CityInfoPanel.kPolicies[i].get_enumValue() >> 5 != DistrictPolicies.Policies.Forest)
				{
					this.m_PoliciesList.get_components()[num].set_isVisible(Singleton<DistrictManager>.get_instance().IsCityPolicySet(CityInfoPanel.kPolicies[i].get_enumValue()));
					if (this.m_PoliciesList.get_components()[num].get_isVisible())
					{
						num2++;
					}
					num++;
				}
			}
		}
		this.m_PoliciesList.set_opacity((num2 <= 0) ? 0f : 1f);
		base.get_component().FitChildrenVertically();
		UIComponent expr_E1 = base.get_component();
		expr_E1.set_height(expr_E1.get_height() + 30f);
	}

	private void LateUpdate()
	{
		if (!base.get_component().get_isVisible())
		{
			return;
		}
		if (Singleton<DistrictManager>.get_exists())
		{
			int num = 0;
			string happinessString = ToolsModifierControl.GetHappinessString(Citizen.GetHappinessLevel((int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_finalHappiness));
			this.m_HappinessIcon.set_spriteName(happinessString);
			int finalCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_childData.m_finalCount;
			int finalCount2 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_teenData.m_finalCount;
			int finalCount3 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_youngData.m_finalCount;
			int finalCount4 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_adultData.m_finalCount;
			int finalCount5 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_seniorData.m_finalCount;
			this.m_ChildLegend.set_text(finalCount.ToString());
			this.m_TeenLegend.set_text(finalCount2.ToString());
			this.m_YoungLegend.set_text(finalCount3.ToString());
			this.m_AdultLegend.set_text(finalCount4.ToString());
			this.m_SeniorLegend.set_text(finalCount5.ToString());
			int finalCount6 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_populationData.m_finalCount;
			int finalAliveCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_residentialData.m_finalAliveCount;
			int finalHomeOrWorkCount = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_residentialData.m_finalHomeOrWorkCount;
			int num2 = (int)(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_commercialData.m_finalAliveCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_industrialData.m_finalAliveCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_officeData.m_finalAliveCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_playerData.m_finalAliveCount);
			int num3 = (int)(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_commercialData.m_finalHomeOrWorkCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_industrialData.m_finalHomeOrWorkCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_officeData.m_finalHomeOrWorkCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_playerData.m_finalHomeOrWorkCount);
			int num4 = (int)(Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_tourist1Data.m_averageCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_tourist2Data.m_averageCount + Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_tourist3Data.m_averageCount);
			this.m_PopulationAmount.set_text(finalCount6.ToString());
			this.m_HouseholdsAmount.set_text(LocaleFormatter.FormatGeneric("DISTRICT_COUNTFORMAT", new object[]
			{
				finalAliveCount,
				finalHomeOrWorkCount
			}));
			this.m_WorkersAmount.set_text(LocaleFormatter.FormatGeneric("DISTRICT_COUNTFORMAT", new object[]
			{
				num2,
				num3
			}));
			this.m_TouristsAmount.set_text(num4.ToString());
			int value = CityInfoPanel.GetValue(finalCount, finalCount6);
			int value2 = CityInfoPanel.GetValue(finalCount2, finalCount6);
			int value3 = CityInfoPanel.GetValue(finalCount3, finalCount6);
			int num5 = CityInfoPanel.GetValue(finalCount4, finalCount6);
			int value4 = CityInfoPanel.GetValue(finalCount5, finalCount6);
			int num6 = value + value2 + value3 + num5 + value4;
			if (num6 != 0 && num6 != 100)
			{
				num5 = 100 - (value + value2 + value3 + value4);
			}
			this.m_AgeChart.SetValues(new int[]
			{
				value,
				value2,
				value3,
				num5,
				value4
			});
			int finalHomeOrWorkCount2 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_residentialData.m_finalHomeOrWorkCount;
			int finalHomeOrWorkCount3 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_commercialData.m_finalHomeOrWorkCount;
			int finalHomeOrWorkCount4 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_industrialData.m_finalHomeOrWorkCount;
			int finalHomeOrWorkCount5 = (int)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_officeData.m_finalHomeOrWorkCount;
			int total = finalHomeOrWorkCount2 + finalHomeOrWorkCount3 + finalHomeOrWorkCount4 + finalHomeOrWorkCount5;
			int num7 = CityInfoPanel.GetValue(finalHomeOrWorkCount2, total);
			int num8 = CityInfoPanel.GetValue(finalHomeOrWorkCount3, total);
			int num9 = CityInfoPanel.GetValue(finalHomeOrWorkCount4, total);
			int num10 = CityInfoPanel.GetValue(finalHomeOrWorkCount5, total);
			int num11 = num7 + num8 + num9 + num10;
			if (num11 != 0 && num11 != 100)
			{
				if (num7 > num8 && num7 > num9 && num7 > num10)
				{
					num7 = 100 - (num8 + num9 + num10);
				}
				else if (num8 > num7 && num8 > num9 && num8 > num10)
				{
					num8 = 100 - (num7 + num9 + num10);
				}
				else if (num9 > num8 && num9 > num7 && num9 > num10)
				{
					num9 = 100 - (num8 + num7 + num10);
				}
				else if (num10 > num8 && num10 > num9 && num10 > num7)
				{
					num10 = 100 - (num8 + num9 + num7);
				}
			}
			this.m_ZoneChart.SetValues(new int[]
			{
				num7,
				num8,
				num9,
				num10
			});
			bool flag = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_residentialData.m_finalHomeOrWorkCount != 0u;
			this.m_NoResidents.set_isVisible(!flag);
			this.m_AgeChart.set_isVisible(flag);
			this.m_ResidentialNo.set_isVisible(!flag);
			bool flag2 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_commercialData.m_finalHomeOrWorkCount != 0u;
			this.m_CommercialNo.set_isVisible(!flag2);
			bool flag3 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_industrialData.m_finalHomeOrWorkCount != 0u;
			this.m_IndustrialNo.set_isVisible(!flag3);
			bool flag4 = Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_officeData.m_finalHomeOrWorkCount != 0u;
			this.m_OfficeNo.set_isVisible(!flag4);
			float val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_residentialData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_ResidentialLevelProgress, this.m_ResidentialLevels, ItemClass.Zone.ResidentialLow, val, flag);
			val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_commercialData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_CommercialLevelProgress, this.m_CommercialLevels, ItemClass.Zone.CommercialLow, val, flag2);
			val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_industrialData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_IndustrialLevelProgress, this.m_IndustrialLevels, ItemClass.Zone.Industrial, val, flag3);
			val = (float)Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].m_officeData.m_finalLevel / 50f;
			this.UpdateLevel(this.m_OfficeLevelProgress, this.m_OfficeLevels, ItemClass.Zone.Office, val, flag4);
			this.m_AverageLandValue.set_text(StringUtils.SafeFormat(Locale.Get("DISTRICT_LANDVALUE_AVERAGE"), new object[]
			{
				Singleton<DistrictManager>.get_instance().m_districts.m_buffer[num].GetLandValue(),
				Locale.Get("MONEY_CURRENCY")
			}));
			this.m_PoliciesButton.set_isEnabled(ToolsModifierControl.IsUnlocked(UnlockManager.Feature.Policies));
			this.UpdatePolicies();
		}
	}

	private static CityInfoPanel sInstance;

	private static readonly string kPolicyEntryTemplate = "CityPolicyEntryTemplate";

	private static readonly PositionData<DistrictPolicies.Policies>[] kPolicies = Utils.GetOrderedEnumData<DistrictPolicies.Policies>();

	private UITextField m_CityName;

	private Action m_RenameAction;

	public Color m_LevelProgressColor = Color.get_blue();

	public Color m_LevelCompleteColor = Color.get_green();

	public Color32 m_ChildColor;

	public Color32 m_TeenColor;

	public Color32 m_YoungColor;

	public Color32 m_AdultColor;

	public Color32 m_SeniorColor;

	private UIRadialChart m_AgeChart;

	private UILabel m_ChildLegend;

	private UILabel m_TeenLegend;

	private UILabel m_YoungLegend;

	private UILabel m_AdultLegend;

	private UILabel m_SeniorLegend;

	private UIRadialChart m_ZoneChart;

	private UISprite m_HappinessIcon;

	private UILabel m_PopulationAmount;

	private UILabel m_AverageLandValue;

	private UILabel m_HouseholdsAmount;

	private UILabel m_WorkersAmount;

	private UILabel m_TouristsAmount;

	private UILabel m_NoResidents;

	private UIPanel m_ResidentialLevelProgress;

	private UIProgressBar[] m_ResidentialLevels = new UIProgressBar[5];

	private UIPanel m_CommercialLevelProgress;

	private UIProgressBar[] m_CommercialLevels = new UIProgressBar[5];

	private UIPanel m_IndustrialLevelProgress;

	private UIProgressBar[] m_IndustrialLevels = new UIProgressBar[5];

	private UIPanel m_OfficeLevelProgress;

	private UIProgressBar[] m_OfficeLevels = new UIProgressBar[5];

	private UIButton m_PoliciesButton;

	private UIPanel m_PoliciesList;

	private UISprite m_ResidentialNo;

	private UISprite m_CommercialNo;

	private UISprite m_IndustrialNo;

	private UISprite m_OfficeNo;
}
