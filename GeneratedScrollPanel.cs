﻿using System;
using System.Collections.Generic;
using ColossalFramework;
using ColossalFramework.DataBinding;
using ColossalFramework.UI;
using UnityEngine;

public abstract class GeneratedScrollPanel : GeneratedPanel
{
	public string category
	{
		get
		{
			return this.m_Category;
		}
		set
		{
			this.m_Category = value;
		}
	}

	public IList<UIComponent> childComponents
	{
		get
		{
			return this.m_ScrollablePanel.get_components();
		}
	}

	public abstract ItemClass.Service service
	{
		get;
	}

	public virtual UIVerticalAlignment buttonsAlignment
	{
		get
		{
			return 1;
		}
	}

	private OptionPanelBase CreateOptionPanel(string templateName)
	{
		if (this.m_OptionsBar != null)
		{
			string str = base.GetType().ToString();
			UIComponent uIComponent = this.m_OptionsBar.Find(templateName + "(" + str + ")");
			if (uIComponent == null)
			{
				UIComponent uIComponent2 = this.m_OptionsBar.Find(templateName);
				if (uIComponent2 != null)
				{
					string name = uIComponent2.get_name();
					Vector2 vector = uIComponent2.get_relativePosition();
					UIComponent uIComponent3 = Object.Instantiate<UIComponent>(uIComponent2);
					uIComponent = this.m_OptionsBar.AttachUIComponent(uIComponent3.get_gameObject());
					uIComponent.set_cachedName(name + "(" + str + ")");
					uIComponent.set_relativePosition(vector);
					uIComponent.GetComponent<TutorialUITag>().m_ParentOverride = base.get_component();
					TutorialUITag[] componentsInChildren = uIComponent3.GetComponentsInChildren<TutorialUITag>();
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].tutorialTag = EnumExtensions.Name<ItemClass.Service>(this.service) + componentsInChildren[i].m_Tag;
					}
				}
			}
			if (uIComponent != null)
			{
				return uIComponent.GetComponent<OptionPanelBase>();
			}
		}
		return null;
	}

	private OptionPanelBase GetOptionPanel(string templateName)
	{
		if (this.m_OptionsBar != null)
		{
			UIComponent uIComponent = this.m_OptionsBar.Find(templateName);
			uIComponent.GetComponent<TutorialUITag>().m_ParentOverride = base.get_component();
			return uIComponent.GetComponent<OptionPanelBase>();
		}
		return null;
	}

	protected void HideAllOptionPanels()
	{
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void ShowPathsOptionPanel()
	{
		if (this.m_PathsOptionPanel == null)
		{
			this.m_PathsOptionPanel = this.CreateOptionPanel("PathsOptionPanel");
		}
		if (this.m_PathsOptionPanel != null)
		{
			this.m_PathsOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Path option panel was not found!");
		}
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HidePathsOptionPanel()
	{
		this.m_PathsOptionPanel.Hide();
	}

	protected void ShowRoadsOptionPanel()
	{
		if (this.m_RoadsOptionPanel == null)
		{
			this.m_RoadsOptionPanel = this.CreateOptionPanel("RoadsOptionPanel");
		}
		if (this.m_RoadsOptionPanel != null)
		{
			this.m_RoadsOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Roads option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideRoadsOptionPanel()
	{
		this.m_RoadsOptionPanel.Hide();
	}

	protected void ShowTracksOptionPanel()
	{
		if (this.m_TracksOptionPanel == null)
		{
			this.m_TracksOptionPanel = this.CreateOptionPanel("TracksOptionPanel");
		}
		if (this.m_TracksOptionPanel != null)
		{
			this.m_TracksOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Tracks option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideTracksOptionPanel()
	{
		this.m_TracksOptionPanel.Hide();
	}

	protected void ShowTunnelsOptionPanel()
	{
		if (this.m_TunnelsOptionPanel == null)
		{
			this.m_TunnelsOptionPanel = this.CreateOptionPanel("TunnelsOptionPanel");
		}
		if (this.m_TunnelsOptionPanel != null)
		{
			this.m_TunnelsOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Tunnels option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideTunnelsOptionPanel()
	{
		this.m_TunnelsOptionPanel.Hide();
	}

	protected void ShowCanalsOptionPanel()
	{
		if (this.m_CanalsOptionPanel == null)
		{
			this.m_CanalsOptionPanel = this.CreateOptionPanel("CanalsOptionPanel");
		}
		if (this.m_CanalsOptionPanel != null)
		{
			this.m_CanalsOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Canals option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideCanalsOptionPanel()
	{
		this.m_CanalsOptionPanel.Hide();
	}

	protected void ShowFloodwallsOptionPanel()
	{
		if (this.m_FloodWallsOptionPanel == null)
		{
			this.m_FloodWallsOptionPanel = this.CreateOptionPanel("FloodWallsOptionPanel");
		}
		if (this.m_FloodWallsOptionPanel != null)
		{
			this.m_FloodWallsOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Floodwalls option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideFloodwallsOptionPanel()
	{
		this.m_FloodWallsOptionPanel.Hide();
	}

	protected void ShowQuaysOptionPanel()
	{
		if (this.m_QuaysOptionPanel == null)
		{
			this.m_QuaysOptionPanel = this.CreateOptionPanel("QuaysOptionPanel");
		}
		if (this.m_QuaysOptionPanel != null)
		{
			this.m_QuaysOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Quays option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideQuaysOptionPanel()
	{
		this.m_QuaysOptionPanel.Hide();
	}

	protected void ShowPipesOptionPanel()
	{
		if (this.m_PipesOptionPanel == null)
		{
			this.m_PipesOptionPanel = this.CreateOptionPanel("PipesOptionPanel");
		}
		if (this.m_PipesOptionPanel != null)
		{
			this.m_PipesOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Pipes option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HidePipesOptionPanel()
	{
		this.m_PipesOptionPanel.Hide();
	}

	protected void ShowZoningOptionPanel()
	{
		if (this.m_ZoningOptionPanel == null)
		{
			this.m_ZoningOptionPanel = this.GetOptionPanel("ZoningOptionPanel");
		}
		if (this.m_ZoningOptionPanel != null)
		{
			this.m_ZoningOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Zoning option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideZoningOptionPanel()
	{
		this.m_ZoningOptionPanel.Hide();
	}

	protected void ShowDistrictOptionPanel()
	{
		if (this.m_DistrictOptionPanel == null)
		{
			this.m_DistrictOptionPanel = this.GetOptionPanel("DistrictOptionPanel");
		}
		if (this.m_DistrictOptionPanel != null)
		{
			this.m_DistrictOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("District option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideZoningOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideDistrictOptionPanel()
	{
		this.m_DistrictOptionPanel.Hide();
	}

	protected void ShowLandscapingOptionPanel()
	{
		if (this.m_LandscapingOptionPanel == null)
		{
			this.m_LandscapingOptionPanel = this.GetOptionPanel("LandscapingOptionPanel");
		}
		if (this.m_LandscapingOptionPanel != null)
		{
			this.m_LandscapingOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Landscaping option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideLandscapingOptionPanel()
	{
		this.m_LandscapingOptionPanel.Hide();
	}

	protected void ShowDisastersOptionPanel()
	{
		if (this.m_DisastersOptionPanel == null)
		{
			this.m_DisastersOptionPanel = this.GetOptionPanel("DisastersOptionPanel");
		}
		if (this.m_DisastersOptionPanel != null)
		{
			this.m_DisastersOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Disasters option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideCurvedPropsOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideDisastersOptionPanel()
	{
		this.m_DisastersOptionPanel.Hide();
	}

	protected void ShowCurvedPropsOptionPanel()
	{
		if (this.m_CurvedPropsOptionPanel == null)
		{
			this.m_CurvedPropsOptionPanel = this.GetOptionPanel("CurvedPropsOptionPanel");
		}
		if (this.m_CurvedPropsOptionPanel != null)
		{
			this.m_CurvedPropsOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Curved Props option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HidePowerLinesOptionPanel();
	}

	protected void HideCurvedPropsOptionPanel()
	{
		this.m_CurvedPropsOptionPanel.Hide();
	}

	protected void ShowPowerLinesOptionPanel()
	{
		if (this.m_PowerLinesOptionPanel == null)
		{
			this.m_PowerLinesOptionPanel = this.GetOptionPanel("PowerLinesOptionPanel");
		}
		if (this.m_PowerLinesOptionPanel != null)
		{
			this.m_PowerLinesOptionPanel.ShowPanel();
		}
		else
		{
			Debug.LogError("Power Lines option panel was not found!");
		}
		this.HidePathsOptionPanel();
		this.HideRoadsOptionPanel();
		this.HideTracksOptionPanel();
		this.HideTunnelsOptionPanel();
		this.HideCanalsOptionPanel();
		this.HideFloodwallsOptionPanel();
		this.HideQuaysOptionPanel();
		this.HidePipesOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideDisastersOptionPanel();
		this.HideZoningOptionPanel();
		this.HideDistrictOptionPanel();
		this.HideLandscapingOptionPanel();
		this.HideCurvedPropsOptionPanel();
	}

	protected void HidePowerLinesOptionPanel()
	{
		this.m_PowerLinesOptionPanel.Hide();
	}

	public int selectedIndex
	{
		get
		{
			return this.m_SelectedIndex;
		}
		set
		{
			if (value != this.m_SelectedIndex)
			{
				this.SelectByIndex(value);
			}
		}
	}

	public override void RefreshPanel()
	{
		this.CleanPanel();
		this.m_ObjectIndex = 0;
	}

	protected override void Awake()
	{
		base.Awake();
		base.get_component().Hide();
		this.m_ScrollablePanel = base.GetComponentInChildren<UIScrollablePanel>();
		this.m_ScrollablePanel.set_scrollWheelAmount(109);
		if (this.m_ScrollablePanel.get_horizontalScrollbar() != null)
		{
			this.m_ScrollablePanel.get_horizontalScrollbar().set_stepSize(1f);
			this.m_ScrollablePanel.get_horizontalScrollbar().set_incrementAmount(109f);
		}
		ToolsModifierControl.bulldozerButton.add_eventActiveStateIndexChanged(delegate(UIComponent c, int idx)
		{
			if (idx == 1)
			{
				this.m_LastSelectedIndex = this.selectedIndex;
				this.selectedIndex = -1;
			}
			else if (idx == 0)
			{
				this.selectedIndex = this.m_LastSelectedIndex;
				this.m_LastSelectedIndex = -1;
			}
		});
	}

	private void ResetTools()
	{
		BuildingTool tool = ToolsModifierControl.GetTool<BuildingTool>();
		if (tool != null)
		{
			tool.m_prefab = null;
		}
		NetTool tool2 = ToolsModifierControl.GetTool<NetTool>();
		if (tool2 != null)
		{
			tool2.Prefab = null;
		}
	}

	private void SelectByIndex(int value)
	{
		value = Mathf.Max(Mathf.Min(value, this.childComponents.Count - 1), -1);
		if (value == this.m_SelectedIndex)
		{
			return;
		}
		this.m_SelectedIndex = value;
		for (int i = 0; i < this.childComponents.Count; i++)
		{
			UIButton uIButton = this.childComponents[i] as UIButton;
			if (!(uIButton == null))
			{
				if (i == value)
				{
					uIButton.set_state(1);
				}
				else
				{
					uIButton.set_state(0);
				}
			}
		}
	}

	protected int ItemsGenericSort(PrefabInfo a, PrefabInfo b)
	{
		int num = a.m_isCustomContent.CompareTo(b.m_isCustomContent);
		if (num == 0)
		{
			num = a.m_UIPriority.CompareTo(b.m_UIPriority);
			if (num == 0 && a.m_isCustomContent && b.m_isCustomContent)
			{
				num = a.GetLocalizedTitle().CompareTo(b.GetLocalizedTitle());
			}
		}
		return num;
	}

	protected int ItemsTypeSort(PrefabInfo a, PrefabInfo b)
	{
		int num = a.m_isCustomContent.CompareTo(b.m_isCustomContent);
		if (num == 0)
		{
			if (a is BuildingInfo && b is NetInfo)
			{
				return 1;
			}
			if (a is BuildingInfo && b is TransportInfo)
			{
				return 1;
			}
			if (a is TransportInfo && b is BuildingInfo)
			{
				return -1;
			}
			if (a is TransportInfo && b is NetInfo)
			{
				return 1;
			}
			if (a is NetInfo && b is TransportInfo)
			{
				return -1;
			}
			if (a is NetInfo && b is BuildingInfo)
			{
				return -1;
			}
			num = a.m_UIPriority.CompareTo(b.m_UIPriority);
			if (num == 0 && a.m_isCustomContent && b.m_isCustomContent)
			{
				num = a.GetLocalizedTitle().CompareTo(b.GetLocalizedTitle());
			}
		}
		return num;
	}

	protected int ItemsTypeReverseSort(PrefabInfo a, PrefabInfo b)
	{
		int num = a.m_isCustomContent.CompareTo(b.m_isCustomContent);
		if (num == 0)
		{
			if (a is BuildingInfo && b is NetInfo)
			{
				return -1;
			}
			if (a is BuildingInfo && b is TransportInfo)
			{
				return -1;
			}
			if (a is TransportInfo && b is BuildingInfo)
			{
				return 1;
			}
			if (a is TransportInfo && b is NetInfo)
			{
				return 1;
			}
			if (a is NetInfo && b is TransportInfo)
			{
				return -1;
			}
			if (a is NetInfo && b is BuildingInfo)
			{
				return 1;
			}
			num = a.m_UIPriority.CompareTo(b.m_UIPriority);
			if (num == 0 && a.m_isCustomContent && b.m_isCustomContent)
			{
				num = a.GetLocalizedTitle().CompareTo(b.GetLocalizedTitle());
			}
		}
		return num;
	}

	protected void OnVisibilityChanged(UIComponent comp, bool isVisible)
	{
		if (base.get_component().get_isVisibleSelf())
		{
			if (isVisible)
			{
				this.OnShow();
				if (!GeneratedPanel.m_IsRefreshing && ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.CurrentTool == ToolsModifierControl.GetTool<BulldozeTool>())
				{
					this.selectedIndex = this.m_LastSelectedIndex;
				}
				if (this.selectedIndex >= this.childComponents.Count)
				{
					this.selectedIndex = this.childComponents.Count - 1;
				}
				if (this.selectedIndex >= 0 && this.childComponents.Count > 0)
				{
					if (!this.childComponents[this.selectedIndex].get_isEnabled())
					{
						this.selectedIndex = -1;
						this.ResetTools();
					}
					else
					{
						this.childComponents[this.selectedIndex].SimulateClick();
					}
				}
			}
			else
			{
				this.OnHide();
				if (!GeneratedPanel.m_IsRefreshing && ToolsModifierControl.toolController != null && ToolsModifierControl.toolController.CurrentTool != ToolsModifierControl.GetTool<BulldozeTool>())
				{
					ToolsModifierControl.SetTool<DefaultTool>();
				}
			}
		}
	}

	protected void OnMouseUp(UIComponent comp, UIMouseEventParameter p)
	{
		if (this.childComponents.Contains(p.get_source()))
		{
			p.get_source().Unfocus();
		}
	}

	protected virtual void OnShow()
	{
	}

	protected virtual void OnHide()
	{
	}

	protected virtual void OnButtonClicked(UIComponent comp)
	{
	}

	protected virtual void OnClick(UIComponent comp, UIMouseEventParameter p)
	{
		UIButton uIButton = p.get_source() as UIButton;
		if (uIButton != null && uIButton.get_parent() == this.m_ScrollablePanel)
		{
			int zOrder = uIButton.get_zOrder();
			this.m_LastSelectedIndex = zOrder;
			this.OnButtonClicked(uIButton);
			this.SelectByIndex(zOrder);
		}
	}

	private void ShowSelectedIndex()
	{
		if (this.selectedIndex >= 0 && this.selectedIndex <= this.childComponents.Count - 1)
		{
			UIButton uIButton = this.childComponents[this.selectedIndex] as UIButton;
			if (uIButton != null)
			{
				uIButton.set_state(1);
			}
		}
	}

	protected virtual UIButton SpawnEntry(string name, string tooltip, string thumbnail, UITextureAtlas atlas, UIComponent tooltipBox, bool enabled)
	{
		if (atlas == null)
		{
			atlas = this.m_ScrollablePanel.get_atlas();
		}
		if (string.IsNullOrEmpty(thumbnail) || atlas.get_Item(thumbnail) == null)
		{
			thumbnail = "ThumbnailBuildingDefault";
		}
		return this.CreateButton(name, tooltip, thumbnail, -1, atlas, tooltipBox, enabled);
	}

	protected UIButton CreateButton(string name, string tooltip, string baseIconName, int index)
	{
		return this.CreateButton(name, tooltip, baseIconName, index, null, null, true);
	}

	protected UIButton CreateButton(string name, string tooltip, string baseIconName, int index, UIComponent tooltipBox)
	{
		return this.CreateButton(name, tooltip, baseIconName, index, null, tooltipBox, true);
	}

	protected UIButton CreateButton(string name, string tooltip, string baseIconName, int index, UITextureAtlas atlas, UIComponent tooltipBox, bool enabled)
	{
		UIButton uIButton;
		if (this.m_ScrollablePanel.get_childCount() > this.m_ObjectIndex)
		{
			uIButton = (this.m_ScrollablePanel.get_components()[this.m_ObjectIndex] as UIButton);
		}
		else
		{
			GameObject asGameObject = UITemplateManager.GetAsGameObject(GeneratedScrollPanel.kPlaceableItemTemplate);
			uIButton = (this.m_ScrollablePanel.AttachUIComponent(asGameObject) as UIButton);
		}
		uIButton.get_gameObject().GetComponent<TutorialUITag>().tutorialTag = name;
		uIButton.set_text(string.Empty);
		uIButton.set_name(name);
		uIButton.set_tooltipAnchor(2);
		uIButton.set_tabStrip(true);
		uIButton.set_horizontalAlignment(1);
		uIButton.set_verticalAlignment(1);
		uIButton.set_pivot(1);
		if (atlas != null)
		{
			uIButton.set_atlas(atlas);
		}
		if (index != -1)
		{
			uIButton.set_zOrder(index);
		}
		uIButton.set_verticalAlignment(this.buttonsAlignment);
		uIButton.set_foregroundSpriteMode(2);
		uIButton.set_normalFgSprite(baseIconName);
		uIButton.set_focusedFgSprite(baseIconName + "Focused");
		uIButton.set_hoveredFgSprite(baseIconName + "Hovered");
		uIButton.set_pressedFgSprite(baseIconName + "Pressed");
		uIButton.set_disabledFgSprite(baseIconName + "Disabled");
		UIComponent uIComponent = (uIButton.get_childCount() <= 0) ? null : uIButton.get_components()[0];
		if (uIComponent != null)
		{
			uIComponent.set_isVisible(false);
		}
		uIButton.set_isEnabled(enabled);
		uIButton.set_tooltip(tooltip);
		uIButton.set_tooltipBox(tooltipBox);
		uIButton.set_group(base.get_component());
		this.m_ObjectIndex++;
		return uIButton;
	}

	private void Update()
	{
		this.ShowSelectedIndex();
		this.UpdateNoMoneyOrAlreadyPlacedNotification();
	}

	private void UpdateNoMoneyOrAlreadyPlacedNotification()
	{
		if (Singleton<EconomyManager>.get_exists() && this.m_ScrollablePanel.get_isVisible())
		{
			for (int i = 0; i < this.m_ScrollablePanel.get_childCount(); i++)
			{
				UIComponent uIComponent = this.m_ScrollablePanel.get_components()[i];
				PrefabInfo prefabInfo = uIComponent.get_objectUserData() as PrefabInfo;
				if (prefabInfo != null && uIComponent.get_childCount() > 0)
				{
					UISprite uISprite = uIComponent.get_components()[0] as UISprite;
					if (uISprite != null)
					{
						if (uIComponent.get_isEnabled())
						{
							if (!prefabInfo.CanBeBuilt())
							{
								uISprite.set_spriteName("ThumbnailBuildingAlreadyBuilt");
								uISprite.set_isVisible(true);
							}
							else
							{
								uISprite.set_spriteName("ThumbnailBuildingNoMoney");
								int constructionCost = prefabInfo.GetConstructionCost();
								uISprite.set_isVisible(Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Construction, constructionCost) != constructionCost);
							}
						}
						else
						{
							uISprite.set_isVisible(false);
						}
					}
				}
				LandscapingPanel.LandscapingInfo landscapingInfo = uIComponent.get_objectUserData() as LandscapingPanel.LandscapingInfo;
				if (landscapingInfo != null && uIComponent.get_childCount() > 0)
				{
					UISprite uISprite2 = uIComponent.get_components()[0] as UISprite;
					if (uISprite2 != null)
					{
						if (uIComponent.get_isEnabled())
						{
							uISprite2.set_spriteName("ThumbnailBuildingNoMoney");
							int dirtPrice = landscapingInfo.m_DirtPrice;
							uISprite2.set_isVisible(Singleton<EconomyManager>.get_instance().PeekResource(EconomyManager.Resource.Landscaping, dirtPrice) != dirtPrice);
						}
						else
						{
							uISprite2.set_isVisible(false);
						}
					}
				}
			}
		}
	}

	protected void CacheVisibility()
	{
		this.m_CachedIsVisible = base.get_component().get_isVisible();
	}

	protected virtual void OnHideOptionBars()
	{
	}

	protected virtual void Start()
	{
		this.CacheVisibility();
		base.get_component().add_eventVisibilityChanged(delegate(UIComponent sender, bool visible)
		{
			if (this.m_CachedIsVisible && !visible)
			{
				this.OnHideOptionBars();
				this.HideAllOptionPanels();
			}
			this.m_CachedIsVisible = visible;
		});
	}

	protected virtual bool IsPlacementRelevant(NetInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	protected virtual bool IsPlacementRelevant(BuildingInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	protected virtual bool IsPlacementRelevant(TransportInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_pathVisibility.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	protected virtual bool IsPlacementRelevant(TreeInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	protected virtual bool IsPlacementRelevant(PropInfo info)
	{
		bool flag = true;
		if (Singleton<ToolManager>.get_exists())
		{
			flag &= info.m_availableIn.IsFlagSet(Singleton<ToolManager>.get_instance().m_properties.m_mode);
		}
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	protected virtual bool IsPlacementRelevant(DisasterInfo info)
	{
		bool flag = info.m_disasterAI.CanSelfTrigger();
		return flag & info.m_placementStyle == ItemClass.Placement.Manual;
	}

	protected virtual bool IsCategoryValid(PrefabInfo info, bool ignore)
	{
		return ignore || string.IsNullOrEmpty(this.m_Category) || this.m_Category == info.category;
	}

	protected virtual bool IsCategoryValid(BuildingInfo info, bool ignore)
	{
		return this.IsCategoryValid(info, ignore);
	}

	protected virtual bool IsCategoryValid(NetInfo info, bool ignore)
	{
		return this.IsCategoryValid(info, ignore);
	}

	protected virtual bool IsCategoryValid(PropInfo info, bool ignore)
	{
		return this.IsCategoryValid(info, ignore);
	}

	protected virtual bool IsCategoryValid(TreeInfo info, bool ignore)
	{
		return this.IsCategoryValid(info, ignore);
	}

	protected virtual bool IsCategoryValid(TransportInfo info, bool ignore)
	{
		return this.IsCategoryValid(info, ignore);
	}

	protected virtual bool IsCategoryValid(DisasterInfo info, bool ignore)
	{
		return this.IsCategoryValid(info, ignore);
	}

	protected virtual bool IsServiceValid(PrefabInfo info)
	{
		return info.GetService() == this.service;
	}

	protected virtual bool IsServiceValid(BuildingInfo info)
	{
		ItemClass.Service service = info.GetService();
		return this.IsServiceValid(info) || (info.m_buildingAI.IsWonder() && service != ItemClass.Service.None);
	}

	protected virtual bool IsServiceValid(NetInfo info)
	{
		return this.IsServiceValid(info);
	}

	protected virtual bool IsServiceValid(PropInfo info)
	{
		return this.IsServiceValid(info);
	}

	protected virtual bool IsServiceValid(TreeInfo info)
	{
		return this.IsServiceValid(info);
	}

	protected virtual bool IsServiceValid(TransportInfo info)
	{
		return this.IsServiceValid(info);
	}

	protected virtual bool IsServiceValid(DisasterInfo info)
	{
		return this.IsServiceValid(info);
	}

	private bool FilterWonders(GeneratedScrollPanel.AssetFilter filter, BuildingInfo info)
	{
		if (filter.IsFlagSet((GeneratedScrollPanel.AssetFilter)18))
		{
			return true;
		}
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Wonder))
		{
			return info.m_buildingAI.IsWonder();
		}
		return filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Building) && !info.m_buildingAI.IsWonder();
	}

	private PoolList<PrefabInfo> CollectAssets(GeneratedScrollPanel.AssetFilter filter, Comparison<PrefabInfo> comparison, bool ignoreCategories)
	{
		PoolList<PrefabInfo> poolList = PoolList<PrefabInfo>.Obtain();
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Net))
		{
			uint num = 0u;
			while ((ulong)num < (ulong)((long)PrefabCollection<NetInfo>.LoadedCount()))
			{
				NetInfo loaded = PrefabCollection<NetInfo>.GetLoaded(num);
				if (loaded != null && this.IsServiceValid(loaded) && this.IsCategoryValid(loaded, ignoreCategories) && this.IsPlacementRelevant(loaded))
				{
					poolList.Add(loaded);
				}
				num += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Building) || filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Wonder))
		{
			uint num2 = 0u;
			while ((ulong)num2 < (ulong)((long)PrefabCollection<BuildingInfo>.LoadedCount()))
			{
				BuildingInfo loaded2 = PrefabCollection<BuildingInfo>.GetLoaded(num2);
				if (loaded2 != null && this.FilterWonders(filter, loaded2) && this.IsServiceValid(loaded2) && this.IsCategoryValid(loaded2, ignoreCategories) && this.IsPlacementRelevant(loaded2))
				{
					poolList.Add(loaded2);
				}
				num2 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Transport))
		{
			uint num3 = 0u;
			while ((ulong)num3 < (ulong)((long)PrefabCollection<TransportInfo>.LoadedCount()))
			{
				TransportInfo loaded3 = PrefabCollection<TransportInfo>.GetLoaded(num3);
				if (loaded3 != null && this.IsServiceValid(loaded3) && this.IsCategoryValid(loaded3, ignoreCategories) && this.IsPlacementRelevant(loaded3))
				{
					poolList.Add(loaded3);
				}
				num3 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Tree))
		{
			uint num4 = 0u;
			while ((ulong)num4 < (ulong)((long)PrefabCollection<TreeInfo>.LoadedCount()))
			{
				TreeInfo loaded4 = PrefabCollection<TreeInfo>.GetLoaded(num4);
				if (loaded4 != null && this.IsServiceValid(loaded4) && this.IsCategoryValid(loaded4, ignoreCategories) && this.IsPlacementRelevant(loaded4))
				{
					poolList.Add(loaded4);
				}
				num4 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Prop))
		{
			uint num5 = 0u;
			while ((ulong)num5 < (ulong)((long)PrefabCollection<PropInfo>.LoadedCount()))
			{
				PropInfo loaded5 = PrefabCollection<PropInfo>.GetLoaded(num5);
				if (loaded5 != null && this.IsServiceValid(loaded5) && this.IsCategoryValid(loaded5, ignoreCategories) && this.IsPlacementRelevant(loaded5))
				{
					poolList.Add(loaded5);
				}
				num5 += 1u;
			}
		}
		if (filter.IsFlagSet(GeneratedScrollPanel.AssetFilter.Disaster))
		{
			uint num6 = 0u;
			while ((ulong)num6 < (ulong)((long)PrefabCollection<DisasterInfo>.LoadedCount()))
			{
				DisasterInfo loaded6 = PrefabCollection<DisasterInfo>.GetLoaded(num6);
				if (loaded6 != null && this.IsServiceValid(loaded6) && this.IsCategoryValid(loaded6, ignoreCategories) && this.IsPlacementRelevant(loaded6))
				{
					poolList.Add(loaded6);
				}
				num6 += 1u;
			}
		}
		poolList.Sort(comparison);
		return poolList;
	}

	private void CreateAssetItem(PrefabInfo info)
	{
		string localizedTooltip = info.GetLocalizedTooltip();
		int hashCode = TooltipHelper.GetHashCode(localizedTooltip);
		UIComponent tooltipBox = GeneratedPanel.GetTooltipBox(hashCode);
		this.SpawnEntry(info.get_name(), localizedTooltip, info.m_Thumbnail, info.m_Atlas, tooltipBox, ToolsModifierControl.IsUnlocked(info.GetUnlockMilestone())).set_objectUserData(info);
	}

	public void PopulateAssets()
	{
		this.PopulateAssets(GeneratedScrollPanel.AssetFilter.All, new Comparison<PrefabInfo>(this.ItemsTypeSort));
	}

	public void PopulateAssets(GeneratedScrollPanel.AssetFilter filter)
	{
		this.PopulateAssets(filter, new Comparison<PrefabInfo>(this.ItemsGenericSort), false);
	}

	public void PopulateAssets(GeneratedScrollPanel.AssetFilter filter, Comparison<PrefabInfo> comparison)
	{
		this.PopulateAssets(filter, comparison, false);
	}

	public void PopulateAssets(GeneratedScrollPanel.AssetFilter filter, Comparison<PrefabInfo> comparison, bool ignoreCategories)
	{
		using (PoolList<PrefabInfo> poolList = this.CollectAssets(filter, comparison, ignoreCategories))
		{
			for (int i = 0; i < poolList.get_Count(); i++)
			{
				this.CreateAssetItem(poolList.get_Item(i));
			}
		}
	}

	public virtual void OnTooltipEnter(UIComponent comp, UIMouseEventParameter p)
	{
		UIButton uIButton = p.get_source() as UIButton;
		if (uIButton != null)
		{
			PrefabInfo prefabInfo = uIButton.get_objectUserData() as PrefabInfo;
			if (prefabInfo != null)
			{
				UISprite uISprite = uIButton.get_tooltipBox().Find<UISprite>("Sprite");
				if (prefabInfo.m_InfoTooltipAtlas != null)
				{
					uISprite.set_atlas(prefabInfo.m_InfoTooltipAtlas);
				}
				else
				{
					uISprite.set_atlas(this.m_DefaultInfoTooltipAtlas);
				}
				if (!string.IsNullOrEmpty(prefabInfo.m_InfoTooltipThumbnail) && uISprite.get_atlas().get_Item(prefabInfo.m_InfoTooltipThumbnail) != null)
				{
					uISprite.set_spriteName(prefabInfo.m_InfoTooltipThumbnail);
				}
				else
				{
					uISprite.set_spriteName("ThumbnailBuildingDefault");
				}
			}
			else
			{
				UISprite uISprite2 = uIButton.get_tooltipBox().Find<UISprite>("Sprite");
				if (uISprite2 != null)
				{
					uISprite2.set_atlas(this.m_DefaultInfoTooltipAtlas);
				}
			}
		}
	}

	public virtual void OnTooltipHover(UIComponent comp, UIMouseEventParameter p)
	{
		UIButton uIButton = p.get_source() as UIButton;
		if (uIButton != null)
		{
			PrefabInfo prefabInfo = uIButton.get_objectUserData() as PrefabInfo;
			if (prefabInfo != null && uIButton.get_tooltipBox().get_isVisible())
			{
				string text;
				string text2;
				string text3;
				string text4;
				string text5;
				ToolsModifierControl.GetUnlockingInfo(prefabInfo.GetUnlockMilestone(), out text, out text2, out text3, out text4, out text5);
				uIButton.set_tooltip(TooltipHelper.Replace(uIButton.get_tooltip(), new string[]
				{
					LocaleFormatter.Locked,
					text5,
					LocaleFormatter.LockedInfo,
					text5,
					LocaleFormatter.UnlockPopulationProgressText,
					text4,
					LocaleFormatter.UnlockPopulationCurrent,
					text2,
					LocaleFormatter.UnlockPopulationTarget,
					text3,
					LocaleFormatter.UnlockDesc,
					text
				}));
				uIButton.RefreshTooltip();
			}
		}
	}

	protected void RefreshDLCBadges()
	{
		foreach (UIComponent current in this.childComponents)
		{
			PrefabInfo prefabInfo;
			if (this.GetInfo(current, out prefabInfo))
			{
				UIPanel uIPanel = current.Find<UIPanel>("DLCBadge");
				List<string> icons = DLCIconUtils.GetIcons(prefabInfo.m_dlcRequired, 1);
				if (icons.Count > 0)
				{
					uIPanel.set_backgroundSprite(icons[0]);
					uIPanel.Show();
				}
			}
		}
	}

	protected bool GetInfo(UIComponent comp, out PrefabInfo info)
	{
		object objectUserData = comp.get_objectUserData();
		PrefabInfo prefabInfo = objectUserData as PrefabInfo;
		if (prefabInfo != null)
		{
			info = prefabInfo;
			return true;
		}
		info = null;
		return false;
	}

	private static readonly string kPlaceableItemTemplate = "PlaceableItemTemplate";

	private UIScrollablePanel m_ScrollablePanel;

	public UIComponent m_OptionsBar;

	public UITextureAtlas m_DefaultInfoTooltipAtlas;

	private string m_Category;

	protected int m_SelectedIndex;

	private int m_LastSelectedIndex;

	private int m_ObjectIndex;

	private bool m_CachedIsVisible;

	private OptionPanelBase m_PathsOptionPanel;

	private OptionPanelBase m_RoadsOptionPanel;

	private OptionPanelBase m_TracksOptionPanel;

	private OptionPanelBase m_TunnelsOptionPanel;

	private OptionPanelBase m_CanalsOptionPanel;

	private OptionPanelBase m_FloodWallsOptionPanel;

	private OptionPanelBase m_QuaysOptionPanel;

	private OptionPanelBase m_PipesOptionPanel;

	private OptionPanelBase m_ZoningOptionPanel;

	private OptionPanelBase m_DistrictOptionPanel;

	private OptionPanelBase m_LandscapingOptionPanel;

	private OptionPanelBase m_DisastersOptionPanel;

	private OptionPanelBase m_CurvedPropsOptionPanel;

	private OptionPanelBase m_PowerLinesOptionPanel;

	public enum AssetFilter
	{
		Net = 1,
		Building,
		Transport = 4,
		Tree = 8,
		Wonder = 16,
		Prop = 32,
		Disaster = 64,
		All = 127
	}
}
