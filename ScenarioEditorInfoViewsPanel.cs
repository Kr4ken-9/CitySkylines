﻿using System;
using ColossalFramework;

public class ScenarioEditorInfoViewsPanel : InfoViewsPanel
{
	public override void RefreshPanel()
	{
		base.RefreshPanel();
		for (int i = 0; i < ScenarioEditorInfoViewsPanel.kResources.Length; i++)
		{
			base.SpawnButtonEntry(ScenarioEditorInfoViewsPanel.kResources[i].get_enumName(), "InfoIcon", "MAPEDITOR_INFOVIEWS", i, true);
		}
	}

	protected override void ShowSelectedIndex()
	{
		if (Singleton<InfoManager>.get_exists())
		{
			if (Singleton<InfoManager>.get_instance().NextMode != this.m_cachedMode)
			{
				this.m_cachedMode = Singleton<InfoManager>.get_instance().NextMode;
				this.m_cachedIndex = Utils.GetEnumIndexByValue<InfoManager.InfoMode>(this.m_cachedMode, "MapEditor");
			}
			base.selectedIndex = this.m_cachedIndex;
		}
		base.ShowSelectedIndex();
	}

	protected override void OnButtonClicked(int index)
	{
		if (index >= 0 && index < ScenarioEditorInfoViewsPanel.kResources.Length)
		{
			base.CloseToolbar();
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(ScenarioEditorInfoViewsPanel.kResources[index].get_enumValue(), InfoManager.SubInfoMode.Default);
			}
		}
		else
		{
			base.CloseToolbar();
			if (Singleton<InfoManager>.get_exists())
			{
				Singleton<InfoManager>.get_instance().SetCurrentMode(InfoManager.InfoMode.None, InfoManager.SubInfoMode.Default);
			}
		}
	}

	private static readonly PositionData<InfoManager.InfoMode>[] kResources = Utils.GetOrderedEnumData<InfoManager.InfoMode>("MapEditor");

	private InfoManager.InfoMode m_cachedMode;

	private int m_cachedIndex = Utils.GetEnumIndexByValue<InfoManager.InfoMode>(InfoManager.InfoMode.None, "MapEditor");
}
