﻿using System;
using ColossalFramework.IO;

[Serializable]
public struct FixedVector2D
{
	public FixedVector2D(int x, int z)
	{
		this.x = x;
		this.z = z;
	}

	public void Serialize24(DataSerializer s)
	{
		s.WriteInt24(this.x);
		s.WriteInt24(this.z);
	}

	public void Deserialize24(DataSerializer s)
	{
		this.x = s.ReadInt24();
		this.z = s.ReadInt24();
	}

	public static FixedVector2D operator -(FixedVector2D a, FixedVector2D b)
	{
		return new FixedVector2D(a.x - b.x, a.z - b.z);
	}

	public static FixedVector2D operator +(FixedVector2D a, FixedVector2D b)
	{
		return new FixedVector2D(a.x + b.x, a.z + b.z);
	}

	public int x;

	public int z;
}
