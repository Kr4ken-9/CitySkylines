﻿using System;

public class LandscapingGroupPanel : GeneratedGroupPanel
{
	public override ItemClass.Service service
	{
		get
		{
			return ItemClass.Service.Beautification;
		}
	}

	public override GeneratedGroupPanel.GroupFilter groupFilter
	{
		get
		{
			return (GeneratedGroupPanel.GroupFilter)107;
		}
	}

	protected override bool IsCategoryRelevant(string category)
	{
		return category == "BeautificationPaths" || category == "BeautificationProps" || category == "LandscapingPaths" || category == "LandscapingTrees" || category == "LandscapingRocks" || category == "LandscapingWaterStructures" || (category == "LandscapingDisasters" && SteamHelper.IsDLCOwned(SteamHelper.DLC.NaturalDisastersDLC));
	}

	protected override int GetCategoryOrder(string name)
	{
		if (name != null)
		{
			if (name == "LandscapingPaths")
			{
				return 1;
			}
			if (name == "LandscapingTrees")
			{
				return 2;
			}
			if (name == "LandscapingRocks")
			{
				return 3;
			}
			if (name == "LandscapingWaterStructures")
			{
				return 4;
			}
			if (name == "LandscapingDisasters")
			{
				return 5;
			}
		}
		return 2147483647;
	}

	protected override GeneratedGroupPanel.GroupInfo CreateGroupInfo(string name, PrefabInfo info)
	{
		if (name == "LandscapingDisasters")
		{
			return new GeneratedGroupPanel.GroupInfo(name, this.GetCategoryOrder(name), "Disasters");
		}
		return base.CreateGroupInfo(name, info);
	}

	protected override bool CustomRefreshPanel()
	{
		base.CreateGroupItem(new GeneratedGroupPanel.GroupInfo("Landscaping", 0, "Landscaping"), "LANDSCAPING_CATEGORY");
		return false;
	}
}
